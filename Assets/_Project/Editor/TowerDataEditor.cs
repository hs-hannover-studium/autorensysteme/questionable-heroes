using System.Collections;
using System.Collections.Generic;
using Heroes.ScriptableObjects.Buildings;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace Heroes.Editor
{
    [CustomEditor(typeof(TowerData))]
    public class TowerDataEditor : OdinEditor
    {
        // public override void OnInspectorGUI()
        // {
        //     PropertyTree tree = Tree;
        //
        //     tree.BeginDraw(true);
        //     
        //     InspectorProperty buildingName = tree.GetPropertyAtPath("buildingName");
        //     buildingName.Draw();  
        //     
        //     InspectorProperty buildingPrefab = tree.GetPropertyAtPath("buildingPrefab");
        //     buildingPrefab.Draw(); 
        //     
        //     InspectorProperty projectilePrefab = tree.GetPropertyAtPath("projectilePrefab");
        //     projectilePrefab.Draw(); 
        //     
        //     InspectorProperty cost = tree.GetPropertyAtPath("cost");
        //     cost.Draw(); 
        //     
        //     InspectorProperty stats = tree.GetPropertyAtPath("stats");
        //     stats.Draw();
        //
        //     tree.EndDraw();
        // }
    }
}
