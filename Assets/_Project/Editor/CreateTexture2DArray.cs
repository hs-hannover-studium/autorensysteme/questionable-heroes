using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Heroes._Project.Editor
{
    public class CreateTexture2DArray
    {
        [MenuItem("Assets/Create/Texture2DArray")]
        public static void Create()
        {
            Object[] selection = UnityEditor.Selection.objects;
            Texture2D[] textures = new Texture2D[selection.Length];
            String path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(selection[0]));

            for (int i = 0; i < textures.Length; i++)
            {
                textures[i] = (Texture2D) selection[i];
            }

            Texture2DArray array = new Texture2DArray(textures[0].width, textures[0].height, textures.Length,
                textures[0].format, false);

            for (int i = 0; i < textures.Length; i++)
            {
                array.SetPixels(textures[i].GetPixels(0),
                    i, 0);
            }

            array.Apply();

            path = AssetDatabase.GenerateUniqueAssetPath(path + "/Texture2DArray.asset");

            AssetDatabase.CreateAsset(array, path);
        }
    }
}