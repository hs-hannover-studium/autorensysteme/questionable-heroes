using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    public class TweenAnimation : SerializedMonoBehaviour
    {
        [SerializeField] private List<ITweenAnimation> _tweenAnimations = new List<ITweenAnimation>();

        [SerializeField] 
        private bool _loop;

        private void OnEnable()
        {
            Sequence sequence = DOTween.Sequence();

            foreach (var tweenAnimation in _tweenAnimations)
            {
                sequence.Join(tweenAnimation.GetTween(gameObject));
            }

            if (_loop)
                sequence.SetLoops(-1);
        }
    }
}
