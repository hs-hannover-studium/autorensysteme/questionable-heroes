using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    public interface ITweenAnimation
    {
        public Tween GetTween(GameObject gameObject);
    }
}
