using DG.Tweening;
using UnityEngine;

namespace Heroes
{
    public class TransformRotationStrategy : ITweenAnimation
    {
        [SerializeField] private Ease _easeType = Ease.Linear;
        [SerializeField] private float _duration = .25f;
        [SerializeField] private Vector3 _to;
        [SerializeField] private RotateMode _rotateMode = RotateMode.Fast;
        [SerializeField] private float _delay;
    
        public Tween GetTween(GameObject gameObject)
        {
            Tween rotationTween = gameObject.transform.DORotate(_to, _duration, _rotateMode)
                .SetEase(_easeType)
                .SetDelay(_delay);
            return rotationTween;
        }
    }
}
