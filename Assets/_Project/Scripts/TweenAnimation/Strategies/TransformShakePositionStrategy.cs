using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Heroes
{
    public class TransformShakePositionStrategy : ITweenAnimation
    {
        [SerializeField] private Ease _easeType = Ease.Linear;
        [SerializeField] private float _duration = 1f;
        [SerializeField] private float _strength = 1f;
        [SerializeField] private int _vibrato = 1;
        [SerializeField] private float _randomness = 1f;
        [SerializeField] private bool _snapping = false;
        [SerializeField] private bool _fadeOut = false;
        [SerializeField] private float _delay;
    
        public Tween GetTween(GameObject gameObject)
        {
            Tween shakePositionTween = gameObject.transform.DOShakePosition(_duration, _strength, _vibrato, _randomness, _snapping, _fadeOut)
                .SetEase(_easeType)
                .SetDelay(_delay);
            return shakePositionTween;
        }
    }
}
