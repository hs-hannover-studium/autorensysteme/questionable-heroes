using Heroes.Heroes;
using Heroes.ScriptableObjects.RuntimeSets;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.HeroSpawner
{
    public class AliveHeroTracker : MonoBehaviour
    {
        public HeroSpawner heroSpawner;
        public RuntimeSet<Hero> heroes;

        public UnityEvent allHeroesDied;

        private void OnEnable()
        {
            heroSpawner.SpawningCompleted += OnSpawningCompleted;
        }

        private void OnDisable()
        {
            heroSpawner.SpawningCompleted -= OnSpawningCompleted;
            heroes.ValueChanged -= OnHeroCountChanged;
        }

        private void OnSpawningCompleted()
        {
            heroes.ValueChanged += OnHeroCountChanged;
        }

        private void OnHeroCountChanged(Hero h)
        {
            if (heroes.Count <= 0)
                allHeroesDied?.Invoke();
        }
    }
}