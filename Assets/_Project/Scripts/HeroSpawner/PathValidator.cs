using System;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.UI;
using Heroes.World;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Heroes.HeroSpawner
{
    public class PathValidator : MonoBehaviour, IDependency
    {
        [SerializeField] private TileSet spawnPoints;
        [SerializeField] private TileSet levelCores;

        public UnityEvent OnNoPathFound;
        public UnityEvent OnAllPathsValid;

        public event Action DependencyCompleted;

        [Button]
        public void ValidatePath()
        {
            NavMeshPath path = new NavMeshPath();

            foreach (Tile tile in spawnPoints)
            {
                SpawnTile spawnPoint = (SpawnTile)tile;

                foreach (Tile c in levelCores)
                {
                    CoreTile levelCore = (CoreTile)c;
                    bool foundPath = false;

                    foreach (Transform rallyPoint in levelCore.rallyPoints)
                    {
                        Vector3 source = spawnPoint.SpawnPoint.transform.position;
                        Vector3 target = rallyPoint.position;
                        NavMesh.CalculatePath(source, target, NavMesh.AllAreas, path);

                        // There is at least one way from spawnpoint to this core
                        if (path.status == NavMeshPathStatus.PathComplete)
                        {
                            foundPath = true;
                            break;
                        }
                    }

                    // There is no path from spawnpoint to this cores rally points
                    // So level is overall not valid
                    if (!foundPath)
                    {
                        // TODO Notify user
                        Debug.Log("Blocked Path found");
                        OnNoPathFound?.Invoke();
                        return;
                    }
                }
            }

            Debug.Log("All paths valid");
            OnAllPathsValid?.Invoke();
            DependencyCompleted?.Invoke();

            return;
        }
    }
}