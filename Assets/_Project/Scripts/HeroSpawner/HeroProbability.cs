using System;
using Heroes.Probabilities;
using Heroes.ScriptableObjects;

namespace Heroes.HeroSpawner
{
    [Serializable]
    public class HeroProbability : Probability<HeroData>
    {
        public HeroProbability(float probability, HeroData data) : base(probability, data)
        {
        }
    }
}