using System;
using System.Collections;
using Heroes.Extensions;
using Heroes.Heroes;
using Heroes.Probabilities;
using Heroes.ScriptableObjects;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.UI;
using Heroes.World;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Heroes.HeroSpawner
{
    public class HeroSpawner : MonoBehaviour, IDependency
    {
        [SerializeField] private float spawnInterval = 0.25f;
        [SerializeField] private TileSet spawnPoints;
        [SerializeField] private TileSet levelCores;
        [SerializeField] private ProbabilityCalculator probabilityCalculator;
        [SerializeField] private HeroWaveCalculator waveCalculator;
        [SerializeField] private HeroesSet aliveHeros;

        public event Action SpawningCompleted;
        public event Action DependencyCompleted;
        
        [Button, HideInEditorMode]
        public void Spawn()
        {
            StartCoroutine(SpawnUnits());
        }

        [Button]
        public void Clear()
        {
            transform.RemoveChildrenImmediate();
        }

        public void KillAll()
        {
            foreach (Hero hero in aliveHeros)
            {
                hero.GetComponent<Damageable.Damageable>().Kill();
            }
        }

        private IEnumerator SpawnUnits()
        {
            for (int i = 0; i < waveCalculator.heroCount.RuntimeValue; i++)
            {
                // Find spawnPoint to spawn unit at
                SpawnTile spawnPoint = (SpawnTile) spawnPoints[Random.Range(0, spawnPoints.Count)];

                // Get hero based on their probability
                HeroData heroToSpawn = probabilityCalculator.SelectHero();

                // Spawn prefab
                GameObject heroPrefabs = Instantiate(heroToSpawn.Prefab,
                    spawnPoint.SpawnPoint.transform.position, Quaternion.identity, transform);

                // Set level of hero
                Hero hero = heroPrefabs.GetComponent<Hero>();
                hero.runtimeStats.SetLevel(waveCalculator.heroLevel.RuntimeValue);

                yield return new WaitForSeconds(spawnInterval);
            }
            
            SpawningCompleted?.Invoke();
        }
    }
}