using Heroes.ScriptableObjects.Values;
using Heroes.ScriptableObjects.World;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.HeroSpawner
{
    [CreateAssetMenu(fileName = "HeroWaveCalculator", menuName = "Spawner/HeroWaveCalculator", order = 0)]
    public class HeroWaveCalculator : ScriptableObject
    {
        public IntValue heroCount;
        public IntValue heroLevel;
        public GameSession gameSession;
        
        [Button]
        public void CalculateHeroCount()
        {
            heroCount.RuntimeValue = gameSession.Difficulty.MinHeroCount + gameSession.Difficulty.HeroCount();
        }
        
        [Button]
        public void CalculateHeroLevel()
        {
            heroLevel.RuntimeValue = gameSession.Difficulty.HeroStrength();
        }
    }
}