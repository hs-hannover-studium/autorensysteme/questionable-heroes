using System.Collections;
using Heroes.ScriptableObjects.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.SceneManagement
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private TextCollection tips;
        [SerializeField] private TextMeshProUGUI tipView;
        [SerializeField] private Slider loadingProgressView;
        [SerializeField] private float tipCycleSpeed = 5f;

        public void SetProgress(float value)
        {
            loadingProgressView.value = value;
        }

        private void OnEnable()
        {
            SetTip();
        }

        private IEnumerator CycleTips()
        {
            yield return new WaitForSeconds(tipCycleSpeed);
            SetTip();
        }

        private void SetTip()
        {
            tipView.SetText(tips.RandomString());
        }
    }
}