using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Heroes.SceneManagement
{
    public class SceneLoader : MonoBehaviour
    {
        public static SceneLoader Instance { get; set; }
        
        [SerializeField] private LoadingScreen loadingScreen;

        private List<AsyncOperation> _loadingScenes = new List<AsyncOperation>();

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) Destroy(gameObject);
            
            DontDestroyOnLoad(gameObject);
        }

        public void LoadScene(int index)
        {
            _loadingScenes.Add(SceneManager.LoadSceneAsync(index));
            StartCoroutine(GetSceneLoadProgress());
        }

        public void LoadScene(String sceneName)
        {
            int sceneIndex = SceneManager.GetSceneByName(sceneName).buildIndex;
            LoadScene(sceneIndex);
        }

        private void ShowLoadingScreen()
        {
            loadingScreen.gameObject.SetActive(true);
        }

        private void HideLoadingScreen()
        {
            loadingScreen.gameObject.SetActive(false);
        }

        private IEnumerator GetSceneLoadProgress()
        {
            ShowLoadingScreen();
            float totalLoadProgress = 0f;

            foreach (AsyncOperation scene in _loadingScenes)
            {
                while (!scene.isDone)
                {
                    foreach (AsyncOperation operation in _loadingScenes)
                    {
                        totalLoadProgress += operation.progress;
                    }

                    totalLoadProgress = (totalLoadProgress / _loadingScenes.Count) * 100f;
                    loadingScreen.SetProgress(totalLoadProgress);

                    yield return null;
                }
            }

            HideLoadingScreen();
        }
    }
}