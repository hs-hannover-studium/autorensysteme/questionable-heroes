using Heroes.Heroes;
using Heroes.Misc;
using Heroes.ScriptableObjects.RuntimeSets;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes
{
    public class AnimationEvents : MonoBehaviour
    {
        [SerializeField] private GameObject _parentObject;

        [SerializeField] private ResourcesSet _resourcesSet;
        
        [SerializeField] private GameObject _weaponTrail;
        
        [SerializeField] private UnityEvent _onFootstep;

        private Hero _hero;
        private DamageDealer _damageDealer;

        private Animator _animator;

        public GameObject WeaponTrail => _weaponTrail;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _hero = _parentObject.GetComponent<Hero>();
            _damageDealer = _parentObject.GetComponent<DamageDealer>();
        }

        public void GetTreasure()
        {
            foreach (var treasure in _hero.runtimeStats.capacity)
            {
                ResourceValuePair resource = new ResourceValuePair(treasure.ResourceValue, treasure.RuntimeValue);
                _resourcesSet.Pay(resource);
            }
            Destroy(_parentObject);
        }

        public void ShowWeaponTrail()
        {
            _weaponTrail.SetActive(true);
            _damageDealer.StartAttack();
        }

        public void HideWeaponTrail() => _weaponTrail.SetActive(false);

        public void SpawnProjectile() => _damageDealer.StartAttack();
        
        public void DealDamage() => _damageDealer.DealDamage();

        public void Death()
        {
            _animator.enabled = false;
            _hero.BodyMesh.gameObject.SetActive(false);
            _hero.WeaponMesh.gameObject.SetActive(false);
            Destroy(_parentObject, 6f);
        }

        public void PlayFootstep() => _onFootstep.Invoke();
    }
}
