using Heroes.ScriptableObjects;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes.Heroes
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class Hero : MonoBehaviour
    {
        [SerializeField, Required]
        private HeroData heroData;
        
        [SerializeField, Required]
        private RandomNameCollection nameCollection;

        private string _heroName;
        
        private NavMeshAgent _navMeshAgent;
        
        [ShowInInspector, PropertyOrder(0)]
        public string HeroName => _heroName;

        [SerializeField] private Renderer _bodyMesh;
        [SerializeField] private Renderer _weaponMesh;

        [HideLabel, FoldoutGroup("Stats"), PropertyOrder(1)]
        public HeroStatCollection runtimeStats;

        public HeroData Data => heroData;

        public Renderer BodyMesh => _bodyMesh;

        public Renderer WeaponMesh => _weaponMesh;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            Instantiate();
        }

        private void Start()
        {
            _navMeshAgent.stoppingDistance = Random.Range(0.15f, 0.3f);
            
            _navMeshAgent.speed = (float) runtimeStats[StatType.MovementSpeed].RuntimeValue / 10 + Random.Range(-0.05f, 0.05f);
            _navMeshAgent.angularSpeed = 1950f;
            _navMeshAgent.acceleration = 1999f;
            NavMesh.avoidancePredictionTime = 0.5f;
            _navMeshAgent.avoidancePriority = Random.Range(1, 100);
        }

        [Button("New Instance"), PropertyOrder(2)]
        private void Instantiate()
        {
            _heroName = nameCollection.GenerateRandomName();
            runtimeStats = new HeroStatCollection(Data);
        }
    }
}
