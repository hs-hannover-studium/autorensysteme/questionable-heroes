using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    public class SpeechBubbleCaller : MonoBehaviour
    {
        public event Action<SpeechBubbleCollection> speechBubbleEvent;

        [Button]
        public void RaiseSpeechBubble(SpeechBubbleCollection speechBubbleCollection)
        {
            speechBubbleEvent?.Invoke(speechBubbleCollection);
        }
    }
}
