using System;
using Heroes.Pathfinding;
using Heroes.World;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

namespace Heroes.Misc
{
    public class Agent : MonoBehaviour
    {
        private NavMeshAgent _agent;
        private InputActions _playerControls;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            
            _playerControls = new InputActions();

            _playerControls.Gameplay.Move.performed += OnMove;
        }
        
        private void OnEnable()
        {
            _playerControls.Enable();
            _playerControls.Gameplay.Enable();
        }
        
        private void OnDisable()
        {
            _playerControls.Disable();
            _playerControls.Gameplay.Disable();
        }

        /// <summary>
        /// Moves agent to Mouse position
        /// </summary>
        /// <param name="context"></param>
        public void OnMove(InputAction.CallbackContext context)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (!Physics.Raycast(ray, out hit, Mathf.Infinity))
                return;

            try
            {
                GetComponent<PathMarker>().MarkPath(_agent, hit.point);
            }
            catch (Exception e)
            {
                Debug.Log("No PathMarker found");
            }
            _agent.SetDestination(hit.point);

        }

        [Button]
        public Tile GetCurrentPosition()
        {
            
            RaycastHit hit;
            if (Physics.Raycast(gameObject.transform.position, Vector3.down, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.GetComponent<Tile>() != null)
                {
                    Destroy(hit.collider.gameObject);
                    //return hit.collider.gameObject.GetComponent<Tile>();
                }
            }
            return null;
        }
    }
}
