using Cinemachine;
using Heroes.Extensions;
using Heroes.ScriptableObjects.Values;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Heroes.Misc
{
    public class CameraController : MonoBehaviour
    {
        public float moveSpeed = 20f;
        public float zoomSpeed = 20f;
        public float zoomTime = 5f;
        public bool invertY;
        public SelectionValue selectionValue;
        [HideInInspector] public BoxCollider confiner;
        [HideInInspector] public AnimationCurve zoomRotationCurve;

        [SerializeField] private CinemachineVirtualCamera _focusCamera;
        private InputActions _playerControls;
        private bool _focusMode;

        public UnityEvent OnFocusEnter;
        public UnityEvent OnFocusExit;

        public void AddRotationCurveKey(Vector2 key1)
        {
            zoomRotationCurve.AddKey(key1.x, key1.y);
        }

        private void Awake()
        {
            _playerControls = new InputActions();
            zoomRotationCurve = new AnimationCurve();
        }

        private void OnEnable()
        {
            _playerControls.Enable();
            _playerControls.Camera.Enable();

            _playerControls.Camera.Move.performed += OnMove;
            _playerControls.Camera.Zoom.performed += OnZoom;
            _playerControls.Camera.Focus.performed += OnFocus;
        }

        private void LateUpdate()
        {
            Move();
        }

        private void OnDisable()
        {
            _playerControls.Disable();
            _playerControls.Camera.Disable();

            _playerControls.Camera.Move.performed -= OnMove;
            _playerControls.Camera.Zoom.performed -= OnZoom;
            _playerControls.Camera.Focus.performed -= OnFocus;
        }

        private void Move()
        {
            Vector3 pos = transform.position;
            Vector2 moveDirection = _playerControls.Camera.Move.ReadValue<Vector2>();

            pos.x += moveDirection.x * moveSpeed * Time.deltaTime;
            pos.z += moveDirection.y * moveSpeed * Time.deltaTime;

            if (confiner.IsInside(pos))
                transform.position = pos;
        }

        private void OnMove(InputAction.CallbackContext obj)
        {
            ResetFocus();
        }

        private void OnZoom(InputAction.CallbackContext obj)
        {
            bool pointerOverUI = EventSystem.current.IsPointerOverGameObject();
            if (pointerOverUI) return;
            
            Vector3 pos = transform.position;
            float zoomMovement = _playerControls.Camera.Zoom.ReadValue<Vector2>().y;
            zoomMovement = invertY ? -zoomMovement : zoomMovement;

            zoomMovement = Mathf.Clamp(zoomMovement, -1, 1);

            pos.y += zoomMovement * zoomSpeed * Time.deltaTime;
            
            float minY = confiner.center.y - confiner.size.y / 2;
            float maxY = confiner.center.y + confiner.size.y / 2;
            pos.y = Mathf.Clamp(pos.y, minY, maxY);

            transform.localPosition = Vector3.Lerp(transform.localPosition, pos, zoomTime);

            float rotationAngle = zoomRotationCurve.Evaluate(transform.position.y);
            transform.rotation = Quaternion.Euler(rotationAngle, 0, 0);
        }

        private void OnFocus(InputAction.CallbackContext obj)
        {
            if (!selectionValue.RuntimeValue)
                return;
            
            _focusMode = true;
            _focusCamera.Follow = selectionValue.RuntimeValue.gameObject.transform;
            _focusCamera.LookAt = selectionValue.RuntimeValue.gameObject.transform;
            
            transform.rotation = Quaternion.Euler(45, 0, 0);
            
            OnFocusEnter?.Invoke();
        }

        private void ResetFocus()
        {
            if (!_focusMode)
                return;
            
            _focusCamera.Follow = null;
            _focusCamera.LookAt = null;
            _focusMode = false;
            
            OnFocusExit?.Invoke();
        }
    }
}