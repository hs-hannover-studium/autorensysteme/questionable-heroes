using Heroes.ScriptableObjects.States;
using UnityEngine;

namespace Heroes.Misc
{
    public class GameStateChanger : MonoBehaviour
    {
        public GameStateManager manager;
        
        public void EnterState(GameState state)
        {
            manager.TransitionToState(state);
        }

        public void ExitState()
        {
            manager.ClearState();
        }

        public void ToggleState(GameState state)
        {
            if (manager.ActiveState == state)
            {
                manager.ClearState();
            }
            else
            {
                manager.TransitionToState(state);
            }
        }
    }
}