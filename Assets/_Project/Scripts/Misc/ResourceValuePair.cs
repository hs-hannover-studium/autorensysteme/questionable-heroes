using System;
using Heroes.ScriptableObjects.Values;

namespace Heroes.Misc
{
    [Serializable]
    public class ResourceValuePair
    {
        public ResourceValue resourceValue;
        public int value;

        public ResourceValuePair(ResourceValue resourceValue, int value)
        {
            this.resourceValue = resourceValue;
            this.value = value;
        }
    }
}