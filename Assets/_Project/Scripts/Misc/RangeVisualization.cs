using System;
using Heroes.Build.Tower;
using Heroes.Stats;
using UnityEngine;

namespace Heroes.Misc
{
    public class RangeVisualization : MonoBehaviour
    {
        [SerializeField] private Tower tower;

        private void OnEnable()
        {
            UpdateRange();

            tower.runtimeStats.StatsChanged += UpdateRange;
        }

        private void OnDisable()
        {
            tower.runtimeStats.StatsChanged -= UpdateRange;
        }

        private void UpdateRange()
        {
            // Divided by 2, by 10 (to get radius of plane) and by 10 again (because RangeValue must be divided by 10)
            transform.localScale = Vector3.one * tower.runtimeStats[StatType.ATKRange].RuntimeValue * 0.02f;
        }
    }
}