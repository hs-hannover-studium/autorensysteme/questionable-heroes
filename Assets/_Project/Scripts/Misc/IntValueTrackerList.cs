using System.Collections.Generic;
using Heroes.ScriptableObjects.Values;
using UnityEngine;

namespace Heroes.Misc
{
    public class IntValueTrackerList : MonoBehaviour
    {
        [SerializeField] private List<IntValueTracker> trackers;
    }
}