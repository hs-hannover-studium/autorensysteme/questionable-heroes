using Cinemachine;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class CameraShake : MonoBehaviour
    {
        private CinemachineVirtualCamera _cinemachineVirtualCamera;
    
        private void Awake()
        {
            _cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
        }

        [Button]
        public void ShakeCamera(float intensity)
        {
            CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin =
                _cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

            DOTween.To(() => cinemachineBasicMultiChannelPerlin.m_AmplitudeGain,
                    value => cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = value,
                    intensity,
                    0.5f)
                .OnComplete(()=> DOTween.To(() => cinemachineBasicMultiChannelPerlin.m_AmplitudeGain,
                    value => cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = value,
                    0,
                    0.5f));
            ;
        }
    }
}
