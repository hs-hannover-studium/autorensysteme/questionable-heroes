using DG.Tweening;
using Heroes.Heroes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [RequireComponent(typeof(Damageable.Damageable), typeof(Hero))]
    public class TintMaterial : MonoBehaviour
    {
        private Hero _hero;

        private Damageable.Damageable _damageable;

        private void Awake()
        {
            _hero = GetComponent<Hero>();
            _damageable = GetComponent<Damageable.Damageable>();
        }

        private void OnEnable()
        {
            _damageable.Damaged += OnDamaged;
            _damageable.Died += OnDamaged;
        }
        
        private void OnDisable()
        {
            _damageable.Damaged -= OnDamaged;
            _damageable.Died -= OnDamaged;
        }

        private void OnDamaged()
        {
            Tint(new Color(0.5f, 0, 0, 1), 0.5f);
        }

        [Button]
        public void Tint(Color color, float duration)
        {
            _hero.BodyMesh.material.DOColor(color, duration)
                .OnComplete(()=>_hero.BodyMesh.material.color = Color.white);
        }
    }
}
