using System;
using System.Collections.Generic;
using Heroes.ScriptableObjects.States;
using Heroes.ScriptableObjects.Values;
using Heroes.ScriptableObjects.World;
using Heroes.TimeManagement;
using Heroes.World;
using UnityEngine;

namespace Heroes.Misc
{
    public class GameManager : MonoBehaviour
    {
        public TimeChanger timeChanger;
        public GameStateManager gameStateManager;
        public LevelGenerator levelGenerator;
        public GameState initialState;
        public GameSession gameSession;
        public IntValue[] valuesToReset;
        public List<ResourceValue> resourceValues;

        private static GameManager Instance { get; set; }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) Destroy(gameObject);
        }

        private void OnEnable()
        {
            levelGenerator.levelGenerated += OnLevelGenerated;
        }

        private void OnDisable()
        {
            levelGenerator.levelGenerated -= OnLevelGenerated;
        }

        private void OnLevelGenerated()
        {
            SetStartingResources();
            ResetIntValues();
            SetInitialState();
            ResetTimeScale();
        }

        private void ResetTimeScale()
        {
            timeChanger.ResetTime();
        }

        private void SetStartingResources()
        {
            foreach (ResourceValue value in resourceValues)
            {
                ResourceValuePair startValue =
                    gameSession.Difficulty.StartingResources.Find(e => e.resourceValue == value);
                value.RuntimeValue = startValue.value;
            }
        }

        private void ResetIntValues()
        {
            foreach (IntValue value in valuesToReset)
            {
                value.Reset();
            }
        }

        private void SetInitialState()
        {
            gameStateManager.TransitionToState(initialState);
        }
    }
}