using Heroes.ScriptableObjects.Events;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.Misc
{
    public class GameEventCaller : MonoBehaviour
    {
        [SerializeField] private GameEvent _gameEvent;

        [Button]
        public void CallEvent()
        {
            _gameEvent.Raise();
        }
    }
}