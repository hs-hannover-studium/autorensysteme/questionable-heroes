using System;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.Damageable
{
    public abstract class Damageable : MonoBehaviour
    {
        public abstract DamageActor Actor { get; }

        public event Action Damaged;
        public event Action Healed;
        public event Action Died;

        public event Action HealthChanged;
        
        [ShowInInspector, ReadOnly]
        private bool _isAlive = true;

        [ReadOnly, ShowInInspector, InlineProperty, HideLabel]
        private Stat _hpStatDebug = new Stat(StatType.HP, 0);

        [FoldoutGroup("Events"), SerializeField] 
        private UnityEvent OnDamaged;
        [FoldoutGroup("Events"), SerializeField] 
        private UnityEvent OnHealed;
        [FoldoutGroup("Events"), SerializeField] 
        private UnityEvent OnDeath;

        public abstract Stat hpStat { get; }

        public int MaxHealth => hpStat.DefaultValue;
        
        public int Health
        {
            get => hpStat.RuntimeValue;
            set
            {
                hpStat.RuntimeValue = Mathf.Clamp(value, 0, MaxHealth);
                HealthChanged?.Invoke();
                
                if (hpStat.RuntimeValue == 0)
                    Death();
            }
        }
        
        public bool IsAlive => _isAlive;

        private protected virtual void Start()
        {
            _hpStatDebug = hpStat;
        }

        [FoldoutGroup("Debug"), Button]
        public void Damage(int amount)
        {
            if (amount < 0)
            {
                Heal(Mathf.Abs(amount));
                return;
            }
            
            Health -= amount;

            if (Health == 0)
                return;
            
            Damaged?.Invoke();
            OnDamaged.Invoke();
        }
        
        [FoldoutGroup("Debug"), Button]
        public void Heal(int amount)
        {
            if (amount == 0 || Health == MaxHealth)
                return;
            
            if (amount < 0)
            {
                Damage(Mathf.Abs(amount));
                return;
            }

            Health += amount;
            
            Healed?.Invoke();
            OnHealed.Invoke();
        }

        [FoldoutGroup("Debug"), Button]
        public void Kill()
        {
            Health = 0;
        }
        
        private protected virtual void Death()
        {
            Died?.Invoke();
            OnDeath.Invoke();
            _isAlive = false;
        }
    }
}
