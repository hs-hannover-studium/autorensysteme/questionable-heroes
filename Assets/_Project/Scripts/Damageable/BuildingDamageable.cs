using Heroes.Build.Tower;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes.Damageable
{
    [RequireComponent(typeof(Tower))]
    public class BuildingDamageable : Damageable
    {
        public override DamageActor Actor => DamageActor.Building;

        private Tower _tower;
        private Collider _collider;
        private NavMeshObstacle _navMeshObstacle;
        
        public override Stat hpStat => _tower.runtimeStats[StatType.HP];

        private void Awake()
        {
            _collider = GetComponent<Collider>();
            _navMeshObstacle = GetComponent<NavMeshObstacle>();
        }

        private protected override void Start()
        {
            _tower = GetComponent<Tower>();
            base.Start();
        }

        private protected override void Death()
        {
            base.Death();
            _collider.enabled = false;
            _navMeshObstacle.enabled = false;
            foreach (var part in _tower.ModelParts)
            {
                part.gameObject.SetActive(false);
            }
            Destroy(gameObject, 3f);
        }
    }
}
