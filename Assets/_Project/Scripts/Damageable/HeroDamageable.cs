using Heroes.Heroes;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.Damageable
{
    [RequireComponent(typeof(Hero))]
    public class HeroDamageable : Damageable
    {
        public override DamageActor Actor => DamageActor.Hero;
        
        [SerializeField, PropertyOrder(-1)] private Animator _animator;
        [SerializeField, Range(0, 1), PropertyOrder(-1)] private float _damageAnimationProbability = 0.25f;
        [SerializeField, Range(0, 1), PropertyOrder(-1)] private float _damagedSometimesProbability = 0.25f;

        private Hero _hero;
        private int _damageHash;

        public UnityEvent OnDamagedSometimes;
        
        public override Stat hpStat => _hero.runtimeStats[StatType.HP];

        private void Awake()
        {
            _damageHash = Animator.StringToHash("Damage");
        }

        private protected override void Start()
        {
            _hero = GetComponent<Hero>();
            base.Start();
        }

        private void OnEnable()
        {
            Damaged += OnDamage;
        }

        private void OnDisable()
        {
            Damaged -= OnDamage;
        }

        private void OnDamage()
        {
            float random = Random.Range(0f, 1f);
            if (!_animator.GetCurrentAnimatorStateInfo(0).IsName("React") && random <= _damageAnimationProbability)
                _animator.SetTrigger(_damageHash);
            
            if(random <= _damagedSometimesProbability)
                OnDamagedSometimes?.Invoke();
        }

        private protected override void Death()
        {
            foreach (var resource in _hero.runtimeStats.drops)
            {
                resource.ResourceValue.Add(resource.RuntimeValue);
            }
            base.Death();
        }
    }
}
