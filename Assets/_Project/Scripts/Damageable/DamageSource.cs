using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.Damageable
{
    [RequireComponent(typeof(Collider))]
    public class DamageSource : MonoBehaviour
    {
        [SerializeField]
        private DamageActor _damageActor;
        [SerializeField]
        private protected int _damage = 0;

        [SerializeField]
        private LayerMask _collisionLayerMask;

        [SerializeField]
        private GameObject _model;

        [SerializeField]
        private bool _destroyOnCollision;

        [FoldoutGroup("Events"), SerializeField]
        private UnityEvent _onImpact;

        private Collider _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (((1 << other.gameObject.layer) & _collisionLayerMask) == 0)
                return;
            
            _onImpact.Invoke();
            
            if (!other.TryGetComponent(out Damageable damageable))
            {
                if (_destroyOnCollision)
                    Destroy(gameObject);
                return;
            }
            
            if (damageable.Actor != _damageActor)
            {
                if (_destroyOnCollision)
                    Destroy(gameObject);
                return;
            }
            
            damageable.Damage(_damage);
            if (_destroyOnCollision)
            {
                _collider.enabled = false;
                if (_model != null)
                    _model.SetActive(false);
                Destroy(gameObject, 3f);
            }
        }
    }
}
