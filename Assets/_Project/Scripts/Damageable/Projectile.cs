using Heroes.Damageable;
using UnityEngine;

namespace Heroes.Build.Tower
{
    public class Projectile : DamageSource
    {
        [SerializeField]
        private float _speed = 8.5f;

        private Vector3 _direction;

        private bool _wasShot = false;

        private void OnEnable()
        {
            Destroy(gameObject, 5f);
        }

        public void Shoot(Vector3 target, int damage)
        {
            _damage = damage;
            _direction = new Vector3(target.x - transform.position.x, 0, target.z - transform.position.z).normalized;
            _wasShot = true;
        }

        private void Update()
        {
            if (!_wasShot)
                return;
            
            transform.rotation = Quaternion.LookRotation(_direction, Vector3.up);
            transform.position += _direction * _speed * Time.deltaTime;
        }
    }
}
