using System.Collections;
using System.Collections.Generic;
using Heroes.Build.Tower;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes
{
    public class LongRangeDamageDealer : DamageDealer
    {
        [SerializeField]
        private Transform _shootPosition;
        [SerializeField] 
        private GameObject _projectile;

        private GameObject _currentProjectileGo;
        private Projectile _currentProjectile;

        private Vector3 _currentTargetPosition;
        
        [FoldoutGroup("Events"), SerializeField]
        private UnityEvent _onProjectileInstantiated;
        [FoldoutGroup("Events"), SerializeField]
        private UnityEvent _onProjectileShot;

        public override Transform Target { get; set; }

        public override void StartAttack()
        {
            if (_currentProjectileGo != null || _shootPosition == null || Target == null)
                return;
            
            Vector3 direction = new Vector3(Target.position.x - _shootPosition.position.x, 0, Target.position.z - _shootPosition.position.z).normalized;
            GameObject projectileObject = Instantiate(_projectile, _shootPosition.position,  Quaternion.LookRotation(direction, Vector3.up));
            _currentProjectileGo = projectileObject;
            
            if (!_currentProjectileGo.TryGetComponent(out Projectile projectile))
            {
                Destroy(_currentProjectileGo);
                return;
            }

            _currentProjectile = projectile;
            _currentTargetPosition = Target.position;
        }

        public override void DealDamage()
        {
            if (_currentProjectileGo == null || _currentProjectile == null)
                return;

            if (Target == null)
            {
                Destroy(_currentProjectileGo);
                return;
            }

            _currentProjectile.Shoot(_currentTargetPosition, _hero.runtimeStats[StatType.Damage].RuntimeValue);
            _onProjectileShot.Invoke();
            _currentProjectileGo = null;
            _currentProjectile = null;
        }

        public void DestroyProjectile()
        {
            if (_currentProjectileGo != null)
                Destroy(_currentProjectileGo);

            if (_currentProjectile == null) return;
            if (_currentProjectile.gameObject != null)
                Destroy(_currentProjectile.gameObject);

        }
    }
}
