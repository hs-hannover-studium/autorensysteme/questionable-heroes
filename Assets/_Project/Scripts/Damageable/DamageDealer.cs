using Heroes.Heroes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [RequireComponent(typeof(Hero))]
    public abstract class DamageDealer : MonoBehaviour
    {
        private protected Hero _hero;
        [ShowInInspector, ReadOnly]
        private protected Transform _target;
        
        public abstract Transform Target { get; set; }

        private void Awake()
        {
            _hero = GetComponent<Hero>();
        }

        public abstract void StartAttack();

        public abstract void DealDamage();
        
    }
}
