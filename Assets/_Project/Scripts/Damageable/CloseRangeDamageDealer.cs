using System.Collections;
using System.Collections.Generic;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes
{
    public class CloseRangeDamageDealer : DamageDealer
    {
        private Damageable.Damageable _damageable;
        
        [FoldoutGroup("Events"), SerializeField]
        private UnityEvent _onAttackStart;
        [FoldoutGroup("Events"), SerializeField]
        private UnityEvent _onImpact;
        
        public override Transform Target
        {
            get => _target;
            set
            {
                if (_target == value)
                    return;
                
                _target = value;
                _damageable = _target != null ? _target.GetComponent<Damageable.Damageable>() : null;
                    
            }
        }

        public override void StartAttack()
        {
            _onAttackStart.Invoke();
        }

        public override void DealDamage()
        {
            if (_damageable == null)
                return;
            
            _damageable.Damage(_hero.runtimeStats[StatType.Damage].RuntimeValue);
            _onImpact.Invoke();
        }
    }
}
