using Heroes.ScriptableObjects.RuntimeSets;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.Resources
{
    public class ResourceSetTracker : MonoBehaviour
    {
        [SerializeField] private ResourcesSet resourceSet;

        public UnityEvent OnResourcesChanged;

        private void OnEnable()
        {
            resourceSet.ResourceValuesChanged += OnValueChanged;
        }

        private void OnDisable()
        {
            resourceSet.ResourceValuesChanged += OnValueChanged;
        }

        private void OnValueChanged()
        {
            OnResourcesChanged?.Invoke();
        }
    }
}