using System;
using Heroes.Pathfinding;
using Heroes.ScriptableObjects.World;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Serialization;

namespace Heroes.World
{
    public enum TileType
    {
        None,

        FBRL,

        // One Open
        FRL,
        BRL,
        RFB,
        LFB,

        // Corners
        FR,
        FL,
        BR,
        BL,

        // Wall
        FB,
        RL,

        F,
        B,
        R,
        L,
    }

    [SelectionBase]
    public class Tile : SerializedMonoBehaviour
    {
        public TileData tileData;
        [ReadOnly] public TileType type;
        [ReadOnly] public TileVariation activeVariation;
        [ReadOnly] public bool isEdgeTile;

        public CorridorData corridor;
        public int corridorIndex;

        [Tooltip("Mask used to check surrounding objects")] [SerializeField]
        private LayerMask neighborMask;

        [SerializeField, ReadOnly] private GameObject occupied;
        [SerializeField, ReadOnly] private bool belowTile;

        [SerializeField] protected TileVariation oneOpen;
        [SerializeField] protected TileVariation threeOpen;
        [SerializeField] protected TileVariation street;
        [SerializeField] protected TileVariation corner;

        [FormerlySerializedAs("none")] [SerializeField]
        protected TileVariation noneOpen;

        [SerializeField] protected TileVariation allOpen;

        [TableMatrix(HorizontalTitle = "Neighbor Matrix", SquareCells = true)] [OdinSerialize]
        protected Tile[,] _neighborMatrix = new Tile[3, 3]
        {
            {null, null, null},
            {null, null, null},
            {null, null, null},
        };

        protected int _neighborCount;
        protected Collider _collider;
        protected bool _isQuitting;

        [ShowInInspector] public int NeighborCount => _neighborCount;

        public Tile[,] NeighborMatrix => _neighborMatrix;

        public Tile Front
        {
            get => _neighborMatrix[1, 0];
            set => _neighborMatrix[1, 0] = value;
        }

        public Tile Back
        {
            get => _neighborMatrix[1, 2];
            set => _neighborMatrix[1, 2] = value;
        }

        public Tile Right
        {
            get => _neighborMatrix[2, 1];
            set => _neighborMatrix[2, 1] = value;
        }

        public Tile Left
        {
            get => _neighborMatrix[0, 1];
            set => _neighborMatrix[0, 1] = value;
        }

        public Tile Top
        {
            get => _neighborMatrix[1, 1];
            set => _neighborMatrix[1, 1] = value;
        }

        public GameObject Occupied
        {
            get => occupied;
            set => occupied = value;
        }
        
        public bool BelowTile
        {
            get => belowTile;
        }

        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }

        private void Start()
        {
            UpdateNeighborMatrix();
        }

        private void OnApplicationQuit()
        {
            _isQuitting = true;
        }

        protected virtual void OnDisable()
        {
            if (_isQuitting)
                return;

            _collider.enabled = false;

            UpdateNeighbors();
        }

        protected void UpdateNeighbors()
        {
            foreach (Tile tile in _neighborMatrix)
            {
                if (!tile)
                    continue;

                tile.UpdateNeighborMatrix();
            }
            
            RaycastHit hit;
            if (Physics.Raycast(gameObject.transform.position, Vector3.down, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.TryGetComponent(out Tile tile))
                {
                    //Destroy(hit.collider.gameObject);
                    tile.UpdateBelowStatus(this);
                }
            }
        }

        public void UpdateNeighborMatrix()
        {
            Vector3 trans = transform.position;
            Vector3 scale = transform.localScale;

            Collider[] front = Physics.OverlapSphere(trans + Vector3.forward * scale.z, 0.1f, neighborMask);
            Collider[] back = Physics.OverlapSphere(trans + Vector3.back * scale.z, 0.1f, neighborMask);
            Collider[] right = Physics.OverlapSphere(trans + Vector3.right * scale.x, 0.1f, neighborMask);
            Collider[] left = Physics.OverlapSphere(trans + Vector3.left * scale.x, 0.1f, neighborMask);
            Collider[] top = Physics.OverlapSphere(trans + Vector3.up * scale.y, 0.1f, neighborMask);
            
            Collider[] below = Physics.OverlapSphere(trans + Vector3.up * scale.y, 0.1f, neighborMask);

            // Front
            if (front.Length > 0)
                Front = front[0].GetComponent<Tile>();
            else
                Front = null;

            // Back
            if (back.Length > 0)
                Back = back[0].GetComponent<Tile>();
            else
                Back = null;

            // Right
            if (right.Length > 0)
                Right = right[0].GetComponent<Tile>();
            else
                Right = null;

            // Left
            if (left.Length > 0)
                Left = left[0].GetComponent<Tile>();
            else
                Left = null;

            // Top
            if (top.Length > 0)
                Top = top[0].GetComponent<Tile>();
            else
                Top = null;
            
            UpdateBelowStatus(this);
            
            SetType();
        }

        public void UpdateBelowStatus(Tile calledfrom)
        {
            if (occupied != null && occupied.GetComponent<Tile>() != null && occupied.GetComponent<Tile>() != calledfrom)
                belowTile = true;
            else
                belowTile = false;
        }

        private void SetType()
        {
            Vector2 topFaceSize = new Vector2(0.252f, 0.252f);
            Vector2 topFaceOffset = new Vector2(-0.25f, -0.125f);

            if (Front && Back && Right && Left)
            {
                SetTileValues(noneOpen, topFaceSize, topFaceOffset, TileType.FBRL, 4);
                return;
            }

            // One Open
            topFaceSize = new Vector2(0.252f, 0.225f);
            topFaceOffset = new Vector2(-0.25f, -0.112f);

            if (Front && Right && Left)
            {
                SetTileValues(oneOpen, topFaceSize, topFaceOffset, TileType.FRL, 3, 180);
                return;
            }

            if (Back && Right && Left)
            {
                SetTileValues(oneOpen, topFaceSize, topFaceOffset, TileType.BRL, 3);
                return;
            }

            if (Right && Front && Back)
            {
                SetTileValues(oneOpen, topFaceSize, topFaceOffset, TileType.RFB, 3, -90);
                return;
            }

            if (Left && Front && Back)
            {
                SetTileValues(oneOpen, topFaceSize, topFaceOffset, TileType.LFB, 3, 90);
                return;
            }

            // Corners
            topFaceSize = new Vector2(0.227f, 0.225f);
            topFaceOffset = new Vector2(-0.237f, -0.112f);

            if (Front && Right)
            {
                SetTileValues(corner, topFaceSize, topFaceOffset, TileType.FR, 2, -180);
                return;
            }

            if (Front && Left)
            {
                SetTileValues(corner, topFaceSize, topFaceOffset, TileType.FL, 2, 90);
                return;
            }

            if (Back && Right)
            {
                SetTileValues(corner, topFaceSize, topFaceOffset, TileType.BR, 2, -90);
                return;
            }

            if (Back && Left)
            {
                SetTileValues(corner, topFaceSize, topFaceOffset, TileType.BL, 2);
                return;
            }

            // Streets
            topFaceSize = new Vector2(0.2f, 0.26f);
            topFaceOffset = new Vector2(-0.25f, -0.125f);

            if (Front && Back)
            {
                SetTileValues(street, topFaceSize, topFaceOffset, TileType.FB, 2);
                return;
            }

            if (Right && Left)
            {
                SetTileValues(street, topFaceSize, topFaceOffset, TileType.RL, 2, 90);
                return;
            }

            // Three Open
            topFaceSize = new Vector2(0.2f, 0.225f);
            topFaceOffset = new Vector2(-0.25f, -0.112f);

            if (Front)
            {
                SetTileValues(threeOpen, topFaceSize, topFaceOffset, TileType.F, 1, 180);
                return;
            }

            if (Back)
            {
                SetTileValues(threeOpen, topFaceSize, topFaceOffset, TileType.B, 1);
                return;
            }

            if (Right)
            {
                SetTileValues(threeOpen, topFaceSize, topFaceOffset, TileType.R, 1, -90);
                return;
            }

            if (Left)
            {
                SetTileValues(threeOpen, topFaceSize, topFaceOffset, TileType.L, 1, 90);
                return;
            }

            // All Open
            topFaceSize = new Vector2(0.2f, 0.2f);
            topFaceOffset = new Vector2(-0.25f, -0.125f);

            SetTileValues(allOpen, topFaceSize, topFaceOffset, TileType.None, 0);
        }

        private void SetTileValues(TileVariation variation, Vector2 topSize, Vector2 topOffset, TileType tileType,
            int count,
            float rotationAngle = 0f)
        {
            HideVariations();

            if (variation)
            {
                try
                {
                    activeVariation = variation;
                    variation.gameObject.SetActive(true);
                    variation.meshRenderer.material.SetVector("_TopSize", topSize);
                    variation.meshRenderer.material.SetVector("_TopOffset", topOffset);

                    if (isEdgeTile)
                    {
                        foreach (GameObject slot in activeVariation.slots)
                            slot.SetActive(false);
                    }
                    else
                    {
                        foreach (GameObject slot in activeVariation.slots)
                            slot.SetActive(true);
                    }
                }
                catch (Exception e)
                {
                    // ignored
                }
            }

            transform.rotation = Quaternion.identity;

            transform.Rotate(Vector3.up, rotationAngle);
            type = tileType;
            _neighborCount = count;
        }

        /// <summary>
        /// Hides all variations if they are set in inspector
        /// </summary>
        protected void HideVariations()
        {
            if (oneOpen)
                oneOpen.gameObject.SetActive(false);
            if (threeOpen)
                threeOpen.gameObject.SetActive(false);
            if (street)
                street.gameObject.SetActive(false);
            if (corner)
                corner.gameObject.SetActive(false);
            if (noneOpen)
                noneOpen.gameObject.SetActive(false);
            if (allOpen)
                allOpen.gameObject.SetActive(false);
        }
    }
}