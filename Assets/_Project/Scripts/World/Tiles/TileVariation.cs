using System.Collections.Generic;
using UnityEngine;

namespace Heroes.World
{
    public class TileVariation : MonoBehaviour
    {
        public List<GameObject> slots = new List<GameObject>();
        [HideInInspector]
        public MeshFilter meshFilter;
        [HideInInspector]
        public Renderer meshRenderer;

        private void Awake()
        {
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<Renderer>();
        }
    }
}