using UnityEngine;

namespace Heroes.World
{
    public class SpawnTile : Tile
    {
        [SerializeField]
        private GameObject spawnPoint;

        public GameObject SpawnPoint => spawnPoint;
    }
}