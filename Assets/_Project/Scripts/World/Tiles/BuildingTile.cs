using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.World
{
    public class BuildingTile : Tile
    {
        [FoldoutGroup("Events")]
        public UnityEvent OnEnter;
        [FoldoutGroup("Events")]
        public UnityEvent OnExit;
        
        [SerializeField]
        private GameObject placementSpot;

        public GameObject PlacementSpot => placementSpot;
    }
}