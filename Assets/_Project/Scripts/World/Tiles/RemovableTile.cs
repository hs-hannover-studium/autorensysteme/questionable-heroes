using System;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.RuntimeSets;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.World.Tiles
{
    public class RemovableTile : Tile
    {
        [SerializeField] private List<ResourceValuePair> cost;
        [SerializeField] private ResourcesSet resources;

        public event Action NotEnoughResources;
        public UnityEvent OnRemove;

        public List<ResourceValuePair> Cost => cost;

        protected override void OnDisable()
        {
        }
        
        public void ForceRemove()
        {
            _collider.enabled = false;

            HideVariations();
            UpdateNeighbors();

            OnRemove?.Invoke();

            Destroy(gameObject, 1f);
        }

        public bool Remove()
        {
            if (!resources.HasEnoughResources(Cost))
            {
                NotEnoughResources?.Invoke();
                return false;
            }
            
            resources.PayResources(Cost);

            _collider.enabled = false;

            HideVariations();
            UpdateNeighbors();

            OnRemove?.Invoke();

            Destroy(gameObject, 1f);

            return true;
        }
    }
}