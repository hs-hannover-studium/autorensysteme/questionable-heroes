using Heroes.Misc;
using Heroes.ScriptableObjects.RuntimeSets;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.World
{
    [RequireComponent(typeof(LevelGenerator))]
    public class LevelCamera : MonoBehaviour
    {
        [SerializeField] private GameObject cameraPrefab;
        [SerializeField] private float yOffset = 5f;
        [SerializeField] private TileSet levelCoreSet;
        [SerializeField] private Vector3 confinerScaling = new Vector3(1.1f, 1, 1.1f);

        private LevelGenerator _generator;
        private GameObject _camRig;
        private CameraController _cameraController;

        private void Awake()
        {
            _generator = GetComponent<LevelGenerator>();
        }

        private void OnEnable()
        {
            _generator.levelGenerated += CreateCamera;
        }

        private void OnDisable()
        {
            _generator.levelGenerated -= CreateCamera;
        }

        [Button]
        public void CreateCamera()
        {
            _camRig = Instantiate(cameraPrefab, Vector3.zero,
                cameraPrefab.transform.rotation, transform);

            _cameraController = _camRig.GetComponentInChildren<CameraController>();

            BoxCollider confiner = CreateLevelConfiner();

            Vector3 cameraPosition = new Vector3(levelCoreSet[0].transform.position.x, 5,
                levelCoreSet[0].transform.position.z);
            cameraPosition -= _camRig.transform.forward * 15;
            _camRig.transform.position = cameraPosition;
            _cameraController.confiner = confiner;
        }

        private BoxCollider CreateLevelConfiner()
        {
            _generator = GetComponent<LevelGenerator>();

            GameObject empty = new GameObject("LevelConfiner", typeof(BoxCollider));
            empty.transform.parent = transform;

            BoxCollider confiner = empty.GetComponent<BoxCollider>();

            float colliderHeight = Mathf.Sqrt(_generator.levelSize.x + _generator.levelSize.y) * 6;

            confiner.size = new Vector3(_generator.levelSize.x * confinerScaling.x, colliderHeight * confinerScaling.y,
                _generator.levelSize.z * confinerScaling.z);
            confiner.center = new Vector3(_generator.levelCenter.x, yOffset + colliderHeight / 2,
                _generator.levelCenter.z);

            _cameraController.AddRotationCurveKey(new Vector2(colliderHeight / 2, 45));
            _cameraController.AddRotationCurveKey(new Vector2(colliderHeight, 90));

            return confiner;
        }
    }
}