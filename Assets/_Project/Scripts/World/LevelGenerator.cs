using System;
using System.Collections.Generic;
using Heroes.Extensions;
using Heroes.ScriptableObjects.World;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Heroes.World
{
    public class LevelGenerator : MonoBehaviour
    {
        [ReadOnly] public Vector3 levelSize;

        [ReadOnly] public Vector3 levelCenter;

        public Level[] level;
        public GameSession gameSession;

        [HideInInspector] public List<Tile> tiles;
        private NavMeshSurface _navMeshSurface;

        public event Action levelGenerated;
        
        [SerializeField] 
        private UnityEvent OnLevelGenerated;

        [SerializeField] 
        private GameObject directionalLightPrefab;

        private Light directionalLight;

        private void Awake()
        {
            _navMeshSurface = GetComponent<NavMeshSurface>();
        }

        private void Start()
        {
            SpawnRandom();
        }

        [Button]
        public void SpawnRandom()
        {
            int random = Random.Range(0, level.Length);

            Spawn(level[random]);
        }

        [Button]
        public void Spawn(Level level)
        {
            RemoveChildren();

            gameSession.Difficulty = level.difficulty;

            directionalLight = Instantiate(directionalLightPrefab, transform.position, Quaternion.identity, transform).GetComponent<Light>();
            
            directionalLight.intensity = level.lightIntensity;
            directionalLight.color = level.lightColor;
            directionalLight.transform.rotation = Quaternion.Euler(level.lightRotation);
            
            tiles = new List<Tile>();

            for (var y = 0; y < level.texture.height; y++)
            {
                for (int x = 0; x < level.texture.width; x++)
                {
                    Color color = level.texture.GetPixel(x, y);

                    TileData block = GetBlock(color, level);
                    Vector3 blockLocalScale = block.prefab.transform.localScale;

                    GameObject currentBlock = Instantiate(block.prefab,
                        new Vector3(x * blockLocalScale.x, blockLocalScale.y * block.heightLevel, y * blockLocalScale.z), Quaternion.identity,
                        transform);

                    Tile currentTile = currentBlock.GetComponent<Tile>();

                    if (block.heightLevel == 1)
                    {
                        GameObject blockToSpawn;
                        
                        if (block.tileBeneath)
                        {
                            blockToSpawn = Instantiate(block.tileBeneath,
                                new Vector3(x * blockLocalScale.x, 0, y * blockLocalScale.z),
                                Quaternion.identity, transform);
                        }
                        else
                        {
                            blockToSpawn = Instantiate(level.defaultBlock.prefab,
                                new Vector3(x * blockLocalScale.x, 0, y * blockLocalScale.z),
                                Quaternion.identity, transform);
                        }
                        

                        blockToSpawn.name = $"{blockToSpawn.name}: {x} : {y}";

                        Tile defaultTile = blockToSpawn.GetComponent<Tile>();
                        defaultTile.Occupied = currentBlock;

                        tiles.Add(defaultTile);
                    }
                    
                    // Tile is on an edge of the map
                    if (x == level.texture.width - 1 || x == 0 || y == level.texture.height - 1 || y == 0)
                    {
                        currentTile.isEdgeTile = true;
                    }

                    currentBlock.name = $"{currentBlock.name}: {x} : {y}";
                    tiles.Add(currentTile);
                }
            }

            if (_navMeshSurface)
            {
                _navMeshSurface.BuildNavMesh();
            }

            Vector3 blockSize = level.defaultBlock.prefab.transform.localScale;

            levelSize = new Vector3(level.texture.width * blockSize.x, blockSize.y, level.texture.height * blockSize.z);
            levelCenter = new Vector3(levelSize.x / 2, 0, levelSize.z / 2);

            levelGenerated?.Invoke();
            OnLevelGenerated?.Invoke();
        }

        [Button]
        public void RemoveChildren()
        {
            transform.RemoveChildrenImmediate();
        }

        private TileData GetBlock(Color color, Level level)
        {
            foreach (TileData block in level.blocks)
            {
                if (ColorEqual(block.color, color))
                    return block;
            }

            return level.defaultBlock;
        }

        private bool ColorEqual(Color color1, Color color2)
        {
            float threshold = 0.1f;
            return (Mathf.Abs(color1.r - color2.r) < threshold
                    && Mathf.Abs(color1.g - color2.g) < threshold
                    && Mathf.Abs(color1.b - color2.b) < threshold);
        }
    }
}