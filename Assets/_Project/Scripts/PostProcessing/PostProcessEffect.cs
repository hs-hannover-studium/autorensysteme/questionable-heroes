using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;

namespace Heroes.PostProcessing
{
    public class PostProcessEffect : SerializedMonoBehaviour
    {
        [SerializeField] private int priority;
        [SerializeField] private Volume volume;
        [SerializeField] private IPostProcessEffect _effect;

        public void Apply()
        {
            _effect.Apply(volume, priority);
        }

        public void Remove()
        {
            _effect.Remove(volume, priority);
        }
    }
}