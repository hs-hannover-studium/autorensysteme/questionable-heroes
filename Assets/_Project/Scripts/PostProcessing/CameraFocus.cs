using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Heroes.PostProcessing
{
    public class CameraFocus : IPostProcessEffect
    {
        [SerializeField] private float _distortionTargetIntensity = 0.25f;
        [SerializeField] private float _chromaticTargetIntensity = 0.25f;
        [SerializeField] private float _resetIntensity = 0.0f;
        [SerializeField] private float _fadeDuration = 0.25f;
        
        private LensDistortion _distortion;
        private ChromaticAberration _chromatic;
        
        public void Apply(Volume volume, int priority)
        {
            if (!volume.profile.TryGet(out _distortion))
                volume.profile.components.Add(_distortion);
            
            if (!volume.profile.TryGet(out _chromatic))
                volume.profile.components.Add(_chromatic);

            volume.priority = priority;
            _distortion.intensity.value = _distortionTargetIntensity;
            _chromatic.intensity.value = _chromaticTargetIntensity;
            volume.weight = 1;
        }

        public void Remove(Volume volume, int priority)
        {
            Tween fadeDistortion = DOTween.To(() => _distortion.intensity.value, x => _distortion.intensity.value = x,
                _resetIntensity,
                _fadeDuration);
            Tween fadeChromatic = DOTween.To(() => _chromatic.intensity.value, x => _chromatic.intensity.value = x,
                _resetIntensity,
                _fadeDuration);
            Tween fadeWeight = DOTween.To(() => volume.weight, x => volume.weight = x,
                0,
                _fadeDuration);

            Sequence sequence = DOTween.Sequence();

            sequence.Append(fadeDistortion);
            sequence.Join(fadeChromatic);
            sequence.Join(fadeWeight);

            sequence.OnComplete(() => { volume.priority = 0; });
        }
    }
}