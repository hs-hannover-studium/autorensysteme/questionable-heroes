using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Heroes.PostProcessing
{
    public class VignetteEffectStrategy : IPostProcessEffect
    {
        [SerializeField] private float _fadeDuration = 0.25f;
        [SerializeField] private Ease _fadeEase = Ease.Linear;
        [SerializeField] private Color _color = Color.black;
        [SerializeField] private float _targetIntensity = 0.25f;
        [SerializeField] private float _resetIntensity = 0.0f;

        private Vignette _component;

        public void Apply(Volume volume, int priority)
        {
            if (!volume.profile.TryGet(out _component))
                volume.profile.components.Add(_component);

            volume.priority = priority;

            _component.color.value = _color;

            Tween fadeIntensity = DOTween.To(() => _component.intensity.value, x => _component.intensity.value = x,
                _targetIntensity,
                _fadeDuration).SetEase(_fadeEase);
            Tween fadeWeight = DOTween.To(() => volume.weight, x => volume.weight = x,
                1,
                _fadeDuration).SetEase(_fadeEase);

            Sequence sequence = DOTween.Sequence();

            sequence.Append(fadeIntensity);
            sequence.Join(fadeWeight);
        }

        public void Remove(Volume volume, int priority)
        {
            Tween fadeIntensity = DOTween.To(() => _component.intensity.value, x => _component.intensity.value = x,
                _resetIntensity,
                _fadeDuration).SetEase(_fadeEase);
            Tween fadeWeight = DOTween.To(() => volume.weight, x => volume.weight = x,
                0,
                _fadeDuration).SetEase(_fadeEase);

            Sequence sequence = DOTween.Sequence();

            sequence.Append(fadeIntensity);
            sequence.Join(fadeWeight);

            sequence.OnComplete(() => { volume.priority = 0; });
        }
    }
}