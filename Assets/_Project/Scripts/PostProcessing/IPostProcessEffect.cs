using UnityEngine.Rendering;

namespace Heroes.PostProcessing
{
    public interface IPostProcessEffect
    {
        public void Apply(Volume volume, int priority);
        public void Remove(Volume volume, int priority);
    }
}