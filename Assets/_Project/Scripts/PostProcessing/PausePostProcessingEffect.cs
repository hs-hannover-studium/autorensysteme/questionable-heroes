using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Heroes.PostProcessing
{
    public class PausePostProcessingEffect : IPostProcessEffect
    {
        [SerializeField] private float _fadeDuration = 0.25f;
        [SerializeField] private float _targetSaturation = -100f;
        [SerializeField] private Color _color = Color.black;
        [SerializeField] private float _targetIntensity = 0.25f;
        [SerializeField] private float _resetIntensity = 0.0f;

        private Vignette _vignette;
        private ColorAdjustments _adjustments;

        public void Apply(Volume volume, int priority)
        {
            if (!volume.profile.TryGet(out _vignette))
                volume.profile.components.Add(_vignette);
            if (!volume.profile.TryGet(out _adjustments))
                volume.profile.components.Add(_adjustments);

            volume.priority = priority;

            _vignette.color.value = _color;

            Tween fadeIntensity = DOTween.To(() => _vignette.intensity.value, x => _vignette.intensity.value = x,
                _targetIntensity,
                _fadeDuration);
            Tween fadeAdjustment = DOTween.To(() => _adjustments.saturation.value, x => _adjustments.saturation.value = x,
                _targetSaturation,
                _fadeDuration);
            Tween fadeWeight = DOTween.To(() => volume.weight, x => volume.weight = x,
                1,
                _fadeDuration);

            Sequence sequence = DOTween.Sequence();

            sequence.Append(fadeIntensity);
            sequence.Join(fadeAdjustment);
            sequence.Join(fadeWeight);
        }

        public void Remove(Volume volume, int priority)
        {
            Tween fadeIntensity = DOTween.To(() => _vignette.intensity.value, x => _vignette.intensity.value = x,
                _resetIntensity,
                _fadeDuration);
            Tween fadeAdjustment = DOTween.To(() => _adjustments.saturation.value, x => _adjustments.saturation.value = x,
                _targetSaturation,
                0);
            Tween fadeWeight = DOTween.To(() => volume.weight, x => volume.weight = x,
                0,
                _fadeDuration);

            Sequence sequence = DOTween.Sequence();

            sequence.Append(fadeIntensity);
            sequence.Join(fadeAdjustment);
            sequence.Join(fadeWeight);

            sequence.OnComplete(() => { volume.priority = 0; });
        }
    }
}