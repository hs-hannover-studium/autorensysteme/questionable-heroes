using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.TimeManagement
{
    public class TimeChanger : MonoBehaviour
    {
        [Button]
        public void SetTime(float value)
        {
            TimeController.SetTime(value);
        }

        [Button]
        public void StopTime()
        {
            TimeController.StopTime();
        }

        [Button]
        public void ResetTime()
        {
            TimeController.ResetTime();
        }

        [Button]
        public void IncreaseTime(float value = 0.25f)
        {
            TimeController.IncreaseTime(value);
        }

        [Button]
        public void DecreaseTime(float value = 0.25f)
        {
            TimeController.DecreaseTime(value);
        }
    }
}