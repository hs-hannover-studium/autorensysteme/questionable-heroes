using Heroes.PlayerInputs;
using Heroes.ScriptableObjects.Events;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Heroes.TimeManagement
{
    public class TimePlayerControls : MonoBehaviour
    {
        [SerializeField] private GameEvent enterPause;
        [SerializeField] private GameEvent exitPause;

        private bool _isPaused;

        private void OnEnable()
        {
            PlayerInputController.Instance.inputActions.Gameplay.PauseMenu.performed += TogglePause;
        }

        private void OnDisable()
        {
            PlayerInputController.Instance.inputActions.Gameplay.PauseMenu.performed -= TogglePause;
        }

        private void TogglePause(InputAction.CallbackContext obj)
        {
            if (_isPaused)
            {
                exitPause.Raise();
                _isPaused = false;
            }
            else
            {
                enterPause.Raise();
                _isPaused = true;
            }
        }
    }
}