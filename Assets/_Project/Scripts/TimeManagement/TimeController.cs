using System;
using UnityEngine;

namespace Heroes.TimeManagement
{
    [Serializable]
    public static class TimeController
    {
        public static void SetTime(float value)
        {
            Time.timeScale = value;
        }

        public static void StopTime()
        {
            Time.timeScale = 0;
        }

        public static void ResetTime()
        {
            Time.timeScale = 1;
        }

        public static void IncreaseTime(float value = 0.25f)
        {
            Time.timeScale += value;
        }

        public static void DecreaseTime(float value = 0.25f)
        {
            Time.timeScale -= value;
        }
    }
}