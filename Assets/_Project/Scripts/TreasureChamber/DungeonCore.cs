using System;
using Heroes.ScriptableObjects.Values;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes
{
    public class DungeonCore : MonoBehaviour
    {
        [SerializeField] private ResourceValue _core;

        [SerializeField] private GameObject _completeState;
        [SerializeField] private GameObject _brokenState;
        [SerializeField] private GameObject _veryBrokenState;

        public event Action CoreDamaged;
        public event Action CoreDestroyed;
        
        [FoldoutGroup("Events"), SerializeField] 
        private UnityEvent OnCoreDamaged;
        [FoldoutGroup("Events"), SerializeField] 
        private UnityEvent OnCoreDetroyed;

        private void Awake()
        {
            _completeState.SetActive(true);
            _brokenState.SetActive(false);
            _veryBrokenState.SetActive(false);
        }
        
        private void OnEnable()
        {
            _core.ValueChanged += OnCoreValueChanged;
        }

        private void OnDisable()
        {
            _core.ValueChanged -= OnCoreValueChanged;
        }

        private void OnCoreValueChanged(int throwAwayValue)
        {
            if (_core.RuntimeValue < _core.DefaultValue / 2.0f)
            {
                _completeState.SetActive(false);

                if (_core.RuntimeValue < _core.DefaultValue / 5.0f)
                {
                    _brokenState.SetActive(false);
                    _veryBrokenState.SetActive(true);
                }
                else
                {
                    _veryBrokenState.SetActive(false);
                    _brokenState.SetActive(true);
                }
                
            }
            
            if (_core.RuntimeValue <= 0)
            {
                CoreDestroyed?.Invoke();
                OnCoreDetroyed.Invoke();
                return;
            }
            CoreDamaged?.Invoke();
            OnCoreDamaged.Invoke();
        }

        [Button, FoldoutGroup("Debug")]
        private void DamageCore(int amount)
        {
            if (amount <= 0)
                return;

            _core.RuntimeValue -= amount;
        }

        [Button, FoldoutGroup("Debug")]
        private void DestroyCore()
        {
            _core.RuntimeValue = 0;
        }
    }
}
