using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.ScriptableObjects.Values;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI
{
    public class ListItemBuilding : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private Image _icon;
        [SerializeField] private Button removeBtn;
        [SerializeField] private UiResourceCostView _resourceCostView;
        [SerializeField] private ResourcesSet resourceSet;


        private List<ResourceValuePair> _cost;

        public List<ResourceValuePair> Cost
        {
            get => _cost;
            set
            {
                _cost = value;
                _resourceCostView.UpdateCostView(_cost);
            }
        }

        public string Name
        {
            get => _name.text;
            set => _name.SetText(value);
        }

        public Sprite Icon
        {
            get => _icon.sprite;
            set => _icon.sprite = value;
        }
        
        private void OnEnable()
        {
            resourceSet.ResourceValuesChanged += UpdateRemoveButton;
        }

        private void OnDisable()
        {
            resourceSet.ResourceValuesChanged += UpdateRemoveButton;
        }

        public void UpdateRemoveButton()
        {
            if (!removeBtn)
                return;
            
            removeBtn.interactable = resourceSet.HasEnoughResources(_cost);
        }
    }
}