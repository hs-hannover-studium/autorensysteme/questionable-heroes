using System;
using Heroes.Heroes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI.AttackerList
{
    [RequireComponent(typeof(Button))]
    public class UiAttackerSlotView : MonoBehaviour
    {
        [SerializeField] private Image portrait;
        [SerializeField] private PortraitHPBar hpBar;
        [SerializeField] private TextMeshProUGUI nameTxt;
        [SerializeField] private UiSpeechBubble speechBubble;

        private SpeechBubbleCaller _speechBubbleCaller;
        private Button _button;
        private Damageable.Damageable _damageable;

        public Hero Attacker
        {
            set
            {
                if (value == null)
                    return;
                
                portrait.sprite = value.Data.Icon;
                
                _damageable = value.GetComponent<Damageable.Damageable>();
                
                _damageable.Died += OnDeath;
                
                if (hpBar.TryGetComponent(out Image image))
                {
                    image.sprite = value.Data.Icon;
                }
                    
                hpBar.Damageable = _damageable;
                nameTxt.SetText(value.HeroName);

                if (!value.TryGetComponent(out SpeechBubbleCaller speechBubbleCaller)) 
                    return;
                
                _speechBubbleCaller = speechBubbleCaller;
                _speechBubbleCaller.speechBubbleEvent += OnSpeechBubbleEvent;
            }
        }
        
        private void Awake()
        {
            _button = GetComponent<Button>();
        }

        private void OnDestroy()
        {
            if (_damageable != null)
                _damageable.Died -= OnDeath;
            
            if (_speechBubbleCaller != null)
                _speechBubbleCaller.speechBubbleEvent -= OnSpeechBubbleEvent;
        }

        private void OnDeath()
        {
            _button.interactable = false;
        }

        private void OnSpeechBubbleEvent(SpeechBubbleCollection speechBubbleCollection)
        {
            speechBubble.StartSpeechBubble(speechBubbleCollection);
        }
    }
}