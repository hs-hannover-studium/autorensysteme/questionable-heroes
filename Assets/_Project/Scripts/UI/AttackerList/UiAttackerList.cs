using Heroes.Extensions;
using Heroes.Heroes;
using Heroes.ScriptableObjects.Values;
using UnityEngine;
using UnityEngine.UI;
using Selectable = Heroes.Selection.Selectable;

namespace Heroes.UI.AttackerList
{
    public class UiAttackerList : UiList<Hero>
    {
        [SerializeField] private int maxSlots = 8;
        [SerializeField] private SelectionValue selectionValue;

        private void OnEnable()
        {
            items.ValueChanged += Show;
        }

        private void OnDisable()
        {
            items.ValueChanged -= Show;
        }

        protected override void Start()
        {
        }

        private void Show(Hero h)
        {
            transform.RemoveChildren();
            
            for (var i = 0; i < items.Count; i++)
            {
                if (i == maxSlots)
                    break;
                
                GameObject item = Instantiate(listItem, transform.position, Quaternion.identity, transform);
                Button btn = item.GetComponent<Button>();
                
                InitItemUI(items[i], item);

                btn.AddEventListener(i, ItemClicked);
            }
        }

        protected override void InitItemUI(Hero data, GameObject o)
        {
            UiAttackerSlotView view = o.GetComponent<UiAttackerSlotView>();

            view.Attacker = data;
        }

        protected override void ItemClicked(int i)
        {
            selectionValue.RuntimeValue = items[i].gameObject.GetComponent<Selectable>();
        }
    }
}