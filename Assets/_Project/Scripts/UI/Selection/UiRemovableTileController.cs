using Heroes.ScriptableObjects.World;
using Heroes.Selection;
using Heroes.World.Tiles;

namespace Heroes.UI.Selection
{
    public class UiRemovableTileController : UiSelectionController<RemovableTile, TileData, UiRemovableTileView>
    {
        protected override void OnSelectionChanged(Selectable s)
        {
            if (!s)
            {
                view.gameObject.SetActive(false);
                return;
            }
            
            if (s.Type != SelectionType.RemovableTile)
            {
                view.gameObject.SetActive(false);
                return;
            }
            
            type = s.gameObject.GetComponent<RemovableTile>();
            data = type.tileData;
            
            UpdateUI();
            
            view.gameObject.SetActive(true);
        }

        public void Remove()
        {
            if (type.Remove())
                selectionValue.Deselect();
            
        }

        protected override void UpdateUI()
        {
            view.NameTxt = data.name;
            view.Cost = type.Cost;
            view.UpdateRemoveButton();
        }
    }
}