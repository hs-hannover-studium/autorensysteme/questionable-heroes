using System;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.ScriptableObjects.Values;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI.Selection
{
    public class UiTowerView : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameTxt;
        [SerializeField] private TextMeshProUGUI levelTxt;
        [SerializeField] private TextMeshProUGUI healthTxt;
        [SerializeField] private TextMeshProUGUI attackTxt;
        [SerializeField] private TextMeshProUGUI rangeTxt;
        [SerializeField] private Button upgradeBtn;
        [SerializeField] private UiResourceCostView upgradeCostView;
        [SerializeField] private UiResourceCostView sellResourcesView;
        [SerializeField] private ResourcesSet resourceSet;
        
        private List<ResourceValuePair> _upgradeCost;
        private List<ResourceValuePair> _sellResources;

        private int _level;
        private int _maxLevel;
        
        public Sprite Icon
        {
            get => icon.sprite;
            set => icon.sprite = value;
        }

        public String NameTxt
        {
            get => nameTxt.text;
            set => nameTxt.SetText(value);
        }
        
        public int Level
        {
            get => _level;
            set
            {
                _level = value;
                levelTxt.SetText(value.ToString());
            }
        }

        public String HealthTxt
        {
            get => healthTxt.text;
            set => healthTxt.SetText(value);
        }
        
        public String AttackTxt
        {
            get => attackTxt.text;
            set => attackTxt.SetText(value);
        }
        
        public String RangeTxt
        {
            get => rangeTxt.text;
            set => rangeTxt.SetText(value);
        }
        
        public List<ResourceValuePair> UpgradeCost
        {
            get => _upgradeCost;
            set
            {
                _upgradeCost = value;
                upgradeCostView.UpdateCostView(_upgradeCost);
            }
        }
        
        public List<ResourceValuePair> SellResources
        {
            get => _sellResources;
            set
            {
                _sellResources = value;
                sellResourcesView.UpdateCostView(_sellResources);
            }
        }

        public int MaxLevel
        {
            get => _maxLevel;
            set => _maxLevel = value;
        }

        private void OnEnable()
        {
            resourceSet.ResourceValuesChanged += UpdateUpgradeButton;
        }

        private void OnDisable()
        {
            resourceSet.ResourceValuesChanged += UpdateUpgradeButton;
        }

        public void UpdateUpgradeButton()
        {
            if (!upgradeBtn)
                return;
            
            
            upgradeBtn.interactable = resourceSet.HasEnoughResources(_upgradeCost) &&
                                      (_level<_maxLevel);
        }
    }
}