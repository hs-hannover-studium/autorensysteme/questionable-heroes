using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI.Selection
{
    public class UiHeroView : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameTxt;
        [SerializeField] private TextMeshProUGUI levelTxt;
        [SerializeField] private UiHeroStatsView heroStatsView;
        
        public Sprite Icon
        {
            get => icon.sprite;
            set => icon.sprite = value;
        }

        public String NameTxt
        {
            get => nameTxt.text;
            set => nameTxt.SetText(value);
        }
        
        public String LevelTxt
        {
            get => levelTxt.text;
            set => levelTxt.SetText(value);
        }
        
        public UiHeroStatsView StatsView => heroStatsView;
    }
}