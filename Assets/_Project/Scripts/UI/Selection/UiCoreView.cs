using System;
using TMPro;
using UnityEngine;

namespace Heroes.UI.Selection
{
    public class UiCoreView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameTxt;


        public String NameTxt
        {
            get => nameTxt.text;
            set
            {
                nameTxt.SetText(value);
            }
        }
    }
}