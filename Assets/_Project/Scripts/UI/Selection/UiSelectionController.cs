using Heroes.ScriptableObjects.Values;
using Heroes.Selection;
using UnityEngine;

namespace Heroes.UI.Selection
{
    public abstract class UiSelectionController<Type, Data, View> : MonoBehaviour
    {
        [SerializeField] protected SelectionValue selectionValue;
        [SerializeField] protected View view;
        protected Type type;
        protected Data data;

        private void OnEnable()
        {
            selectionValue.ValueChanged += OnSelectionChanged;
        }

        private void OnDisable()
        {
            selectionValue.ValueChanged -= OnSelectionChanged;
        }

        protected abstract void OnSelectionChanged(Selectable s);

        protected abstract void UpdateUI();
    }
}