using System.Collections.Generic;
using Heroes.Build.Tower;
using Heroes.Damageable;
using Heroes.Misc;
using Heroes.ScriptableObjects.Buildings;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.ScriptableObjects.Values;
using Heroes.Selection;
using Heroes.Stats;
using UnityEngine;

namespace Heroes.UI.Selection
{
    public class UiTowerController : UiSelectionController<Tower, TowerData, UiTowerView>
    {
        [SerializeField] private ResourcesSet _resourcesSet;
        
        private BuildingDamageable _damageable;

        public void Upgrade()
        {
            type.runtimeStats.LevelUp();
        }

        public void Demolish()
        {
            _resourcesSet.AddResources(type.runtimeStats.SellResources);
            _damageable.Kill();
        }

        protected override void OnSelectionChanged(Selectable s)
        {
            if (!s)
            {
                view.gameObject.SetActive(false);
                return;
            }

            if (s.Type != SelectionType.Tower)
            {
                UnsubscribeValues();
                view.gameObject.SetActive(false);
                return;
            }
            
            type = s.gameObject.GetComponent<Tower>();
            _damageable = type.gameObject.GetComponent<BuildingDamageable>();
            data = (TowerData) type.Data;
            
            SubscribeValues();
            UpdateUI();
            
            view.gameObject.SetActive(true);
        }

        protected override void UpdateUI()
        {
            view.Icon = data.Icon;
            view.NameTxt = data.BuildingName;
            view.HealthTxt = _damageable.Health + "/" + _damageable.MaxHealth;
            view.AttackTxt = type.runtimeStats[StatType.Damage].RuntimeValue.ToString();
            view.RangeTxt = type.runtimeStats[StatType.ATKRange].RuntimeValue.ToString();

            view.Level = type.runtimeStats.Level;
            view.MaxLevel = data.MaxLevel;

            List<ResourceValuePair> upgradeCost = new List<ResourceValuePair>();
            
            // Create ResourceValuePair list for upgrade costs
            foreach (ResourceScaling resourceScaling in data.levelCost)
            {
                ResourceValue resourceType = resourceScaling.range.RangeType;
                int costValue = resourceScaling.CalculateMinMaxScaled(type.runtimeStats.Level).x;
                upgradeCost.Add(new ResourceValuePair(resourceType, costValue));
            }

            view.UpgradeCost = upgradeCost;
            view.UpdateUpgradeButton();

            view.SellResources = type.runtimeStats.SellResources;
        }
        
        private void SubscribeValues()
        {
            type.runtimeStats.StatsChanged += UpdateUI;
            _damageable.HealthChanged += UpdateUI;
            _damageable.Died += OnDeath;
        }

        private void UnsubscribeValues()
        {
            if (!type)
                return;
            
            type.runtimeStats.StatsChanged -= UpdateUI;
            _damageable.HealthChanged -= UpdateUI;
        }

        private void OnDeath()
        {
            _damageable.Died -= OnDeath;
            selectionValue.Deselect();
        }
    }
}