using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.ScriptableObjects.Values;
using Heroes.World;
using UnityEngine;
using Selectable = Heroes.Selection.Selectable;

namespace Heroes.UI.Selection
{
    public class UiCoreSelector : MonoBehaviour
    {
        public RuntimeSet<Tile> coreTiles;
        public SelectionValue selectionValue;

        private Selectable _selectable;

        public void Select()
        {
            _selectable = coreTiles[0].GetComponent<Selectable>();
            selectionValue.RuntimeValue = _selectable;
        }
    }
}