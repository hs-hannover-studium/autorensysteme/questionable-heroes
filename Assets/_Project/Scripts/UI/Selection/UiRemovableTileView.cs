using System;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.RuntimeSets;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI.Selection
{
    public class UiRemovableTileView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameTxt;
        [SerializeField] private Button removeBtn;
        [SerializeField] private ResourcesSet resourceSet;
        
        private List<ResourceValuePair> _cost;

        public String NameTxt
        {
            get => nameTxt.text;
            set
            {
                nameTxt.SetText(value);
            }
        }

        public List<ResourceValuePair> Cost
        {
            get => _cost;
            set
            {
                _cost = value;
                // TODO: ADD UI
                // Foreach element in cost add UI Cost icon + price
            }
        }

        private void OnEnable()
        {
            resourceSet.ResourceValuesChanged += UpdateRemoveButton;
        }

        private void OnDisable()
        {
            resourceSet.ResourceValuesChanged += UpdateRemoveButton;
        }

        public void UpdateRemoveButton()
        {
            if (!removeBtn)
                return;
            
            removeBtn.interactable = resourceSet.HasEnoughResources(_cost);
        }
    }
}