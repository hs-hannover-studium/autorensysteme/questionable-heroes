using Heroes.Damageable;
using Heroes.Heroes;
using Heroes.Misc;
using Heroes.ScriptableObjects;
using Heroes.ScriptableObjects.Values;
using Heroes.Selection;
using Heroes.Stats;
using UnityEngine;

namespace Heroes.UI.Selection
{
    public class UiHeroController : UiSelectionController<Hero, HeroData, UiHeroView>
    {
        [SerializeField] private ResourceValue dungeonCore;
        [SerializeField] private ResourceValue treasureGold;

        private HeroDamageable _damageable;

        public void Upgrade()
        {
            type.runtimeStats.LevelUp();
        }

        public void Demolish()
        {
            _damageable.Kill();
        }

        protected override void OnSelectionChanged(Selectable s)
        {
            if (!s)
            {
                view.gameObject.SetActive(false);
                return;
            }
            
            if (s.Type != SelectionType.Unit)
            {
                UnsubscribeValues();
                view.gameObject.SetActive(false);
                return;
            }
            
            type = s.gameObject.GetComponent<Hero>();
            _damageable = type.gameObject.GetComponent<HeroDamageable>();
            data = type.Data;
            
            SubscribeValues();
            UpdateUI();
            
            view.gameObject.SetActive(true);
        }

        protected override void UpdateUI()
        {
            view.Icon = data.Icon;
            view.NameTxt = type.HeroName;
            view.LevelTxt = type.runtimeStats.Level.ToString();
            view.StatsView.HealthTxt = _damageable.Health + "/" + _damageable.MaxHealth;
            view.StatsView.AttackTxt = type.runtimeStats[StatType.Damage].RuntimeValue.ToString();
            view.StatsView.RangeTxt = type.runtimeStats[StatType.ATKRange].RuntimeValue.ToString();
            view.StatsView.CoreTxt = type.runtimeStats[dungeonCore].RuntimeValue.ToString();
            view.StatsView.TreasureTxt = type.runtimeStats[treasureGold].RuntimeValue.ToString();
        }
        
        private void SubscribeValues()
        {
            type.runtimeStats.StatsChanged += UpdateUI;
            _damageable.HealthChanged += UpdateUI;
            _damageable.Died += OnDeath;
        }

        private void UnsubscribeValues()
        {
            if (!type)
                return;
            
            type.runtimeStats.StatsChanged -= UpdateUI;
            _damageable.HealthChanged -= UpdateUI;
        }

        private void OnDeath()
        {
            _damageable.Died -= OnDeath;
            selectionValue.Deselect();
        }
    }
}