using Heroes.Buildings;
using Heroes.ScriptableObjects.Buildings;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.Selection;
using Heroes.World;
using UnityEngine;

namespace Heroes.UI.Selection
{
    public class UiCoreController : UiSelectionController<Building, CoreData, UiCoreView>
    {
        [SerializeField] private RuntimeSet<Tile> coreTiles; 
        
        protected override void OnSelectionChanged(Selectable s)
        {
            if (!s)
            {
                view.gameObject.SetActive(false);
                return;
            }
            
            if (s.Type != SelectionType.Core)
            {
                view.gameObject.SetActive(false);
                return;
            }
            
            type = s.gameObject.GetComponent<Building>();
            data = (CoreData) type.Data;
            
            UpdateUI();
            
            view.gameObject.SetActive(true);
        }

        protected override void UpdateUI()
        {
            view.NameTxt = data.name;
        }
    }
}