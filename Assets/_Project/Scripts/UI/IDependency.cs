using System;

namespace Heroes.UI
{
    public interface IDependency
    {
        public event Action DependencyCompleted;
    }
}