using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes
{
    [RequireComponent(typeof(Image))]
    public abstract class FillableBar : MonoBehaviour
    {
        private Image _fillImage;

        private void Awake()
        {
            _fillImage = GetComponent<Image>();
        }

        private protected void SetValueUsingMax(int current, int max)
        {
            _fillImage.fillAmount = (float) current / max;
        }
    }
}
