using Heroes.ScriptableObjects.Values;
using UnityEngine;

namespace Heroes
{
    public class DungeonCoreBar : FillableBar
    {
        [SerializeField]
        private ResourceValue dungeonCore;

        private void OnEnable()
        {
            dungeonCore.ValueChanged += UpdateValue;
        }

        private void OnDisable()
        {
            dungeonCore.ValueChanged -= UpdateValue;
        }

        private void UpdateValue(int throwAwayValue)
        {
            SetValueUsingMax(dungeonCore.RuntimeValue, dungeonCore.DefaultValue);
        }
    }
}
