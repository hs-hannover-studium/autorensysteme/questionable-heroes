using System.Collections;
using System.Collections.Generic;
using Heroes.Damageable;
using UnityEngine;

namespace Heroes
{
    public class UiWorldController : MonoBehaviour
    {
        [SerializeField] private Damageable.Damageable _damageable;
        [SerializeField] private HPBar _hpBar;

        private void OnEnable()
        {
            _damageable.Damaged += OnDamaged;
            _damageable.Died += OnDamaged;
        }

        private void OnDisable()
        {
            _damageable.Damaged -= OnDamaged;
            _damageable.Died -= OnDamaged;
        }
        
        private void OnDamaged()
        {
            _hpBar.Show(_damageable);
        }
        
    }
}
