using System.Collections;
using Heroes.Damageable;
using UnityEngine;

namespace Heroes
{
    public class HPBar : FillableBar
    {
        public float secondsUntilDisable = 5f;

        private Coroutine _waitToDisableCoroutine;

        public void Show(Damageable.Damageable damageable)
        {
            transform.parent.gameObject.SetActive(true);
            UpdateValue(damageable);
            if (_waitToDisableCoroutine != null)
                StopCoroutine(_waitToDisableCoroutine);
            _waitToDisableCoroutine = StartCoroutine(ShowHealthBar());
        }

        private IEnumerator ShowHealthBar()
        {
            yield return new WaitForSeconds(secondsUntilDisable);
            transform.parent.gameObject.SetActive(false);
        }
        
        private void UpdateValue(Damageable.Damageable damageable)
        {
            SetValueUsingMax(damageable.hpStat.RuntimeValue, damageable.hpStat.DefaultValue);
        }
    }
}
