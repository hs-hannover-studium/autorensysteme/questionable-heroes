using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    public class PortraitHPBar : FillableBar
    {
        [SerializeField] private Damageable.Damageable _damageable;
        
        [ReadOnly]
        public Damageable.Damageable Damageable
        {
            set
            {
                if (value == null)
                    return;
                
                _damageable = value;
                _damageable.HealthChanged += OnHealthChanged;
                OnHealthChanged();
            }
        }

        private void OnEnable()
        {
            OnHealthChanged();
        }

        private void OnDestroy()
        {
            if (_damageable != null)
                _damageable.HealthChanged -= OnHealthChanged;
        }

        private void OnHealthChanged()
        {
            try
            {
                SetValueUsingMax(_damageable.Health, _damageable.MaxHealth);
            }
            catch (Exception e)
            {
                // ignored
            }
        }
    }
}
