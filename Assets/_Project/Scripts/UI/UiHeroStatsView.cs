using TMPro;
using UnityEngine;

namespace Heroes
{
    public class UiHeroStatsView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI healthTxt;
        [SerializeField] private TextMeshProUGUI attackTxt;
        [SerializeField] private TextMeshProUGUI rangeTxt;
        [SerializeField] private TextMeshProUGUI coreTxt;
        [SerializeField] private TextMeshProUGUI treasureTxt;

        public string HealthTxt
        {
            get => healthTxt.text;
            set => healthTxt.SetText(value);
        }

        public string AttackTxt
        {
            get => attackTxt.text;
            set => attackTxt.SetText(value);
        }

        public string RangeTxt
        {
            get => rangeTxt.text;
            set => rangeTxt.SetText(value);
        }

        public string CoreTxt
        {
            get => coreTxt.text;
            set => coreTxt.SetText(value);
        }

        public string TreasureTxt
        {
            get => treasureTxt.text;
            set => treasureTxt.SetText(value);
        }
    }
}
