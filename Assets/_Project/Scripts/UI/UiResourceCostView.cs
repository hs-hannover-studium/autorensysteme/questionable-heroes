using System.Collections;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.Values;
using TMPro;
using UnityEngine;

namespace Heroes
{
    public class UiResourceCostView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _goldTxt;
        [SerializeField] private TextMeshProUGUI _magicTxt;
        [SerializeField] private TextMeshProUGUI _scrapsTxt;
        
        [SerializeField] private ResourceValue gold;
        [SerializeField] private ResourceValue magic;
        [SerializeField] private ResourceValue scraps;

        public void UpdateCostView(List<ResourceValuePair> cost)
        {
            foreach (var resource in cost)
            {
                if (resource.resourceValue == gold)
                {
                    _goldTxt.SetText(resource.value.ToString());
                } else if (resource.resourceValue == magic)
                {
                    _magicTxt.SetText(resource.value.ToString());
                } else if (resource.resourceValue == scraps)
                {
                    _scrapsTxt.SetText(resource.value.ToString());
                }
            }
        }
    }
}
