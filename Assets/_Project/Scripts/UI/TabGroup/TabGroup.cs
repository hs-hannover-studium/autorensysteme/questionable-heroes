using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.UI
{
    public class TabGroup : MonoBehaviour
    {
        [SerializeField, ReadOnly]
        private TabButton selectedTab;
        [SerializeField, ReadOnly]
        private TabButton lastSelected;
        [SerializeField]
        private List<TabButton> tabButtons = new List<TabButton>();
        [SerializeField]
        private List<TabPage> tabPages = new List<TabPage>();

        [SerializeField] private Sprite idle;
        [SerializeField] private Sprite active;
        [SerializeField] private Sprite hover;

        private void OnEnable()
        {
            if (lastSelected)
            {
                selectedTab = lastSelected;
                selectedTab.background.sprite = active;
                tabPages[tabButtons.IndexOf(selectedTab)].content.SetActive(true);
            }
        }

        private void Start()
        {
            selectedTab = tabButtons[0];
            selectedTab.background.sprite = active;
            tabPages[0].content.SetActive(true);
        }

        private void OnDisable()
        {
            lastSelected = selectedTab;
        }

        public void OnTabEnter(TabButton btn)
        {
            ResetTabs();
            if (btn != selectedTab)
            {
                btn.background.sprite = hover;
            }
            
        }

        public void OnTabExit(TabButton btn)
        {
            ResetTabs();
            if (btn != selectedTab)
            {
                btn.background.sprite = idle;
            }
        }

        public void OnTabSelected(TabButton btn)
        {
            // selectedTab.Deselect();
            selectedTab = btn;
            // selectedTab.Select();
            
            ResetTabs();
            btn.background.sprite = active;

            int index = tabButtons.IndexOf(btn);

            for (var i = 0; i < tabPages.Count; i++)
            {
                if (index == i)
                {
                    tabPages[i].content.SetActive(true);
                }
                else
                {
                    tabPages[i].content.SetActive(false);
                }
            }
        }

        private void ResetTabs()
        {
            foreach (TabButton btn in tabButtons)
            {
                if (btn == selectedTab)
                    continue;
                
                btn.background.sprite = idle;
            }
        }
    }
}