using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Heroes.UI
{
    [RequireComponent(typeof(Image))]
    public class TabButton : Button
    {
        public TabGroup tabGroup;
        
        [HideInInspector]
        public Image background;

        protected override void Awake()
        {
            base.Awake();
            
            background = GetComponent<Image>();
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            
            tabGroup.OnTabEnter(this);
            // OnHovered.Invoke();
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            
            tabGroup.OnTabExit(this);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            
            tabGroup.OnTabSelected(this);
        }
    }
}