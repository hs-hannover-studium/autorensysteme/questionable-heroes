using DG.Tweening;
using Heroes.UI.Views.Behaviours;
using UnityEngine;

namespace Heroes.UI.Views.Strategies
{
    public class WindowSlideStrategy : IViewOpenBehaviour, IViewCloseBehaviour
    {
        [SerializeField] private UiTweenSetting _setting;
        [SerializeField] private Vector3 _from;
        [SerializeField] private Vector3 _to;
        [SerializeField] private float delay;
        
        public void Open(View view)
        {
            view.gameObject.SetActive(true);

            Sequence sequence = DOTween.Sequence();
            
            sequence.Append(Fade(view, 1f));
            sequence.Join(Slide(view, _from, _to));

            sequence.OnComplete(() =>
            {
                OpenChildren(view);
            });
        }
        
        public void Close(View view)
        {
            Sequence sequence = DOTween.Sequence();
            
            sequence.Append(Slide(view, _from, _to));
            sequence.Join(Fade(view, 0));
            
            sequence.OnComplete(() =>
            {
                view.gameObject.SetActive(false);
            });
        }

        private Tween Slide(View view, Vector3 from, Vector3 to)
        {
            view.RectTransform.anchoredPosition = from;
            Tween slide = view.RectTransform.DOAnchorPos(to, _setting.duration).SetEase(_setting.easeType).SetDelay(delay);
            
            return slide;
        }

        private Tween Fade(View view, float endValue)
        {
            Tween fade = view.CanvasGroup.DOFade(endValue, _setting.duration).SetEase(_setting.easeType).SetDelay(delay);

            return fade;
        }
        
        private void OpenChildren(View view)
        {
            Window window = (Window) view;

            foreach (View windowView in window.views)
            {
                windowView.Open();
            }
        }
    }
}