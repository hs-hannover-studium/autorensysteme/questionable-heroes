using Heroes.UI.Views.Behaviours;

namespace Heroes.UI.Views.Strategies
{
    public class DefaultStrategy : IViewOpenBehaviour, IViewCloseBehaviour
    {
        public void Open(View view)
        {
            view.gameObject.SetActive(true);
            view.CanvasGroup.alpha = 1f;
        }

        public void Close(View view)
        {
            view.CanvasGroup.alpha = 0f;
            view.gameObject.SetActive(false);
        }
    }
}