namespace Heroes.UI.Views.Behaviours
{
    public interface IViewCloseBehaviour
    {
        public void Close(View view);
    }
}