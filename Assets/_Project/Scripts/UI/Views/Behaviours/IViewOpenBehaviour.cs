namespace Heroes.UI.Views.Behaviours
{
    public interface IViewOpenBehaviour
    {
        public void Open(View view);
    }
}