using Heroes.UI.Views.Behaviours;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.UI.Views
{
    [RequireComponent(typeof(CanvasGroup), typeof(RectTransform))]
    public class View : SerializedMonoBehaviour
    {
        [SerializeField] private IViewOpenBehaviour _openBehaviour;
        [SerializeField] private IViewCloseBehaviour _closeBehaviour;

        protected CanvasGroup canvasGroup;
        protected RectTransform rectTransform;

        [SerializeField, ReadOnly]
        private bool _isOpen;

        public UnityEvent OnOpen;
        public UnityEvent OnClose;

        public CanvasGroup CanvasGroup => canvasGroup;

        public RectTransform RectTransform => rectTransform;

        public bool IsOpen => _isOpen;

        protected virtual void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            rectTransform = GetComponent<RectTransform>();
        }

        [Button]
        public void Open()
        {
            if (IsOpen)
                return;

            _isOpen = true;
            OnOpen?.Invoke();
            
            _openBehaviour.Open(this);
        }

        [Button]
        public void Close()
        {
            if (!IsOpen)
                return;
            
            _isOpen = false;
            OnClose?.Invoke();
            
            _closeBehaviour.Close(this);
        }
        
        [Button]
        public void Toggle()
        {
            if (_isOpen)
                Close();
            else
                Open();
        }
    }
}