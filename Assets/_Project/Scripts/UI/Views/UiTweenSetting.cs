using DG.Tweening;
using UnityEngine;

namespace Heroes.UI.Views
{
    [CreateAssetMenu(fileName = "UI", menuName = "UI/TweenSetting", order = 0)]
    public class UiTweenSetting : ScriptableObject
    {
        public Ease easeType = Ease.Linear;
        public float duration = .25f;
    }
}