using System;
using System.Collections.Generic;
using Heroes.Extensions;
using Heroes.ScriptableObjects.RuntimeSets;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI
{
    [RequireComponent(typeof(Button))]
    public class UiButtonDependencies : MonoBehaviour
    {
        public DependencySet dependencies;

        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(delegate
            {
                dependencies.ResetCompletedDependenciesCount();
            });
        }

        private void OnEnable()
        {
            dependencies.DependenciesCompleted += OnDependenciesCompleted;
            _button.interactable = false;
        }

        private void OnDisable()
        {
            dependencies.DependenciesCompleted -= OnDependenciesCompleted;
            dependencies.ResetCompletedDependenciesCount();
        }

        private void OnDependenciesCompleted()
        {
            _button.interactable = true;
        }
    }
}