using Heroes.ScriptableObjects.Values;
using TMPro;
using UnityEngine;

namespace Heroes.UI.Textfields
{
    public class IntValueTextfield : MonoBehaviour
    {
        public IntValue value;
        public TextMeshProUGUI txtElement;

        private void OnEnable()
        {
            value.ValueChanged += OnValueChanged;
            OnValueChanged(value.RuntimeValue);
        }

        private void OnDisable()
        {
            value.ValueChanged -= OnValueChanged;
        }

        private void OnValueChanged(int val)
        {
            txtElement.SetText(value.RuntimeValue.ToString());
        }
    }
}