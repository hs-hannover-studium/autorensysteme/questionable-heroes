using Heroes.ScriptableObjects.RuntimeSets;
using TMPro;
using UnityEngine;

namespace Heroes.UI.Textfields
{
    public class RuntimeSetCountTextfield<T> : MonoBehaviour
    {
        public RuntimeSet<T> runtimeSet;
        public TextMeshProUGUI txtElement;

        private void OnEnable()
        {
            runtimeSet.ValueChanged += OnValueChanged;
            txtElement.SetText(runtimeSet.Count.ToString());
        }

        private void OnDisable()
        {
            runtimeSet.ValueChanged -= OnValueChanged;
        }

        private void OnValueChanged(T t)
        {
            txtElement.SetText(runtimeSet.Count.ToString());
        }
    }
}