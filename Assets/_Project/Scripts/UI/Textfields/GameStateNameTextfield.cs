using Heroes.ScriptableObjects.States;
using TMPro;
using UnityEngine;

namespace Heroes.UI.Textfields
{
    public class GameStateNameTextfield : MonoBehaviour
    {
        public GameStateManager gameStateManager;
        public TextMeshProUGUI txtElement;

        private void OnEnable()
        {
            gameStateManager.StateChanged += OnStateChanged;
        }

        private void OnDisable()
        {
            gameStateManager.StateChanged -= OnStateChanged;
        }

        private void OnStateChanged(GameState state)
        {
            txtElement.SetText(state.stateName);
        }
    }
}