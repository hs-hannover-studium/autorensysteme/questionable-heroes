using DG.Tweening;
using Heroes.ScriptableObjects.Values;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

namespace Heroes.UI
{
    public class Resource : MonoBehaviour
    {
        [SerializeField] private GameObject valueChangeText;
        [SerializeField] private TextMeshProUGUI valueTextField;
        [SerializeField] private ResourceValue resource;
        [SerializeField] private Vector3 valueChangeMovement;
        [SerializeField] private Vector3 valueChangeScaleTarget;
        [SerializeField] private Vector3 valueChangeScaleStart;
        [SerializeField] private float valueChangeDuration;
        [SerializeField] private Ease valueChangeEase;
        [SerializeField] private Color addColor = Color.green;
        [SerializeField] private Color subtractColor = Color.red;

        private Image _icon;
        private int currentValue;
        private int oldValue;

        public Sprite Icon
        {
            set => _icon.sprite = value;
        }

        public int Value
        {
            set { valueTextField.SetText(value.ToString()); }
        }

        private void Awake()
        {
            _icon = GetComponent<Image>();
            Icon = resource.Icon;
            Value = resource.RuntimeValue;
        }

        private void OnEnable()
        {
            resource.ValueChanged += OnValueChanged;
        }

        private void OnDisable()
        {
            resource.ValueChanged -= OnValueChanged;
        }

        private void OnValueChanged(int newValue)
        {
            Value = resource.RuntimeValue;
            oldValue = currentValue;
            currentValue = newValue;
            SpawnValueChangeText();
        }

        private void SpawnValueChangeText()
        {
            GameObject go = Instantiate(valueChangeText, transform);
            TextMeshProUGUI textMesh = go.GetComponent<TextMeshProUGUI>();
            CanvasGroup canvasGroup = go.GetComponent<CanvasGroup>();

            int valueChange = currentValue - oldValue;

            go.transform.localScale = valueChangeScaleStart;
            textMesh.text = "+" + valueChange.ToString();
            textMesh.color = addColor;

            if (valueChange < 0)
            {
                textMesh.text = valueChange.ToString();
                textMesh.color = subtractColor;
            }

            Tween move = go.transform.DOMove(go.transform.position + valueChangeMovement, valueChangeDuration)
                .SetEase(valueChangeEase);
            Tween fade = canvasGroup.DOFade(0, valueChangeDuration).SetEase(valueChangeEase);
            Tween scale = go.transform.DOScale(valueChangeScaleTarget, valueChangeDuration).SetEase(valueChangeEase);

            Sequence sequence = DOTween.Sequence();

            sequence.Append(move);
            sequence.Join(fade);
            sequence.Join(scale);

            sequence.OnComplete(() => { Destroy(go); });
        }
    }
}