using System;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.ScriptableObjects.Values;
using Heroes.ScriptableObjects.World;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Heroes
{
    public class UiTreasureGoldController : MonoBehaviour
    {
        [SerializeField] private GameSession _gameSession;
        [SerializeField] private ResourcesSet _resourcesSet;
        [SerializeField] private ResourceValue _gold;
        [SerializeField] private ResourceValue _treasureGold;
        
        [SerializeField] private Button _addBtn;
        [SerializeField] private Button _subtractBtn;
        [SerializeField] private TextMeshProUGUI _addTxt;
        [SerializeField] private TextMeshProUGUI _subtractTxt;

        public event Action TreasureGoldAdded;
        public event Action TreasureGoldTaken;

        [ShowInInspector, ReadOnly]
        private int _amount;
        
        [FoldoutGroup("Events"), SerializeField] 
        private UnityEvent OnTreasureGoldAdded;
        [FoldoutGroup("Events"), SerializeField] 
        private UnityEvent OnTreasureGoldTaken;

        private void Awake()
        {
            foreach (var scalingFactor in _gameSession.Difficulty.HeroCountScaling)
            {
                if (scalingFactor.Source == _treasureGold)
                {
                    _amount = scalingFactor.Per;
                }
            }
            _addTxt.SetText("+" + _amount);
            _subtractTxt.SetText("-" + _amount);
            UpdateButtons();
        }
        
        private void OnEnable()
        {
            _resourcesSet.ResourceValuesChanged += UpdateButtons;
        }

        private void OnDisable()
        {
            _resourcesSet.ResourceValuesChanged += UpdateButtons;
        }

        [Button, FoldoutGroup("Debug")]
        public void AddGoldToTreasure()
        {
            List<ResourceValuePair> resources = new List<ResourceValuePair>();
            resources.Add(new ResourceValuePair(_gold, _amount));
            if (!_resourcesSet.HasEnoughResources(resources))
                return;

            _gold.Subtract(_amount);
            _treasureGold.Add(_amount);
            OnTreasureGoldAdded.Invoke();
            TreasureGoldAdded?.Invoke();
            
            UpdateButtons();
        }

        [Button, FoldoutGroup("Debug")]
        public void TakeGoldFromTreasure()
        {
            if (_treasureGold.RuntimeValue < _amount)
            {
                _gold.Add(_treasureGold.RuntimeValue);
            }
            else
            {
                _gold.Add(_amount);
            }
            _treasureGold.Subtract(_amount);
            
            OnTreasureGoldTaken.Invoke();
            TreasureGoldTaken?.Invoke();
            
            UpdateButtons();
        }

        private void UpdateButtons()
        {
            if (_addBtn)
            {
                List<ResourceValuePair> goldCost = new List<ResourceValuePair>();
                goldCost.Add(new ResourceValuePair(_gold, _amount));
                _addBtn.interactable = _resourcesSet.HasEnoughResources(goldCost);
            }

            if (_subtractBtn)
            {
                _subtractBtn.interactable = (_treasureGold.RuntimeValue != 0);
            }
        }
    }
}
