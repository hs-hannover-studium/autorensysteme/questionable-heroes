using Heroes.Buildings;
using Heroes.ScriptableObjects.Buildings;
using UnityEngine;

namespace Heroes.UI
{
    public class ListBuildings : UiList<BuildingData>
    {
        protected override void InitItemUI(BuildingData data, GameObject o)
        {
            ListItemBuilding item = o.GetComponent<ListItemBuilding>();
            
            item.Name = data.BuildingName;
            item.Icon = data.Icon;
            item.Cost = data.cost;
            item.UpdateRemoveButton();
        }

        protected override void ItemClicked(int i)
        {
            BuildMode.Instance.StartPlacement(items[i].Building);
        }
    }
}