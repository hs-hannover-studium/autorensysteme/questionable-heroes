using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes
{
    public class TypeWriterEffect : MonoBehaviour
    {
        [SerializeField]
        private float _speed = 25f;

        public UnityEvent onCharacterTyped;
        
        private Coroutine _typeEffectCoroutine;
        private bool _isRunning;

        public bool IsRunning => _isRunning;
        
        [ShowInInspector, ReadOnly]
        public bool isPaused { get; set; }

        public void StartTypeWriterEffect(TextMeshProUGUI textField, string textToType)
        {
            _typeEffectCoroutine = StartCoroutine(TypeEffect(textField, textToType));
        }

        public void Skip()
        {
            StopCoroutine(_typeEffectCoroutine);
            _isRunning = false;
        }

        private IEnumerator TypeEffect(TextMeshProUGUI textField, string textToType)
        {
            _isRunning = true;
            textField.SetText(textToType);
            textField.maxVisibleCharacters = 0;
            
            float t = 0;
            
            while (textField.maxVisibleCharacters < textToType.Length)
            {
                while (isPaused)
                {
                    yield return null;
                }
                
                t += _speed * Time.deltaTime;
                int charIndex = Mathf.FloorToInt(t);
                charIndex = Mathf.Clamp(charIndex, 0, textToType.Length);

                if (charIndex % 6 == 0)
                {
                    onCharacterTyped.Invoke();
                }

                textField.maxVisibleCharacters = charIndex;
                yield return null;
            }

            _isRunning = false;
        }
    }
}
