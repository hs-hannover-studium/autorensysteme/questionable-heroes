using System.Collections;
using System.Collections.Generic;
using Heroes.UI.Views;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Heroes
{
    [RequireComponent(typeof(TypeWriterEffect))]
    public class UiDialogue : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _speakerNameTextField;
        [SerializeField]
        private Image _speakerPortrait;
        [SerializeField]
        private TextMeshProUGUI _textField;

        private TypeWriterEffect _typeWriterEffect;
        [SerializeField] private Window _window;

        private readonly Queue<DialogueObject> _dialoguesToRun = new Queue<DialogueObject>();
        private DialogueObject _currentRunningDialogue;

        private Coroutine _dialogueCoroutine;
        private InputActions _dialogueControls;

        private void Awake()
        {
            _typeWriterEffect = GetComponent<TypeWriterEffect>();
            _dialogueControls = new InputActions();
        }

        private void OnEnable()
        {
            _dialogueControls.Enable();
            _dialogueControls.Dialogue.Enable();
            _typeWriterEffect.isPaused = false;
            _dialogueCoroutine = null;
        }
        
        private void OnDisable()
        {
            _dialogueControls.Disable();
            _dialogueControls.Dialogue.Disable();
            _typeWriterEffect.isPaused = true;
        }

        [Button]
        public void RunDialogue(DialogueObject dialogue)
        {
            if (dialogue.PlayOnce && dialogue.Played)
                return;

            if (_dialoguesToRun.Contains(dialogue) || _currentRunningDialogue == dialogue)
                return;
            
            _window.Open();

            _dialoguesToRun.Enqueue(dialogue);
            dialogue.Played = true;

            if (_dialogueCoroutine == null)
                _dialogueCoroutine = StartCoroutine(ShowDialogueBox());
        }

        private IEnumerator ShowDialogueBox()
        {
            while (_dialoguesToRun.Count > 0)
            {
                _currentRunningDialogue = _dialoguesToRun.Dequeue();
                _speakerNameTextField.SetText(_currentRunningDialogue.speaker.Name);
                _speakerPortrait.sprite = _currentRunningDialogue.speaker.Portrait;
                _textField.SetText("");
                
                yield return TypeDialogue(_currentRunningDialogue);
            }
            
            _window.Close();
            _currentRunningDialogue = null;
            _textField.SetText("");
            _dialogueCoroutine = null;
            yield return null;
        }

        private IEnumerator TypeDialogue(DialogueObject dialogue)
        {
            yield return new WaitForSeconds(0.5f);
            
            foreach (var text in dialogue.Dialogue)
            {
                yield return TypeText(text);

                _textField.maxVisibleCharacters = text.Length;

                yield return null;
                yield return new WaitUntil(() => _dialogueControls.Dialogue.Continue.triggered);
            }
        }

        private IEnumerator TypeText(string textToType)
        {
            _typeWriterEffect.StartTypeWriterEffect(_textField, textToType);

            while (_typeWriterEffect.IsRunning)
            {
                yield return null;

                if (_dialogueControls.Dialogue.Continue.triggered)
                {
                    _typeWriterEffect.Skip();
                }
            }
        }
    }
}
