using System.Collections;
using System.Collections.Generic;
using Heroes.Extensions;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes
{
    [RequireComponent(typeof(TypeWriterEffect))]
    public class UiSpeechBubble : MonoBehaviour
    {
        [SerializeField] private Button _speechBubble;
        [SerializeField] private TextMeshProUGUI _speechBubbleTextField;
        
        [SerializeField] private float _closeAfterDivisor = 2f;
        
        private TypeWriterEffect _typeWriterEffect;

        [ShowInInspector, ReadOnly]
        private string _currentText;

        private Coroutine _speechBubbleCoroutine;
        
        private void Awake()
        {
            _typeWriterEffect = GetComponent<TypeWriterEffect>();
        }

        private void OnEnable()
        {
            _speechBubble.AddEventListener(0, CloseSpeechBubble);
            _typeWriterEffect.isPaused = false;
        }
        
        private void OnDisable()
        {
            _typeWriterEffect.isPaused = true;
        }

        public void StartSpeechBubble(SpeechBubbleCollection speechBubbleCollection)
        {
            if (speechBubbleCollection == null)
                return;

            if (_speechBubbleCoroutine != null)
                return;

            _speechBubbleCoroutine = StartCoroutine(TypeText(speechBubbleCollection));
        }

        private IEnumerator TypeText(SpeechBubbleCollection speechBubbleCollection)
        {
            _speechBubble.gameObject.SetActive(true);
            _speechBubbleTextField.SetText("");
            _currentText = speechBubbleCollection.GetRandom();
            _speechBubble.interactable = false;

            yield return new WaitForSeconds(0.5f);
            
            _typeWriterEffect.StartTypeWriterEffect(_speechBubbleTextField, _currentText);

            while (_typeWriterEffect.IsRunning)
            {
                yield return null;
            }

            yield return null;
            
            _speechBubbleTextField.maxVisibleCharacters = _currentText.Length;

            yield return new WaitForSeconds(0.5f);

            _speechBubble.interactable = true;

            yield return new WaitForSeconds(Mathf.Clamp(_currentText.Length / _closeAfterDivisor, 4, 10));
            _speechBubbleCoroutine = null;
            _speechBubble.gameObject.SetActive(false);
            _speechBubbleTextField.SetText("");
            _currentText = "";
        }
        
        private void CloseSpeechBubble(int obj)
        {
            StopCoroutine(_speechBubbleCoroutine);
            _speechBubbleCoroutine = null;
            
            _speechBubble.gameObject.SetActive(false);
            _speechBubbleTextField.SetText("");
            _currentText = "";
        }
    }
}
