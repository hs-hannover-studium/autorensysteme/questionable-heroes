using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI.WaveOverview
{
    public class UiIncomingHeroInformationView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameTxt;
        [SerializeField] private TextMeshProUGUI levelTxt;
        [SerializeField] private Image portrait;
        [SerializeField] private UiHeroStatsView heroStatsView;

        public string NameTxt
        {
            get => nameTxt.text;
            set => nameTxt.SetText(value);
        }
        
        public string LevelTxt
        {
            get => levelTxt.text;
            set => levelTxt.SetText(value);
        }

        public Sprite Portrait
        {
            get => portrait.sprite;
            set => portrait.sprite = value;
        }

        public UiHeroStatsView StatsView => heroStatsView;
    }
}