using Heroes.HeroSpawner;
using Heroes.Probabilities;
using Heroes.ScriptableObjects;
using Heroes.ScriptableObjects.Values;
using Heroes.Stats;
using UnityEngine;

namespace Heroes.UI.WaveOverview
{
    public class UiIncomingHeroesList : UiList<HeroData>
    {
        [SerializeField] private UiIncomingHeroInformationView heroInformationView;
        [SerializeField] private ProbabilityCalculator probabilityCalculator;
        [SerializeField] private HeroWaveCalculator heroWaveCalculator;
        [SerializeField] private ResourceValue dungeonCore;
        [SerializeField] private ResourceValue treasureGold;
        
        private void OnEnable()
        {
            probabilityCalculator.ValueChanged += UpdateUi;
            heroWaveCalculator.CalculateHeroCount();
            heroWaveCalculator.CalculateHeroLevel();
            UpdateUi();
        }

        private void OnDisable()
        {
            probabilityCalculator.ValueChanged -= UpdateUi;
        }

        private void UpdateUi()
        {
            Show();
            ItemClicked(0);
        }

        protected override void InitItemUI(HeroData data, GameObject o)
        {
            UiIncomingHeroView view = o.GetComponent<UiIncomingHeroView>();
            
            view.Portrait = data.Icon;
            view.FillAmount = probabilityCalculator.GetHeroProbability(data);
            view.FillColor = data.HeroColor;
        }

        protected override void ItemClicked(int i)
        {
            heroInformationView.NameTxt = items[i].HeroClassName;
            heroInformationView.LevelTxt = heroWaveCalculator.heroLevel.RuntimeValue.ToString();
            heroInformationView.Portrait = items[i].Icon;

            heroInformationView.StatsView.HealthTxt = items[i].GetStat(StatType.HP).ToString(heroWaveCalculator.heroLevel.RuntimeValue);
            heroInformationView.StatsView.AttackTxt = items[i].GetStat(StatType.Damage).ToString(heroWaveCalculator.heroLevel.RuntimeValue);
            heroInformationView.StatsView.RangeTxt = items[i].GetStat(StatType.ATKRange).ToString(heroWaveCalculator.heroLevel.RuntimeValue);
            heroInformationView.StatsView.CoreTxt = items[i].GetCapacity(dungeonCore).ToString(heroWaveCalculator.heroLevel.RuntimeValue);
            heroInformationView.StatsView.TreasureTxt = items[i].GetCapacity(treasureGold).ToString(heroWaveCalculator.heroLevel.RuntimeValue);
        }
    }
}