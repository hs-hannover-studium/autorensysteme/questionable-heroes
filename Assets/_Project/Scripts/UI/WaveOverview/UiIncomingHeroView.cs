using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI.WaveOverview
{
    [RequireComponent(typeof(Button))]
    public class UiIncomingHeroView : MonoBehaviour
    {
        [SerializeField] private Image portrait;
        [SerializeField] private Image fillAmount;
        [SerializeField] private TextMeshProUGUI probabilityTxt;

        public Sprite Portrait
        {
            get => portrait.sprite;
            set => portrait.sprite = value;
        }

        public float FillAmount
        {
            get => fillAmount.fillAmount;
            set
            {
                fillAmount.fillAmount = value;
                probabilityTxt.SetText((Math.Round(value * 100)) + "%");
            }
        }

        public Color FillColor
        {
            get => fillAmount.color;
            set => fillAmount.color = value;
        }
        
    }
}