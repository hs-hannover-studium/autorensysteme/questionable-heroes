using Heroes.Extensions;
using Heroes.ScriptableObjects.RuntimeSets;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.UI
{
    public abstract class UiList<T> : MonoBehaviour
    {
        public RuntimeSet<T> items;
        public GameObject listItem;
        
        protected virtual void Start()
        {
            Show();
        }

        protected virtual void Show()
        {
            transform.RemoveChildren();
            
            for (var i = 0; i < items.Count; i++)
            {
                GameObject item = Instantiate(listItem, transform.position, Quaternion.identity, transform);
                Button btn = item.GetComponent<Button>();
                
                InitItemUI(items[i], item);

                btn.AddEventListener(i, ItemClicked);
            }
        }

        protected abstract void InitItemUI(T data, GameObject o);
        protected abstract void ItemClicked(int i);
    }
}