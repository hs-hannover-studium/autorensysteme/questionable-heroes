using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Heroes.Extensions
{
    // TODO: Remove methods from 
    // LevelGenerator: ColorEqual
    // BuildMode: Approximation
    
    public static class ExtensionMethods
    {
        public static bool Approximation(this Vector3 vector, Vector3 vec2, float epsilon)
        {
            return (Mathf.Abs(vector.x - vec2.x) < epsilon
                    && Mathf.Abs(vector.y - vec2.y) < epsilon
                    && Mathf.Abs(vector.z - vec2.z) < epsilon);
        }
        
        public static void AddEventListener<T>(this Button button, T param, Action<T> OnClick)
        {
            button.onClick.AddListener(delegate
            {
                OnClick(param);
            });
        }
        
        public static bool ColorEqual(this Color color, Color color1, Color color2, float epsilon = 0.1f)
        {
            return (Mathf.Abs(color1.r - color2.r) < epsilon
                    && Mathf.Abs(color1.g - color2.g) < epsilon
                    && Mathf.Abs(color1.b - color2.b) < epsilon);
        }
        
        public static void RemoveChildrenImmediate(this Transform transform)
        {
            var tempArray = new GameObject[transform.childCount];

            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = transform.GetChild(i).gameObject;
            }

            foreach (var child in tempArray)
            {
                Object.DestroyImmediate(child);
            }
        }
        
        public static void RemoveChildren(this Transform transform)
        {
            var tempArray = new GameObject[transform.childCount];

            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = transform.GetChild(i).gameObject;
            }

            foreach (var child in tempArray)
            {
                Object.Destroy(child);
            }
        }

        public static float GetPathDistance(this NavMeshPath path)
        {
            float distance = 0;

            for (int i = 0; i < path.corners.Length - 1; i++)
                distance += Vector3.Distance(path.corners[i], path.corners[i + 1]);

            return distance;
        }

        public static void DrawPath(this NavMeshPath path, Color color)
        {
            for (int i = 0; i < path.corners.Length - 1; i++)
                Debug.DrawLine(path.corners[i], path.corners[i + 1], color, 0.5f);
        }

        public static bool IsInside(this BoxCollider boxCollider, Vector3 position)
        {
            float dist = boxCollider.bounds.SqrDistance(position);
            return dist <= 0.1f;
        }
    }
}