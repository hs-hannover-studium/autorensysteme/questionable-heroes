using UnityEngine;

namespace Heroes.Probabilities
{
    public abstract class Probability<T> where T : ScriptableObject
    {
        public float probability;
        public T data;

        public Probability(float probability, T data)
        {
            this.probability = probability;
            this.data = data;
        }
    }
}