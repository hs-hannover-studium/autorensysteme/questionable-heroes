using System;
using System.Collections.Generic;
using Heroes.HeroSpawner;
using Heroes.ScriptableObjects;
using Heroes.ScriptableObjects.RuntimeSets;
using Sirenix.OdinInspector;
using Random = UnityEngine.Random;

namespace Heroes.Probabilities
{
    [Serializable]
    public class ProbabilitySet
    {
        public float Sum { get; private set; }

        [ShowInInspector] public List<HeroProbability> Probabilities { get; private set; }

        public ProbabilitySet(HeroTypesSet heroTypes)
        {
            Probabilities = new List<HeroProbability>();

            CalculateSum(heroTypes);
            NormalizeProbabilities();
            SortProbabilities();
        }

        /// <summary>
        /// Creates a random values for each Hero Type and creates a sum 
        /// based on these values.
        /// These values are then used to determine the probability of each hero 
        /// </summary>
        /// <param name="heroTypes"></param>
        private void CalculateSum(HeroTypesSet heroTypes)
        {
            foreach (HeroData data in heroTypes)
            {
                float randomValue = Random.Range(0, 100);
                Sum += randomValue;
                Probabilities.Add(new HeroProbability(randomValue, data));
            }
        }

        /// <summary>
        /// Normalizes all Probabilities between 0 and 1
        /// </summary>
        private void NormalizeProbabilities()
        {
            foreach (HeroProbability heroProbability in Probabilities)
            {
                heroProbability.probability /= Sum;
            }
        }

        /// <summary>
        /// Sorts Hero Probabilities in descending order based on their probability
        /// </summary>
        private void SortProbabilities()
        {
            Probabilities.Sort(delegate(HeroProbability x, HeroProbability y)
            {
                if (x.probability > y.probability)
                    return 1;

                return -1;
            });
        }
    }
}