using System;
using Heroes.HeroSpawner;
using Heroes.ScriptableObjects;
using Heroes.ScriptableObjects.RuntimeSets;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Heroes.Probabilities
{
    [CreateAssetMenu(fileName = "ProbabilitiesCalculator", menuName = "Probabilities/ProbabilityCalculator")]
    public class ProbabilityCalculator : ScriptableObject
    {
        public HeroTypesSet heroTypes;

        [SerializeField, HideInEditorMode] private ProbabilitySet probabilitySet;
        
        public event Action ValueChanged;

        public ProbabilitySet ProbabilitySet => probabilitySet;

        [Button, HideInEditorMode]
        public void CalculateProbability()
        {
            probabilitySet = new ProbabilitySet(heroTypes);
            ValueChanged?.Invoke();
        }

        /// <summary>
        /// Selects hero from given probability set based on their probability
        /// </summary>
        /// <returns></returns>
        public HeroData SelectHero()
        {
            float rand = Random.value;

            foreach (HeroProbability hero in probabilitySet.Probabilities)
            {
                if (hero.probability > rand)
                    return hero.data;
            }

            // Return most possible hero (last hero in set)
            return probabilitySet.Probabilities[probabilitySet.Probabilities.Count - 1].data;
        }
        
        public float GetHeroProbability(HeroData data)
        {
            float probabilityValue = 0;

            foreach (HeroProbability probability in probabilitySet.Probabilities)
            {
                if (probability.data == data)
                {
                    probabilityValue = probability.probability;
                    break;
                }
            }

            return probabilityValue;
        }
    }
}