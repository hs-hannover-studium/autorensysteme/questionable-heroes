using UnityEngine;

namespace Heroes.PlayerInputs
{
    [DefaultExecutionOrder(-10)]
    public class PlayerInputController : MonoBehaviour
    {
        public static PlayerInputController Instance { get; private set; }
        public InputActions inputActions;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);
        }

        private void OnEnable()
        {
            inputActions = new InputActions();
            inputActions.Gameplay.Enable();
        }

        /// <summary>
        /// Resets action map to Gameplay map
        /// </summary>
        public void ResetActionMap()
        {
            inputActions.Gameplay.Enable();
        }
    }
}