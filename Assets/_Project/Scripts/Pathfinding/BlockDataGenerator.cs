using System.Collections.Generic;
using UnityEngine;

namespace Heroes.Pathfinding
{
    public class BlockDataGenerator : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject levelGen;
        public Dictionary<Vector3Int, BlockData> blockData = new Dictionary<Vector3Int, BlockData>();
        
        private Vector3Int[] _neighbourCandidates = new Vector3Int[4]
        {
            new Vector3Int(0, 0, 2),
            new Vector3Int(0, 0, -2),
            new Vector3Int(2, 0, 0),
            new Vector3Int(-2, 0, 0)
        };
        void Start()
        {
            GetBlockdata();
            Debug.Log("Blöcke:" + blockData.Count);
            GetNeighbours();
            //DestroyAll();
            
            Debug.Log(blockData.Count);
        }

        private void GetBlockdata()
        {
            for (int i = 0; i < levelGen.transform.childCount; i++)
            {
                GameObject current = levelGen.transform.GetChild(i).gameObject;
                var block = new BlockData
                {
                    GridPos = Vector3Int.CeilToInt(current.transform.position),
                    ObjectReference = current,
                    Neighbours = new List<BlockData>(),
                    NeighbourCount = 0
                };
                blockData.Add(block.GridPos, block);
            }
        }

        private void DestroyAll()
        {
            foreach (KeyValuePair<Vector3Int, BlockData> data in blockData)
            {
                Destroy(data.Value.ObjectReference);
            }
        }

        private void GetNeighbours()
        {
            foreach (KeyValuePair<Vector3Int, BlockData> data in blockData)
            {
                foreach (Vector3Int pos in _neighbourCandidates)
                {
                    Vector3Int positionToCheck = data.Value.GridPos + pos;
                    if (blockData.TryGetValue(positionToCheck, out BlockData neighbourBlock))
                    {
                        data.Value.Neighbours.Add(neighbourBlock);
                    }
                }

                data.Value.NeighbourCount = data.Value.Neighbours.Count;
            }
        }
        
    }
}
