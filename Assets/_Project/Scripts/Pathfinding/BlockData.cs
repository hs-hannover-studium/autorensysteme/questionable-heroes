using System.Collections.Generic;
using UnityEngine;

namespace Heroes.Pathfinding
{
    public class BlockData
    {
        public Vector3Int GridPos { get; set; }
        
        public GameObject ObjectReference { get; set; }
        
        public List<BlockData> Neighbours { get; set; }
        
        public int NeighbourCount { get; set; }
    }
}
