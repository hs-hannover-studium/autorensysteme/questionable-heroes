using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Heroes.Pathfinding
{
    public class DynamicPathCalc : MonoBehaviour
    {
        public GameObject rallyPoint;
        public GameObject blockade;
        public LineRenderer lineRenderer;
        public List<LineRenderer> lineRenderers;
        public NavMeshSurface nav;
        
        private List<NavMeshPath> _paths = new List<NavMeshPath>();
        private NavMeshPath _currentPath;
        private Vector3 _source;
        private Vector3 _target;

        private GameObject _currentBlockade;
        private Coroutine dynamicPath;

        private bool _foundPath;
        private void Start()
        {
            _source = transform.position;
            _target = rallyPoint.transform.position;
            _currentPath = new NavMeshPath();

            lineRenderer = GetComponent<LineRenderer>();
            //lineRenderer.startColor = Color.red;
            //lineRenderer.endColor = Color.red;
            //lineRenderer.startWidth = 0.2f;
            //lineRenderer.endWidth = 0.2f;
            //
            //lineRenderer.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));

            //if (dynamicPath == null)
            //{
            //    dynamicPath = StartCoroutine(DynamicPath());
            //}
            dynamicPath = StartCoroutine(DynamicPath());
            
            //MarkPaths();
        }

        private IEnumerator DynamicPath()
        {
            /*
            for (int i = 0; i <= 5; i++)
            {
                _paths.Add(new NavMeshPath());
                _foundPath = GetComponent<NavMeshAgent>().CalculatePath(_target, _paths[_paths.Count-1]);
            }
            */
            _paths.Add(new NavMeshPath());
            GetComponent<NavMeshAgent>().CalculatePath(_target, _paths[_paths.Count-1]);
            Debug.Log(_foundPath.ToString());
            int cL = _paths[0].corners.Length;
            Vector3 blockPos = _paths[0].corners[cL - 2] + 0.5f * (_paths[0].corners[cL - 1] - _paths[0].corners[cL - 2]);
            _currentBlockade = Instantiate(blockade, blockPos, Quaternion.identity);
            yield return new WaitForSeconds(0.2f);

            //_paths.Add(new NavMeshPath());
            
            _currentPath = new NavMeshPath();
            GetComponent<NavMeshAgent>().CalculatePath(_target, _currentPath);
            
            if(_currentPath.status == NavMeshPathStatus.PathInvalid)
                _paths.Add(_currentPath);
            
            
            
            Debug.Log(_foundPath.ToString());

            MarkPaths();
            yield return null;
        }

        private void MarkPaths()
        {
            Debug.Log("die Paths liste enthält" + _paths.Count + " Elemente");
            foreach (NavMeshPath path in _paths)
            {
                GameObject placeHolder = new GameObject();
                placeHolder.AddComponent<LineRenderer>();
                lineRenderers.Add(placeHolder.GetComponent<LineRenderer>());
                int lineNumber = lineRenderers.Count - 1;
                
                
                Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                lineRenderers[lineNumber].startColor = color;
                lineRenderers[lineNumber].endColor = color;
                lineRenderers[lineNumber].startWidth = 0.2f;
                lineRenderers[lineNumber].endWidth = 0.2f;
            
                lineRenderers[lineNumber].material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));

                lineRenderers[lineNumber].positionCount = path.corners.Length;
                
                lineRenderers[lineNumber].SetPositions(path.corners);
                
            }
        }
    }
}
