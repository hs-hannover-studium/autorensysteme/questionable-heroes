using System;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityAudioSource;
using Heroes.Extensions;
using Heroes.Pathfinding;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.World;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Heroes
{
    public class PathDecider : MonoBehaviour
    {
        private List<WayPoint> visited = new List<WayPoint>();
        private int wpCount = 0;
        [SerializeField] [Range(0, 100)] private int smartness = 60;
        [SerializeField] private RuntimeSet<Tile> _coreSet;
        
        private float currentDistanceToCore;

        private float currentWPDistance;


        [Button]
        public WayPoint DecidePath()
        {
            if (GetCurrentPosition().GetComponent<WayPoint>() != null)
                visited.Add(GetCurrentPosition().GetComponent<WayPoint>());
            List<WayPoint> options = GetPathOptions();
            return DecideFromOptions3(options);
        }

        public List<WayPoint> GetPathOptions()
        {
            Tile currentTile = GetCurrentPosition();

            if (currentTile == null)
            {
                return null;
            }

            List<WayPoint> options = new List<WayPoint>();

            WayPoint wp = currentTile.GetComponent<WayPoint>();

            if (wp != null && wp.IsActive)
            {
                //Steht auf Wegpunkt
                foreach (CorridorData corridor in wp.Corridors)
                {
                    foreach (WayPoint waypoint in corridor.WayPoints)
                    {
                        if (waypoint != wp && !options.Contains(waypoint))
                            options.Add(waypoint);
                    }
                }
            }
            else if (wp == null || (wp != null && !wp.IsActive))
            {
                //Steht nicht auf Wegpunkt
                CorridorData corridor = currentTile.corridor;
                options = corridor.WayPoints;
            }

            List<WayPoint> updatedOptions = new List<WayPoint>(options);

            NavMeshPath pathToCore = new NavMeshPath();
            GetComponent<NavMeshAgent>().CalculatePath(_coreSet[0].transform.position, pathToCore);
            //Debug.Log("updated Count before: " + updatedOptions.Count);
            foreach (WayPoint wayPoint in options)
            {
                if (visited.Contains(wayPoint) && updatedOptions.Contains(wayPoint) && wayPoint.CoreDistance > GetDistance(pathToCore))
                {
                    updatedOptions.Remove(wayPoint);
                    //Debug.Log("removed wayPoint");
                }
            }
            //Debug.Log("updated Count after: " + updatedOptions.Count);

            if (updatedOptions.Count <= 0)
            {
                //Debug.Log("Used BaseOptionList");
                return options;
            }

            //Debug.Log("Used UpdatedOptionList");
            return updatedOptions;
        }

        public WayPoint DecideFromOptions(List<WayPoint> options)
        {
            /*
            10 30 60
            100/concrete
            10 = 10
            30 = 3.33
            60 = 1.66
            
            nach runden
            10 = 10, 30 = 3, 60 = 2
            
            10 + 3 + 2 = 15
            
            get random 1-15
            */
            float totalDistance = 0;
            int index = 0;
            int currentTotal = 0;
            List<int> end = new List<int>();
            end.Add(1);
            foreach (WayPoint wp in options)
            {
                totalDistance += wp.CoreDistance;
            }

            for (int i = 0; i < options.Count; i++)
            {
                end.Add(currentTotal + (int) (totalDistance / options[i].CoreDistance) * 10);
                currentTotal += (int) (totalDistance / options[i].CoreDistance) * 10;
                //Debug.Log("end" + end[i]);
            }
            //Debug.Log("endcount" + end.Count);
            //Debug.Log("optionscount" + options.Count);

            int random = Random.Range(1, currentTotal + 1);
            //Debug.Log("currtotal" + currentTotal);
            //Debug.Log("random" + random);

            for (int j = 0; j < options.Count; j++)
            {
                if (random >= end[j] && random <= end[j + 1])
                {
                    //Debug.Log(options[j]); 
                    return options[j];
                }
            }

            return null;
        }

        public WayPoint DecideFromOptions2(List<WayPoint> options)
        {
            int random = Random.Range(0, options.Count);

            return options[random];
        }

        public WayPoint DecideFromOptions3(List<WayPoint> options)
        {
            foreach (WayPoint wp in options)
            {
                if (wp.isNextToCore)
                    return wp;
            }
            
            int random = Random.Range(1, 101);

            Tile currentTile = GetCurrentPosition();
            //Debug.Log("currentTile :" + currentTile);
            if (currentTile.GetComponent<WayPoint>() != null)
                currentWPDistance = currentTile.GetComponent<WayPoint>().CoreDistance;


            WayPoint destination;

            //Debug.Log("Random:  " + random+ "\n" + "wpCount:  " + wpCount);
            if (random <= smartness + wpCount * 3 && currentWPDistance != null)
            {
                int k = 0;
                do
                {
                    random = Random.Range(0, options.Count);

                    destination = options[random];
                    k++;
                } while (destination.CoreDistance > currentWPDistance && k < 11);
            }
            else
            {
                random = Random.Range(0, options.Count);

                destination = options[random];
            }

            visited.Add(destination);
            wpCount++;

            return destination;
        }


        public Tile GetCurrentPosition()
        {
            RaycastHit hit;
            if (Physics.Raycast(gameObject.transform.position, Vector3.down, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.TryGetComponent(out Tile tile))
                {
                    //Destroy(hit.collider.gameObject);
                    return tile;
                }
            }

            return null;
        }
        
        private float GetDistance(NavMeshPath path)
        {
            //Debug.Log(path.status);
            float distance = 0f;
            if (path.status != NavMeshPathStatus.PathInvalid)
            {
                for (int i = 0; i < path.corners.Length - 1; i++)
                {
                    distance += Vector3.Distance(path.corners[i], path.corners[i + 1]);
                }
            }

            //Debug.Log(distance);
            return distance;
        }
    }
}