using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.UI;
using Heroes.World;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Debug = UnityEngine.Debug;

namespace Heroes.Pathfinding
{
    public class CorridorDivider : MonoBehaviour, IDependency
    {
        public Tile startBlock;
        public Tile core;
        public Material markMat;
        [SerializeField] private RuntimeSet<Tile> _coreSet;
        [SerializeField] private RuntimeSet<Tile> _spawnPointSet;

        public event Action DependencyCompleted;

        public List<CorridorData> corridors = new List<CorridorData>();

        public float waitTime;

        [Button]
        public void CalculateMapData()
        {
            ResetCorridors();
            Divide();
            SetWayPointData();

            DependencyCompleted?.Invoke();
        }

        public void ResetMapData()
        {
            ResetCorridors();
        }

        [Button]
        public void StartDivide()
        {
            StartCoroutine(DebugDivide());
            //Divide();
        }

        [Button]
        public void Print()
        {
            PrintCorridors();
        }

        [Button]
        public void WayPoints()
        {
            SetWayPointData();
        }

        [Button]
        public void ByeByeWayPoints()
        {
            ResetWayPointData();
        }

        #region DebugDivide

        private IEnumerator DebugDivide()
        {
            List<Tile> openSet = new List<Tile>();
            List<Tile> closedSet = new List<Tile>();
            List<Tile> stopPoints = new List<Tile>();

            Tile startBlock1 = _spawnPointSet[0];
            Tile startBlock2 = _spawnPointSet[1];
            core = _coreSet[0];

            List<Tile> spawnPointTiles = new List<Tile>();

            foreach (Tile tile in _spawnPointSet)
            {
                spawnPointTiles.Add(getTileBelow(tile));
                stopPoints.Add(getTileBelow(tile));
                closedSet.Add(getTileBelow(tile));
            }

            //stopPoints.Add(getTileBelow(startBlock1));
            //closedSet.Add(getTileBelow(startBlock1));

            //stopPoints.Add(getTileBelow(startBlock2));
            //closedSet.Add(getTileBelow(startBlock2));

            //stopPoints.Add(startBlock.GetComponent<Tile>());
            //closedSet.Add(startBlock.GetComponent<Tile>());
            Tile current;

            bool running = true;

            bool inRoom = false;


            corridors.Add(new CorridorData());
            corridors[corridors.Count - 1].Tiles = new List<Tile>();
            corridors[corridors.Count - 1].WayPointTiles = new List<Tile>();
            corridors[corridors.Count - 1].WayPoints = new List<WayPoint>();
            SetWayPointsAroundCore(getTileBelow(core), openSet, stopPoints);


            while (running)
            {
                int i = 0;
                bool newStartFound = false;

                corridors.Add(new CorridorData());
                corridors[corridors.Count - 1].Tiles = new List<Tile>();
                corridors[corridors.Count - 1].WayPointTiles = new List<Tile>();
                corridors[corridors.Count - 1].WayPoints = new List<WayPoint>();
                while (i < stopPoints.Count && !newStartFound)
                {
                    foreach (Tile neighbour in stopPoints[i].NeighborMatrix)
                    {
                        if (!closedSet.Contains(neighbour) && neighbour != null &&
                            neighbour != stopPoints[i].NeighborMatrix[1, 1] && !neighbour.BelowTile)
                        {
                            if (openSet.Count == 0)
                            {
                                openSet.Add(neighbour);
                                corridors[corridors.Count - 1].Tiles.Add(stopPoints[i]);
                                corridors[corridors.Count - 1].WayPointTiles.Add(stopPoints[i]);
                                inRoom = CheckRoomStatus(neighbour, stopPoints[i]);
                                if (spawnPointTiles.Contains(stopPoints[i]))
                                {
                                    corridors[corridors.Count - 1].WayPointTiles.Add(stopPoints[i]);
                                    corridors[corridors.Count - 1].Tiles.Add(stopPoints[i]);
                                    stopPoints[i].corridor = corridors[corridors.Count - 1];
                                    stopPoints[i].corridorIndex = corridors.Count - 1;
                                }

                                newStartFound = true;
                            }
                        }
                    }

                    i++;
                }

                if (openSet.Count == 0)
                {
                    running = false;
                }

                while (openSet.Count > 0)
                {
                    current = openSet[0];
                    yield return new WaitForSeconds(waitTime);


                    if (!stopPoints.Contains(current) && !closedSet.Contains(current))
                    {
                        current.gameObject.transform.position += new Vector3(0, 3, 0);
                        current.transform.GetChild(4).GetComponent<Renderer>().material = markMat;
                    }

                    corridors[corridors.Count - 1].Tiles.Add(current);
                    current.corridor = corridors[corridors.Count - 1];
                    current.corridorIndex = corridors.Count - 1;
                    closedSet.Add(current);


                    foreach (Tile neighbour in current.NeighborMatrix)
                    {
                        if (stopPoints.Contains(neighbour))
                        {
                            //Debug.Log("-------------------------" + neighbour);
                            //corridors[corridors.Count - 1].Tiles.Add(neighbour);
                            if (!corridors[corridors.Count - 1].Tiles.Contains(neighbour))
                                corridors[corridors.Count - 1].Tiles.Add(neighbour);
                            //corridors[corridors.Count - 1].WayPointTiles.Add(neighbour);
                            // Debug.Log(corridors.Count-1);
                        }

                        if (!closedSet.Contains(neighbour) && !stopPoints.Contains(current) &&
                            !openSet.Contains(neighbour) && neighbour != null &&
                            neighbour != current.NeighborMatrix[1, 1] && !neighbour.BelowTile)
                        {
                            openSet.Add(neighbour);

                            bool stopPointCriteria;
                            //Debug.Log("inRoom: " + inRoom);

                            if (inRoom)
                            {
                                stopPointCriteria = !stopPoints.Contains(neighbour)
                                                    && (neighbour.Front.BelowTile && neighbour.Back.BelowTile &&
                                                        !neighbour.Left.BelowTile && !neighbour.Right.BelowTile
                                                        || !neighbour.Front.BelowTile && !neighbour.Back.BelowTile &&
                                                        neighbour.Left.BelowTile && neighbour.Right.BelowTile);
                            }
                            else
                            {
                                stopPointCriteria = 2 != GetOccupiedneighbours(neighbour) &&
                                                    !stopPoints.Contains(neighbour);
                            }

                            if (stopPointCriteria)
                            {
                                //Debug.Log("stop");
                                stopPoints.Add(neighbour);
                                corridors[corridors.Count - 1].WayPointTiles.Add(neighbour);
                            }
                        }
                    }

                    openSet.Remove(current);
                }
            }

            //SetWayPointsAroundCore(core, closedSet, stopPoints);
            //Debug.Log("KnotenPunte: " + stopPoints.Count);
            //Debug.Log("Korridore: " + corridors.Count);
            //Debug.Log("ClosedSet: " + closedSet.Count);
            //Debug.Log("OpenSet: " + openSet.Count);
            //Debug.Log("----------------------------------------------------");
        }

        #endregion

        #region Divide

        private void Divide()
        {
            List<Tile> openSet = new List<Tile>();
            List<Tile> closedSet = new List<Tile>();
            List<Tile> stopPoints = new List<Tile>();

            //startBlock = GameObject.Find("Ground(Clone): 24 : 30");
            //core = GameObject.Find("Ground(Clone): 9 : 1");

            startBlock = _spawnPointSet[0];
            core = _coreSet[0];

            List<Tile> spawnPointTiles = new List<Tile>();

            //stopPoints.Add(getTileBelow(startBlock));
            //closedSet.Add(getTileBelow(startBlock));
            foreach (Tile tile in _spawnPointSet)
            {
                spawnPointTiles.Add(getTileBelow(tile));
                stopPoints.Add(getTileBelow(tile));
                closedSet.Add(getTileBelow(tile));
            }


            //stopPoints.Add(startBlock.GetComponent<Tile>());
            //closedSet.Add(startBlock.GetComponent<Tile>());
            Tile current;

            bool running = true;

            bool inRoom = false;

            corridors.Add(new CorridorData());
            corridors[corridors.Count - 1].Tiles = new List<Tile>();
            corridors[corridors.Count - 1].WayPointTiles = new List<Tile>();
            corridors[corridors.Count - 1].WayPoints = new List<WayPoint>();
            SetWayPointsAroundCore(getTileBelow(core), openSet, stopPoints);

            while (running)
            {
                int i = 0;
                bool newStartFound = false;

                corridors.Add(new CorridorData());
                corridors[corridors.Count - 1].Tiles = new List<Tile>();
                corridors[corridors.Count - 1].WayPointTiles = new List<Tile>();
                corridors[corridors.Count - 1].WayPoints = new List<WayPoint>();
                while (i < stopPoints.Count && !newStartFound)
                {
                    foreach (Tile neighbour in stopPoints[i].NeighborMatrix)
                    {
                        if (!closedSet.Contains(neighbour) && neighbour != null &&
                            neighbour != stopPoints[i].NeighborMatrix[1, 1] && !neighbour.BelowTile)
                        {
                            if (openSet.Count == 0)
                            {
                                openSet.Add(neighbour);
                                corridors[corridors.Count - 1].Tiles.Add(stopPoints[i]);
                                corridors[corridors.Count - 1].WayPointTiles.Add(stopPoints[i]);
                                inRoom = CheckRoomStatus(neighbour, stopPoints[i]);
                                if (spawnPointTiles.Contains(stopPoints[i]))
                                {
                                    corridors[corridors.Count - 1].WayPointTiles.Add(stopPoints[i]);
                                    corridors[corridors.Count - 1].Tiles.Add(stopPoints[i]);
                                    stopPoints[i].corridor = corridors[corridors.Count - 1];
                                    stopPoints[i].corridorIndex = corridors.Count - 1;
                                }

                                newStartFound = true;
                            }
                        }
                    }

                    i++;
                }

                if (openSet.Count == 0)
                {
                    running = false;
                }

                while (openSet.Count > 0)
                {
                    current = openSet[0];

                    corridors[corridors.Count - 1].Tiles.Add(current);
                    current.corridor = corridors[corridors.Count - 1];
                    current.corridorIndex = corridors.Count - 1;
                    closedSet.Add(current);


                    foreach (Tile neighbour in current.NeighborMatrix)
                    {
                        if (stopPoints.Contains(neighbour))
                        {
                            //Debug.Log("-------------------------" + neighbour);
                            //corridors[corridors.Count - 1].Tiles.Add(neighbour);
                            if (!corridors[corridors.Count - 1].Tiles.Contains(neighbour))
                                corridors[corridors.Count - 1].Tiles.Add(neighbour);
                            //corridors[corridors.Count - 1].WayPointTiles.Add(neighbour);
                            //Debug.Log(corridors.Count-1);
                        }

                        if (!closedSet.Contains(neighbour) && !stopPoints.Contains(current) &&
                            !openSet.Contains(neighbour) && neighbour != null &&
                            neighbour != current.NeighborMatrix[1, 1] && !neighbour.BelowTile)
                        {
                            openSet.Add(neighbour);

                            bool stopPointCriteria;
                            //Debug.Log("inRoom: " + inRoom);

                            if (inRoom)
                            {
                                stopPointCriteria = !stopPoints.Contains(neighbour)
                                                    && (neighbour.Front.BelowTile && neighbour.Back.BelowTile &&
                                                        !neighbour.Left.BelowTile && !neighbour.Right.BelowTile
                                                        || !neighbour.Front.BelowTile && !neighbour.Back.BelowTile &&
                                                        neighbour.Left.BelowTile && neighbour.Right.BelowTile);
                            }
                            else
                            {
                                stopPointCriteria = 2 != GetOccupiedneighbours(neighbour) &&
                                                    !stopPoints.Contains(neighbour);
                            }

                            if (stopPointCriteria)
                            {
                                //Debug.Log("stop");
                                stopPoints.Add(neighbour);
                                corridors[corridors.Count - 1].WayPointTiles.Add(neighbour);
                            }
                        }
                    }

                    openSet.Remove(current);
                }
            }

            //SetWayPointsAroundCore(core, closedSet, stopPoints);
            //Debug.Log("KnotenPunte: " + stopPoints.Count);
            //Debug.Log("Korridore: " + corridors.Count);
            //Debug.Log("ClosedSet: " + closedSet.Count);
            //Debug.Log("OpenSet: " + openSet.Count);
            //Debug.Log("----------------------------------------------------");
        }

        #endregion

        #region ResetCorridors

        private void ResetCorridors()
        {
            WayPoint wp;
            foreach (CorridorData corr in corridors)
            {
                foreach (Tile tile in corr.WayPointTiles)
                {
                    if (tile.GetComponent<WayPoint>() != null)
                    {
                        wp = tile.GetComponent<WayPoint>();
                        wp.IsActive = false;
                        wp.enabled = false;
                    }
                }
            }

            corridors.Clear();
        }

        #endregion

        #region SetWayPoints

        private void SetWayPointData()
        {
            Tile coreTile = getTileBelow(core).GetComponent<Tile>();
            foreach (CorridorData corr in corridors)
            {
                foreach (Tile tile in corr.WayPointTiles)
                {
                    WayPoint wp;
                    NavMeshPath path = new NavMeshPath();
                    NavMesh.CalculatePath(tile.transform.position + new Vector3(0, 1f, 0),
                        core.transform.position + new Vector3(0, 0, 2.5f), NavMesh.AllAreas, path);
                    if (tile.gameObject.GetComponent<WayPoint>() == null)
                    {
                        tile.gameObject.AddComponent<WayPoint>();
                    }

                    wp = tile.gameObject.GetComponent<WayPoint>();
                    wp.enabled = true;
                    wp.IsActive = true;
                    //TODO replace with extention function
                    wp.CoreDistance = GetDistance(path);
                    corr.WayPoints.Add(wp);

                    foreach (Tile neighbour in tile.NeighborMatrix)
                    {
                        if (neighbour != null && neighbour != tile.NeighborMatrix[1, 1] && neighbour == coreTile)
                        {
                            wp.isNextToCore = true;
                        }
                    }

                    //TODO probably needs to be refactured
                    //GetWayPointCorridors(wp);
                    //wp.TileReference = tile;
                }
            }

            GetWayPointCorridors();
        }

        #endregion

        #region GetWayPointCorridors

        private void GetWayPointCorridors()
        {
            foreach (CorridorData corr in corridors)
            {
                foreach (Tile tile in corr.WayPointTiles)
                {
                    if (tile == null)
                        return;

                    foreach (Tile neighbour in tile.NeighborMatrix)
                    {
                        if (neighbour != null && neighbour != tile.NeighborMatrix[1, 1] && !neighbour.BelowTile)
                        {
                            WayPoint wp = tile.GetComponent<WayPoint>();
                            if (wp == null)
                                return;

                            if (!wp.Corridors.Contains(neighbour.corridor))
                            {
                                wp.Corridors.Add(neighbour.corridor);
                                neighbour.corridor.WayPoints.Add(wp);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region ResetWayPointData

        private void ResetWayPointData()
        {
            foreach (CorridorData corr in corridors)
            {
                foreach (Tile tile in corr.WayPointTiles)
                {
                    if (tile.gameObject.GetComponent<WayPoint>() != null)
                    {
                        WayPoint wp;
                        wp = tile.gameObject.GetComponent<WayPoint>();
                        wp.IsActive = false;
                        wp.CoreDistance = 0f;
                        wp.enabled = false;
                    }
                }
            }
        }

        #endregion

        #region HelpFunctions

        private float GetDistance(NavMeshPath path)
        {
            //Debug.Log(path.status);
            float distance = 0f;
            if (path.status != NavMeshPathStatus.PathInvalid)
            {
                for (int i = 0; i < path.corners.Length - 1; i++)
                {
                    distance += Vector3.Distance(path.corners[i], path.corners[i + 1]);
                }
            }

            //Debug.Log(distance);
            return distance;
        }

        private int GetNeighbourCount(Tile tile)
        {
            int i = 0;
            foreach (Tile neighbour in tile.NeighborMatrix)
            {
                if (neighbour != null && !neighbour.BelowTile)
                    i++;
            }

            return i;
        }

        [Button]
        public void ShowCorridor(int placeholder)
        {
            foreach (Tile tile in corridors[placeholder].Tiles)
            {
                tile.gameObject.transform.position += new Vector3(0, 3, 0);
            }
        }

        public int GetOccupiedneighbours(Tile tile)
        {
            int nc = 0;

            foreach (Tile neighbour in tile.NeighborMatrix)
            {
                if (neighbour != null && neighbour != tile.NeighborMatrix[1, 1] && neighbour.BelowTile)
                    nc++;
            }

            return nc;
        }

        private bool CheckRoomStatus(Tile tile, Tile startPoint)
        {
            bool isRoom = false;

            if (tile != null && tile != startPoint && tile != tile.NeighborMatrix[1, 1] && !tile.BelowTile)
            {
                int nc = GetNeighbourCount(tile);
                if (nc > 2)
                    isRoom = true;
                else if (nc == 2 && !IsStreet(tile))
                {
                    foreach (Tile neighbour in tile.NeighborMatrix)
                    {
                        if (neighbour != tile.NeighborMatrix[1, 1] && neighbour != startPoint &&
                            neighbour.BelowTile == null
                            && !IsStreet(neighbour))
                        {
                            isRoom = true;
                        }
                    }
                }
            }

            return isRoom;
        }

        private bool IsStreet(Tile tile)
        {
            return (tile.Front.BelowTile && tile.Back.BelowTile && !tile.Left.BelowTile && !tile.Right.BelowTile
                    || !tile.Front.BelowTile && !tile.Back.BelowTile && tile.Left.BelowTile && tile.Right.BelowTile);
        }

        private void PrintCorridors()
        {
            foreach (CorridorData corr in corridors)
            {
                Debug.Log(corr.AsString());
            }
        }

        private void SetWayPointsAroundCore(Tile core, List<Tile> openSet, List<Tile> stopPoints)
        {
            //Tile coreTile = null;
            //coreTile = core.GetComponent<Tile>();
            /*RaycastHit hit;
            if (Physics.Raycast(core.transform.position, Vector3.down, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.GetComponent<Tile>() != null)
                {
                    coreTile = hit.transform.GetComponent<Tile>();
                    //return hit.collider.gameObject.GetComponent<Tile>();
                }
            }*/

            // if (coreTile == null)
            //    return;

            foreach (Tile neighbour in core.NeighborMatrix)
            {
                if (neighbour != null && neighbour != core.NeighborMatrix[1, 1] && !neighbour.BelowTile)
                {
                    openSet.Add(neighbour);
                    stopPoints.Add(neighbour);
                    corridors[corridors.Count - 1].Tiles.Add(neighbour);
                    corridors[corridors.Count - 1].WayPointTiles.Add(neighbour);
                }
            }
        }

        private Tile getTileBelow(Tile originTile)
        {
            RaycastHit hit;
            if (Physics.Raycast(originTile.transform.position, Vector3.down, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.GetComponent<Tile>() != null)
                {
                    Tile belowTile = hit.transform.GetComponent<Tile>();
                    //return hit.collider.gameObject.GetComponent<Tile>();
                    return belowTile;
                }
            }

            return null;
        }

        #endregion
    }
}