using System;
using System.Collections.Generic;
using Heroes.World;

namespace Heroes.Pathfinding
{
    public class CorridorData
    {
        public List<Tile> Tiles { get; set; }
        
        public List<Tile> WayPointTiles { get; set; }
        
        public List<WayPoint> WayPoints { get; set; }

        public string AsString()
        {
            string output = "Tiles=";
            foreach (Tile tile in Tiles)
            {
                output += " " + tile;
            }

            output += "\n Waypoints=";

            foreach (Tile tile in WayPointTiles)
            {
                output += " " + tile;
            }

            return output;
        }
    }
}
