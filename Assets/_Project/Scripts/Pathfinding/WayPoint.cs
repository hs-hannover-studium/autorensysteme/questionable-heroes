using System.Collections;
using System.Collections.Generic;
using Heroes.Pathfinding;
using Heroes.World;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    public class WayPoint : SerializedMonoBehaviour
    {
        [ShowInInspector]
        public bool IsActive { get; set; }

        [ShowInInspector] public List<CorridorData> Corridors { get; set; } = new List<CorridorData>();
        
        //public Tile TileReference { get; set; }
        
        [ShowInInspector]
        public float CoreDistance { get; set; }

        [ShowInInspector] public bool isNextToCore = false;

        [Button]
        public void showreachable()
        {
            foreach (CorridorData corridor in Corridors)
            {
                foreach (WayPoint wayPoint in corridor.WayPoints)
                {
                    wayPoint.transform.position += Vector3.up*3 ;
                }
            }
        }
    }
}
