using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes.Pathfinding
{
    public class AgentController : MonoBehaviour
    {
        public GameObject rallyPoint;
        private Vector3 _targetPosition;
        
        public GameObject marker;
        private List<Vector3> _markerPos = new List<Vector3>();
        private NavMeshPath _path;

        private NavMeshAgent _agent;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _path = new NavMeshPath();
            _targetPosition = rallyPoint.transform.position;
        }

        /*
        private void Start()
        {
            agent.SetDestination(targetPosition);
        }
        */

        private void OnEnable()
        {
            GetComponent<PathMarker>().MarkPath(_agent, _targetPosition);

            _agent.SetDestination(_targetPosition);
        }

        private void MarkPath()
        {
            foreach (Vector3 pos in _markerPos)
            {
                Instantiate(marker, pos, Quaternion.identity);
            }
        }

        private void GetMarkerPos()
        {
            _agent.CalculatePath(_targetPosition, _path);
            foreach (Vector3 pos in _path.corners)
            {
                Instantiate(marker, pos, Quaternion.identity);
            }

            int i = 0;
            while (i < _path.corners.Length - 1)
            {
                Vector3 connection = _path.corners[i+1] - _path.corners[i];
                float distance = connection.magnitude;
                int j = 0;
                while (j * 0.1f <= distance)
                {
                    Vector3 connectionPos = _path.corners[i] + j * (connection / (distance/0.1f));
                    _markerPos.Add(connectionPos);
                    j ++;
                }

                _markerPos.Add(_path.corners[_path.corners.Length-1]);
                i++;
            }
        }
        
    }
}
