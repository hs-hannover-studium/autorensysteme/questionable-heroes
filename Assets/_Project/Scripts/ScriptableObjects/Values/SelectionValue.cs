using System;
using Heroes.Misc;
using Heroes.Selection;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.Values
{
    [CreateAssetMenu(fileName = "SelectionValue", menuName = "Values/SelectionValue", order = 0)]
    public class SelectionValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private Selectable defaultValue;

        [NonSerialized, ReadOnly, ShowInInspector]
        private Selectable runtimeValue;

        public event Action<Selectable> ValueChanged;

        public Selectable RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                if (runtimeValue)
                    runtimeValue.Deselect();

                runtimeValue = value;
                
                if (runtimeValue)
                    runtimeValue.Select();
                
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        protected virtual void OnValueChanged()
        {
            ValueChanged?.Invoke(runtimeValue);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }

        [Button]
        public void Deselect()
        {
            if (!RuntimeValue)
                return;
            
            RuntimeValue.Deselect();
            RuntimeValue = null;
        }
    }
}