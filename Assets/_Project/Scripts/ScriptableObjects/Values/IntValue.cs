using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.Values
{
    [CreateAssetMenu(fileName = "IntValue", menuName = "Values/IntValue", order = 0)]
    public class IntValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private int defaultValue;
        [NonSerialized, ReadOnly, ShowInInspector] private int runtimeValue;

        public event Action<int> ValueChanged;

        public int DefaultValue => defaultValue;

        public int RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                runtimeValue = value;
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        protected virtual void OnValueChanged()
        {
            ValueChanged?.Invoke(runtimeValue);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }

        [Button]
        public void Add(int value = 1)
        {
            RuntimeValue += value;
        }

        [Button]
        public virtual void Subtract(int value = 1)
        {
            RuntimeValue -= value;
        }

        [Button]
        public void Reset()
        {
            RuntimeValue = defaultValue;
        }
    }
}