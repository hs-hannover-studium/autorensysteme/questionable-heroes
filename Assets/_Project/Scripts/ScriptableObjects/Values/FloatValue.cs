using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.Values
{
    [CreateAssetMenu(fileName = "FloatValue", menuName = "Values/FloatValue", order = 0)]
    public class FloatValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] protected float defaultValue;
        [NonSerialized, ReadOnly, ShowInInspector] protected float runtimeValue;

        public event Action<float> ValueChanged;

        public float DefaultValue => defaultValue;

        public virtual float RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                runtimeValue = value;
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        protected virtual void OnValueChanged()
        {
            ValueChanged?.Invoke(runtimeValue);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }
        
        [Button]
        public void Add(int value = 1)
        {
            RuntimeValue += value;
        }

        [Button]
        public void Subtract(int value = 1)
        {
            RuntimeValue -= value;
        }
    }
}