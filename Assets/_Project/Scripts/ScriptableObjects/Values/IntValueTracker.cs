using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.ScriptableObjects.Values
{
    [CreateAssetMenu(fileName = "IntValueTracker", menuName = "Values/IntValueTracker", order = 0)]
    public class IntValueTracker : ScriptableObject
    {
        [Serializable]
        public class ActionPoint
        {
            public int value;
            public UnityEvent OnPointReached;
        }

        public IntValue intValue;
        public List<ActionPoint> actionPoints;

        private void OnEnable()
        {
            if (!intValue)
                return;
            
            intValue.ValueChanged += OnValueChanged;
        }

        private void OnDisable()
        {
            if (!intValue)
                return;
            
            intValue.ValueChanged -= OnValueChanged;
        }

        private void OnValueChanged(int value)
        {
            foreach (ActionPoint point in actionPoints)
            {
                if (value == point.value)
                    point.OnPointReached.Invoke();
            }
        }
    }
}