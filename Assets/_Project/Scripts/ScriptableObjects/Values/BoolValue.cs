using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.Values
{
    [CreateAssetMenu(fileName = "BoolValue", menuName = "Values/BoolValue", order = 0)]
    public class BoolValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private bool defaultValue;
        [NonSerialized, ReadOnly, ShowInInspector] private bool runtimeValue;

        public event Action<bool> ValueChanged;

        public bool DefaultValue => defaultValue;

        public bool RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                runtimeValue = value;
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        protected virtual void OnValueChanged()
        {
            ValueChanged?.Invoke(runtimeValue);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }
    }
}