using System;
using Heroes.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "DependencySet", menuName = "RuntimeSets/DependencySet", order = 0)]
    public class DependencySet : RuntimeSet<IDependency>
    {
        [ShowInInspector, ReadOnly] private int _completedDependencies;

        public event Action DependenciesCompleted;
        public event Action DependenciesReset;
        
        public override void Add(IDependency t)
        {
            base.Add(t);

            t.DependencyCompleted += OnDependencyCompleted;
        }

        public override void Remove(IDependency t)
        {
            t.DependencyCompleted -= OnDependencyCompleted;

            base.Remove(t);
        }

        public void ResetCompletedDependenciesCount()
        {
            _completedDependencies = 0;
        }

        public void OnDependencyCompleted()
        {
            _completedDependencies++;
            
            if (_completedDependencies >= Count)
                DependenciesCompleted?.Invoke();
        }

        public override void OnAfterDeserialize()
        {
            base.OnAfterDeserialize();

            _completedDependencies = 0;
        }
    }
}