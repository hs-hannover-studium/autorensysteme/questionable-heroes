using Heroes.World;
using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "TileSet", menuName = "RuntimeSets/TileSet", order = 0)]
    public class TileSet : RuntimeSet<Tile>
    {
        
    }
}