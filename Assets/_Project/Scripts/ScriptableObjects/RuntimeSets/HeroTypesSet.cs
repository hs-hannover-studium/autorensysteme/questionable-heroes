using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "HeroTypes", menuName = "RuntimeSets/HeroTypesSet", order = 0)]
    public class HeroTypesSet : RuntimeSet<HeroData>
    {
        
    }
}