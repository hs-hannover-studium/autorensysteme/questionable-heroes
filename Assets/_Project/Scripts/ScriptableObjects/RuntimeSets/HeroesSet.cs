using Heroes.Heroes;
using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "HeroesSet", menuName = "RuntimeSets/HeroesSet", order = 0)]
    public class HeroesSet : RuntimeSet<Hero>
    {
        public void KillAll()
        {
            for (var index = _runtimeValues.Count - 1; index >= 0; index--)
            {
                _runtimeValues[index].GetComponent<Damageable.Damageable>().Kill();
            }
        }
    }
}
