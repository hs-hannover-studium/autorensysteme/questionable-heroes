using Heroes.ScriptableObjects.Buildings;
using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "PlaceableBuildingsSet", menuName = "RuntimeSets/PlaceableBuildingsSet", order = 0)]
    public class PlaceableBuildingsSet : RuntimeSet<BuildingData>
    {
        
    }
}