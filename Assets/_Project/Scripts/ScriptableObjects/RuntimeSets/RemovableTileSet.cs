using Heroes.World.Tiles;
using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "RemovableTile", menuName = "RuntimeSets/RemovableTile", order = 0)]
    public class RemovableTileSet : RuntimeSet<RemovableTile>
    {
        public void RemoveAll()
        {
            for (var index = _runtimeValues.Count - 1; index >= 0; index--)
            {
                RemovableTile tile = _runtimeValues[index];
                tile.ForceRemove();
            }
        }
    }
}