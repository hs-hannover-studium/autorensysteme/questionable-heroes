using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    public abstract class RuntimeSet<T> : ScriptableObject, ISerializationCallbackReceiver, IEnumerable<T>
    {
        [SerializeField] [Tooltip("Resets RuntimeValue to DefaultValue on OnAfterDeserialize")]
        private bool resetOnLoad = true;

        [SerializeField] private List<T> defaultValue = new List<T>();

        [NonSerialized] [ShowInInspector] [ReadOnly]
        protected List<T> _runtimeValues = new List<T>();

        public event Action<T> ValueChanged;

        public int Count => _runtimeValues.Count;

        public T this[int i] => _runtimeValues[i];

        public virtual void Add(T t)
        {
            if (_runtimeValues.Contains(t))
                return;

            _runtimeValues.Add(t);
            ValueChanged?.Invoke(t);
        }

        public virtual void Remove(T t)
        {
            if (!_runtimeValues.Contains(t))
                return;

            _runtimeValues.Remove(t);
            ValueChanged?.Invoke(t);
        }

        public T Find(T t)
        {
            return _runtimeValues.Find( e => e.Equals(t));
        }

        public virtual void OnBeforeSerialize()
        {
        }

        public virtual void OnAfterDeserialize()
        {
            if (resetOnLoad)
                _runtimeValues = defaultValue;
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach(T t in _runtimeValues)
                yield return t;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}