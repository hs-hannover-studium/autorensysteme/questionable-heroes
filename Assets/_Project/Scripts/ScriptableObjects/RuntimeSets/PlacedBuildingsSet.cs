using Heroes.Buildings;
using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "PlacedBuildingsSet", menuName = "RuntimeSets/PlacedBuildingsSet", order = 0)]
    public class PlacedBuildingsSet : RuntimeSet<Building>
    {
        public void RepairAll()
        {
            foreach (Building building in _runtimeValues)
            {
                Damageable.Damageable damageable = building.GetComponent<Damageable.Damageable>();
                damageable.Health = damageable.MaxHealth;
            }
        }

        public void KillAll()
        {
            foreach (Building building in _runtimeValues)
            {
             building.GetComponent<Damageable.Damageable>().Kill();
            }
        }
    }
}