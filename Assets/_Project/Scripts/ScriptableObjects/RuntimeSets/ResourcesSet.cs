using System;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.Values;
using UnityEngine;

namespace Heroes.ScriptableObjects.RuntimeSets
{
    [CreateAssetMenu(fileName = "ResourcesSet", menuName = "RuntimeSets/ResourcesSet", order = 0)]   
    public class ResourcesSet : RuntimeSet<ResourceValue>
    {
        public event Action ResourceValuesChanged;
        
        private void OnEnable()
        {
            foreach (ResourceValue resource in _runtimeValues)
            {
                resource.ValueChanged += ResourceValueChanged;
            }
        }

        private void OnDisable()
        {
            foreach (ResourceValue resource in _runtimeValues)
            {
                resource.ValueChanged -= ResourceValueChanged;
            }
        }

        public bool HasEnoughResources(List<ResourceValuePair> cost)
        {
            foreach (ResourceValuePair costValue in cost)
            {
                ResourceValue resourceValue = Find(costValue.resourceValue);
                
                if (costValue.value > resourceValue.RuntimeValue)
                    return false;
            }

            return true;
        }
        
        public bool HasEnoughOf(ResourceValuePair cost)
        {
            ResourceValue resourceValue = Find(cost.resourceValue);
                
            return cost.value <= resourceValue.RuntimeValue;
        }

        public void PayResources(List<ResourceValuePair> cost)
        {
            foreach (ResourceValuePair costValue in cost)
            {
                ResourceValue resourceValue = Find(costValue.resourceValue);

                resourceValue.Subtract(costValue.value);
            }
        }
        
        public void Pay(ResourceValuePair cost)
        {
            ResourceValue resourceValue = Find(cost.resourceValue);

            resourceValue.Subtract(cost.value);
        }
        
        public void AddResources(List<ResourceValuePair> resources)
        {
            foreach (ResourceValuePair valuePair in resources)
            {
                ResourceValue resourceValue = Find(valuePair.resourceValue);

                resourceValue.Add(valuePair.value);
            }
        }

        private void ResourceValueChanged(int value)
        {
            ResourceValuesChanged?.Invoke();
        }
    }
}
