using System.Collections.Generic;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.Buildings
{
    [CreateAssetMenu(menuName = "Data/Building/Tower")]
    public class TowerData : BuildingData
    {
        [SerializeField, PreviewField]
        private GameObject projectilePrefab;

        [Header("Stats")]
        [ListDrawerSettings(Expanded = true)]
        public List<StatScaling> stats;

        [Header("Leveling")]
        [SerializeField]
        private int _maxLevel = 10;
        public List<ResourceScaling> levelCost;

        public GameObject Projectile => projectilePrefab;

        public int MaxLevel => _maxLevel;
    }
}
