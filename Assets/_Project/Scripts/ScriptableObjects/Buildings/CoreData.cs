using UnityEngine;

namespace Heroes.ScriptableObjects.Buildings
{
    [CreateAssetMenu(menuName = "Data/Building/Core")]
    public class CoreData : BuildingData
    {
        
    }
}