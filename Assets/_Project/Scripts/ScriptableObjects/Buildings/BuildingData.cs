using System.Collections.Generic;
using Heroes.Misc;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.Buildings
{
    public abstract class BuildingData : ScriptableObject
    {
        [SerializeField]
        private string buildingName;
        [SerializeField]
        private Sprite icon;
        [SerializeField, PreviewField]
        private GameObject buildingPrefab;
        
        [PropertySpace, SerializeField]
        public List<ResourceValuePair> cost;
        
        public string BuildingName => buildingName;

        public GameObject Building => buildingPrefab;

        public Sprite Icon => icon;
    }
}
