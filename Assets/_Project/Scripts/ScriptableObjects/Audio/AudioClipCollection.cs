using System.Collections.Generic;
using UnityEngine;

namespace Heroes.ScriptableObjects.Audio
{
    [CreateAssetMenu(fileName = "AudioClipCollection", menuName = "Audio/ClipCollection", order = 0)]
    public class AudioClipCollection : ScriptableObject
    {
        public List<AudioClip> collection;
    }
}