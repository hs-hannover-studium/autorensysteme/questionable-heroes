using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;

namespace Heroes.ScriptableObjects.Values
{
    [CreateAssetMenu(menuName = "Values/ResourceValue")]
    public class ResourceValue : IntValue
    {
        [SerializeField, PreviewField]
        private Sprite icon;

        public Sprite Icon => icon;

        public override void Subtract(int value = 1)
        {
            if (value >= RuntimeValue)
                RuntimeValue = 0;
            else
                RuntimeValue -= value;
        }
    }
}
