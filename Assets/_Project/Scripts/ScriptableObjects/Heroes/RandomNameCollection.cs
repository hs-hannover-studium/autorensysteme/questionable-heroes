using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [CreateAssetMenu(menuName = "Data/Random Name Collection")]
    public class RandomNameCollection : ScriptableObject
    {
        [SerializeField, ListDrawerSettings(Expanded = true)]
        private List<string> _firstName = new List<string>();

        [SerializeField, ListDrawerSettings(Expanded = true)]
        private List<string> _secondName = new List<string>();

        public string GenerateRandomName()
        {
            if (_firstName.Count == 0 || _secondName.Count == 0)
                return "";
            
            int random1 = Random.Range(0, _firstName.Count);
            int random2 = Random.Range(0, _secondName.Count);

            return _firstName[random1] + " " + _secondName[random2];
        }
    }
}
