using System.Collections.Generic;
using Heroes.ScriptableObjects.Values;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Data/Hero")]
    public class HeroData : ScriptableObject
    {
        [SerializeField]
        private string _heroClassName;
        [SerializeField, PreviewField]
        private Sprite _icon;
        [SerializeField, PreviewField]
        private GameObject _prefab;
        [SerializeField] private Color _heroColor;

        [Header("Stats")]
        [ListDrawerSettings(Expanded = true)]
        public List<StatScaling> stats;

        [PropertySpace]
        public List<ResourceScaling> capacity;

        [PropertySpace]
        public List<ResourceScaling> drops;

        public string HeroClassName => _heroClassName;

        public Sprite Icon => _icon;
        
        public GameObject Prefab => _prefab;

        public Color HeroColor => _heroColor;

        public StatScaling GetStat(StatType type)
        {
            foreach (var stat in stats)
            {
                if (stat.range.RangeType == type)
                    return stat;
            }

            return null;
        }
        
        public ResourceScaling GetCapacity(ResourceValue type)
        {
            foreach (var resource in capacity)
            {
                if (resource.range.RangeType == type)
                    return resource;
            }

            return null;
        }
    }
}
