using System;
using System.Collections.Generic;
using Heroes.ScriptableObjects.Events;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [CreateAssetMenu(menuName = "Dialogue/DialogueObject")]
    public class DialogueObject : ScriptableObject, ISerializationCallbackReceiver
    {
        public Speaker speaker;
        
        [SerializeField, ListDrawerSettings(Expanded = true), TextArea]
        private string[] _dialogue;
        
        [SerializeField] 
        private bool _playOnce;

        [ShowInInspector, ReadOnly, NonSerialized]
        private bool _played = false;

        public string[] Dialogue => _dialogue;

        public bool PlayOnce => _playOnce;

        public bool Played
        {
            get => _played;
            set => _played = value;
        }

        public void OnBeforeSerialize()
        {
            
        }

        public void OnAfterDeserialize()
        {
            Played = false;
        }
    }
}
