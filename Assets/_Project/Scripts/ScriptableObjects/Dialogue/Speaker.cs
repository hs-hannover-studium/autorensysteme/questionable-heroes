using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [CreateAssetMenu(menuName = "Dialogue/Speaker")]
    public class Speaker : ScriptableObject
    {
        [SerializeField]
        private string _name;
        
        [SerializeField, PreviewField]
        private Sprite _portrait;

        public string Name => _name;

        public Sprite Portrait => _portrait;
    }
}
