using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Heroes
{
    [CreateAssetMenu(menuName = "Dialogue/SpeechBubbleCollection")]
    public class SpeechBubbleCollection : ScriptableObject
    {
        [SerializeField, TextArea]
        private string[] _speechBubbles;

        public string[] SpeechBubbles => _speechBubbles;

        public string GetRandom()
        {
            if (SpeechBubbles.Length == 0)
                return string.Empty;
            
            int randomNum = Random.Range(0, SpeechBubbles.Length);
            return SpeechBubbles[randomNum];
        }
    }
}
