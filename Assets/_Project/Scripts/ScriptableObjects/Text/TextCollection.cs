using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Heroes.ScriptableObjects.Text
{
    [CreateAssetMenu(fileName = "TextCollection", menuName = "Text/TextCollection", order = 0)]
    public class TextCollection : ScriptableObject
    {
        public String[] text;

        public string RandomString()
        {
            int rand = Random.Range(0, text.Length);
            return text[rand];
        }
    }
}