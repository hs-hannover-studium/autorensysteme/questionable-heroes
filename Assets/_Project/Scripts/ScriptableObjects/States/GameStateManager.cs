using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.States
{
    [CreateAssetMenu(fileName = "GameStateManager", menuName = "Game/GameStateManager", order = 0)]
    public class GameStateManager : ScriptableObject, ISerializationCallbackReceiver
    {
        private GameState _activeState;

        [ShowInInspector]
        public GameState ActiveState => _activeState;

        public event Action<GameState> StateChanged;

        /// <summary>
        /// Sets current active state
        /// Calls exit event on previous state
        /// </summary>
        /// <param name="newState"></param>
        public void TransitionToState(GameState newState)
        {
            if (_activeState)
                _activeState.Exit();

            _activeState = newState;
           _activeState.Enter();
           StateChanged?.Invoke(newState);
        }

        public void ClearState()
        {
            if (!_activeState)
                return;
            
            _activeState.Exit();
            _activeState = null;
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            _activeState = null;
        }
    }
}