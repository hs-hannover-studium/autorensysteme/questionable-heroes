using Heroes.ScriptableObjects.Events;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.States
{
    [CreateAssetMenu(fileName = "GameState", menuName = "Game/State", order = 0)]
    public class GameState : ScriptableObject
    {
        public GameEvent enterEvent;
        public GameEvent exitEvent;
        public GameStateManager subManager;
        public string stateName;

        [Button]
        public void Enter()
        {
            if (!enterEvent)
                return;
            
            enterEvent.Raise();
        }

        [Button]
        public void Exit()
        {
            if (!exitEvent)
                return;
            
            if(subManager)
                subManager.ClearState();
            
            exitEvent.Raise();
        }
    }
}