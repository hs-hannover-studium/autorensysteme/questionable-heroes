using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.World
{
    [CreateAssetMenu(fileName = "Tile", menuName = "World/Tile", order = 0)]
    public class TileData : ScriptableObject
    {
        [PreviewField(75)]
        [HideLabel]
        [AssetList(Path = "_Project/Prefabs/World/Tiles/", AutoPopulate = true)]
        [AssetsOnly]
        public GameObject prefab;
        public Color color;
        [Range(0, 1)]
        public int heightLevel;
        [ShowIf("heightLevel", 1)]
        public GameObject tileBeneath;

        [ShowInInspector]
        public string Hex => ColorUtility.ToHtmlStringRGB(color);
    }
}