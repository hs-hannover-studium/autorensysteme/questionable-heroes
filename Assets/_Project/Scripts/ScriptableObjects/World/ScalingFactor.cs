using System;
using System.Collections.Generic;
using Heroes.ScriptableObjects.Values;
using UnityEngine;

namespace Heroes.ScriptableObjects.World
{
    [Serializable]
    public class ScalingFactor
    {
        [SerializeField] private IntValue source;
        [SerializeField] private int per = 100;
        [SerializeField] private float value = 1;

        public IntValue Source => source;

        public int Per => per;

        public int GetCount()
        {
            return Mathf.FloorToInt(Mathf.Floor((float)Source.RuntimeValue / Per) * value);
        }

        public static int Calculate(List<ScalingFactor> factors)
        {
            int value = 0;

            foreach (ScalingFactor scaling in factors)
            {
                value += scaling.GetCount();
            }

            return value;
        }
    }
}