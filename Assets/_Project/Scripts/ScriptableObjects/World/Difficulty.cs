using System;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.Values;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.World
{
    [Serializable]
    public class Difficulty
    {
        [SerializeField] [FoldoutGroup("Heros")]
        private int minHeroCount = 3;

        [SerializeField] [FoldoutGroup("Heros")]
        private List<ScalingFactor> heroCount;

        [SerializeField] [FoldoutGroup("Heros")]
        private List<ScalingFactor> heroLevel;
        
        [SerializeField] [FoldoutGroup("Resources")]
        private List<ResourceValuePair> startingResources;

        public int MinHeroCount => minHeroCount;

        public List<ResourceValuePair> StartingResources => startingResources;

        public List<ScalingFactor> HeroCountScaling => heroCount;

        public int HeroCount()
        {
            return ScalingFactor.Calculate(HeroCountScaling);
        }

        public int HeroStrength()
        {
            return ScalingFactor.Calculate(heroLevel);
        }
    }
}