using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.World
{
    [CreateAssetMenu(fileName = "GameSession", menuName = "GameSession", order = 0)]
    public class GameSession : ScriptableObject
    {
        [ReadOnly] private Difficulty difficulty;

        public Difficulty Difficulty
        {
            get => difficulty;
            set => difficulty = value;
        }
    }
}