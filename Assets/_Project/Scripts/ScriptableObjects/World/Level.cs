using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.ScriptableObjects.World
{
    [CreateAssetMenu(fileName = "Tile", menuName = "World/Level", order = 0)]
    public class Level : ScriptableObject
    {
        public float lightIntensity = 1f;
        public Color lightColor = Color.white;
        public Vector3 lightRotation = new Vector3(45f, 0, 0);
        public Difficulty difficulty;

        [AssetList(Path = "_Project/Textures/Level/", AutoPopulate = true)]
        public Texture2D texture;

        public TileData defaultBlock;
        public List<TileData> blocks = new List<TileData>();
    }
}