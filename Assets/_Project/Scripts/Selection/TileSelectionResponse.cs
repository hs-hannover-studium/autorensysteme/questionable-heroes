using Heroes.World;
using UnityEngine;

namespace Heroes.Selection
{
    [RequireComponent(typeof(Tile), typeof(Selectable))]
    public class TileSelectionResponse : SelectionResponse
    {
        public Color selection = Color.yellow;

        private Tile _tile;
        private Color _originalBaseColor;
        private Color _originalTopColor;

        private void Awake()
        {
            _tile = GetComponent<Tile>();
            selectable = GetComponent<Selectable>();
        }

        private void Start()
        {
            _originalBaseColor = _tile.activeVariation.meshRenderer.material.GetColor("_BaseColor");
            _originalTopColor = _tile.activeVariation.meshRenderer.material.GetColor("_TopColor");
        }
        
        public override void SelectionHighlight()
        {
            _tile.activeVariation.meshRenderer.material.SetColor("_BaseColor", selection);
            _tile.activeVariation.meshRenderer.material.SetColor("_TopColor", selection);
        }

        public override void ResetSelectionHighlight()
        {
            _tile.activeVariation.meshRenderer.material.SetColor("_BaseColor", _originalBaseColor);
            _tile.activeVariation.meshRenderer.material.SetColor("_TopColor", _originalTopColor);
        }
    }
}