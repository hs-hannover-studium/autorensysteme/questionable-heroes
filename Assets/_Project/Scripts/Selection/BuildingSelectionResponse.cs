using System.Collections.Generic;
using Heroes.Buildings;
using UnityEngine;

namespace Heroes.Selection
{
    [RequireComponent(typeof(Selectable), typeof(Building))]
    public class BuildingSelectionResponse : SelectionResponse
    {
        private Building _building;
        private List<Material> _baseMaterials = new List<Material>();
        
        [SerializeField] private Material _outlineMaterial;
        [SerializeField] private float _outlineThickness = 0.03f;
        [SerializeField] private Color _outlineColor = Color.yellow;

        private void Awake()
        {
            _building = GetComponent<Building>();
            foreach (var mesh in _building.ModelParts)
            {
                _baseMaterials.Add(mesh.material);
            }
        }
        
        public override void SelectionHighlight()
        {
            for (var i = 0; i < _building.ModelParts.Count; i++)
            {
                var mesh = _building.ModelParts[i];
                mesh.materials = new[] {_baseMaterials[i], _outlineMaterial};
                mesh.materials[1].SetFloat("_OutlineThickness", _outlineThickness);
                mesh.materials[1].SetColor("_OutlineColor", _outlineColor);
            }
        }

        public override void ResetSelectionHighlight()
        {
            for (var i = 0; i < _building.ModelParts.Count; i++)
            {
                var mesh = _building.ModelParts[i];
                mesh.materials = new[] {_baseMaterials[i]};
            }
        }
    }
}