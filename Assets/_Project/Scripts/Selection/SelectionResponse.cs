using UnityEngine;

namespace Heroes.Selection
{
    public abstract class SelectionResponse : MonoBehaviour
    {
        protected Selectable selectable;
        
        public abstract void SelectionHighlight();
        public abstract void ResetSelectionHighlight();
    }
}