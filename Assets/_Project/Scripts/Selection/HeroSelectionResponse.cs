using Heroes.Heroes;
using UnityEngine;

namespace Heroes.Selection
{
    [RequireComponent(typeof(Selectable), typeof(Hero))]
    public class HeroSelectionResponse : SelectionResponse
    {
        private Hero _hero;
        private Material _bodyBaseMaterial;
        private Material _weaponBaseMaterial;

        [SerializeField] private Material _outlineMaterial;
        [SerializeField] private float _outlineThickness = 0.03f;
        
        private void Awake()
        {
            _hero = GetComponent<Hero>();
            _bodyBaseMaterial = _hero.BodyMesh.material;
            _weaponBaseMaterial = _hero.WeaponMesh.material;
        }
        
        public override void SelectionHighlight()
        {
            _hero.BodyMesh.materials = new[] {_bodyBaseMaterial, _outlineMaterial};
            _hero.WeaponMesh.materials = new[] {_weaponBaseMaterial, _outlineMaterial};
            _hero.BodyMesh.materials[1].SetColor("_OutlineColor", _hero.Data.HeroColor);
            _hero.WeaponMesh.materials[1].SetColor("_OutlineColor", _hero.Data.HeroColor);
            _hero.BodyMesh.materials[1].SetFloat("_OutlineThickness", _outlineThickness);
            _hero.WeaponMesh.materials[1].SetFloat("_OutlineThickness", _outlineThickness);
        }

        public override void ResetSelectionHighlight()
        {
            _hero.BodyMesh.materials = new[] {_bodyBaseMaterial};
            _hero.WeaponMesh.materials = new[] {_weaponBaseMaterial}; 
        }
    }
}