using Heroes.ScriptableObjects.Values;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Heroes.Selection
{
    [RequireComponent(typeof(Camera))]
    public class SelectionHandler : MonoBehaviour
    {
        public bool allowSelection = true;

        [SerializeField] private LayerMask selectableMask;
        [SerializeField] private SelectionValue currentSelection;

        private Camera _cam;

        private void Awake()
        {
            _cam = GetComponent<Camera>();
        }

        private void Update()
        {
            if (Mouse.current.leftButton.wasPressedThisFrame)
                ClickSelect();
        }

        public void EnableSelection()
        {
            allowSelection = true;
        }

        public void DisableSelection()
        {
            allowSelection = false;
            currentSelection.Deselect();
        }

        private void ClickSelect()
        {
            bool pointerOverUI = EventSystem.current.IsPointerOverGameObject();
            if (pointerOverUI || !allowSelection) return;

            Ray ray = _cam.ScreenPointToRay(Mouse.current.position.ReadValue());
            bool successfulHit = Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, selectableMask);
            
            if (!successfulHit)
            {
                currentSelection.Deselect();
                return;
            }

            if (hit.transform.TryGetComponent(out Selectable selectable))
                selectable.Select();
        }
    }
}