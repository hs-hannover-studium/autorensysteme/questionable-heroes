using Heroes.ScriptableObjects.Values;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace Heroes.Selection
{
    public enum SelectionType
    {
        Unit,
        Core,
        Tower,
        Trap,
        RemovableTile
    }

    [RequireComponent(typeof(SelectionResponse))]
    public class Selectable : MonoBehaviour
    {
        public bool allowSelection = true;
        private bool _isSelected;
        private SelectionResponse _selectionResponse;
        
        [SerializeField] private SelectionValue selectionValue;
        [SerializeField] private SelectionType selectionType;

        [FormerlySerializedAs("OnClick")] [FoldoutGroup("Selection Events")] [SerializeField]
        private UnityEvent onSelect;
        
        [FoldoutGroup("Selection Events")] [SerializeField]
        private UnityEvent onDeselect;

        public SelectionType Type => selectionType;

        public bool IsSelected => _isSelected;

        private void Awake()
        {
            _selectionResponse = GetComponent<SelectionResponse>();
        }

        [Button]
        public void Select()
        {
            if (!allowSelection)
                return;
                
            onSelect.Invoke();
            selectionValue.RuntimeValue = this;
            _isSelected = true;
            _selectionResponse.SelectionHighlight();
        }
        
        [Button]
        public void Deselect()
        {
            onDeselect.Invoke();
            _isSelected = false;
            _selectionResponse.ResetSelectionHighlight();
        }
    }
}