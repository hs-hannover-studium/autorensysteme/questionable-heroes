using System;
using Heroes.ScriptableObjects.Values;
using UnityEngine;
using UnityEngine.UI;

namespace Heroes.Audio
{
    [RequireComponent(typeof(Slider))]
    public class AudioSlider : MonoBehaviour
    {
        [SerializeField] private AudioVolumeValue audioVolume;

        private Slider _slider;

        private void Awake()
        {
            _slider = GetComponent<Slider>();
        }

        private void OnEnable()
        {
            _slider.value = audioVolume.RuntimeValue;
        }

        public void ChangeVolume(float value)
        {
            audioVolume.RuntimeValue = value;
        }
    }
}