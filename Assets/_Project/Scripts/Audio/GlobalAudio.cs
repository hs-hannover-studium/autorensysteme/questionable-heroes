using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

namespace Heroes.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class GlobalAudio : MonoBehaviour
    {
        private static GlobalAudio Instance { get; set; }
        [SerializeField] private AudioVolumeValue[] audioVolumeValues;
        [SerializeField] private float transitionTime = 5f;

        private AudioSource audioSource;
        private Sequence sequence;
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) Destroy(gameObject);

            DontDestroyOnLoad(gameObject);

            audioSource = GetComponent<AudioSource>();
            sequence = DOTween.Sequence();
        }

        private void Start()
        {
            LoadSavedAudioVolumes();
        }

        private void OnDisable()
        {
            SaveAudioVolumes();
        }

        [Button]
        public void ChangeThemeTrack(AudioClip clip)
        {
            sequence.Complete();
            
            sequence = DOTween.Sequence();
            
            if (clip == audioSource.clip)
                return;
            
            AudioSource fadeSource = gameObject.AddComponent<AudioSource>();
            fadeSource.outputAudioMixerGroup = audioSource.outputAudioMixerGroup;
            fadeSource.loop = audioSource.loop;
            fadeSource.clip = clip;
            fadeSource.volume = 0;
            fadeSource.Play();

            Tween fadeInNewClip = DOTween.To(() => fadeSource.volume, x => fadeSource.volume = x, audioSource.volume, transitionTime);
            Tween fadeOutOldClip = DOTween.To(() => audioSource.volume, x => audioSource.volume = x, 0, transitionTime);
            
            sequence.Append(fadeInNewClip);
            sequence.Join(fadeOutOldClip);

            sequence.OnComplete(() =>
            {
                Destroy(audioSource);
                audioSource = fadeSource;
            });

        }

        private void LoadSavedAudioVolumes()
        {
            foreach (AudioVolumeValue volume in audioVolumeValues)
            {
                float audioVolume = PlayerPrefs.GetFloat(volume.AudioGroupName);
                volume.RuntimeValue = audioVolume == 0 ? 1 : audioVolume;
            }
        }

        private void SaveAudioVolumes()
        {
            foreach (AudioVolumeValue volume in audioVolumeValues)
            {
                PlayerPrefs.SetFloat(volume.AudioGroupName, volume.RuntimeValue);
            }
            
            PlayerPrefs.Save();
        }
    }
}