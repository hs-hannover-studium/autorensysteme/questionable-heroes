using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

namespace Heroes.Audio
{
    [CreateAssetMenu(fileName = "AudioVolumeValue", menuName = "Audio/AudioVolumeValue", order = 0)]
    public class AudioVolumeValue : ScriptableObject
    {
        [SerializeField] private AudioMixer audioMixer;
        [SerializeField] private string audioGroupName;
        
        [ReadOnly, SerializeField] private float runtimeValue;

        public event Action<float> ValueChanged;
        
        public float RuntimeValue
        {
            get => runtimeValue;
            set
            {
                runtimeValue = value;
                
                float audioVolume = value == 0 ? -80 : Mathf.Log10(value) * 20;
                
                Mixer.SetFloat(AudioGroupName, audioVolume);
                
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        public string AudioGroupName => audioGroupName;

        public AudioMixer Mixer => audioMixer;
    }
}