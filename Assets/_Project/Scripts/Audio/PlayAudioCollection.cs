using Heroes.ScriptableObjects.Audio;
using UnityEngine;

namespace Heroes.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class PlayAudioCollection : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;

        private void Awake()
        {
            audioSource = audioSource ? audioSource : GetComponent<AudioSource>();
        }

        public void PlayRandomOneShot(AudioClipCollection collection)
        {
            if (collection.collection.Count <= 0)
                return;
            
            int i = Random.Range(0, collection.collection.Count);

            audioSource.PlayOneShot(collection.collection[i]);
        }
    }
}