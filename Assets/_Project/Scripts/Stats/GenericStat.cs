using System;
using System.Collections;
using System.Collections.Generic;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Heroes
{
    [Serializable]
    public class GenericStat<T, U> where U: GenericRange<T>
    {
        public event Action<int> ValueChanged;

        [NonSerialized] private protected T _type;
        [NonSerialized] private protected int _runtimeDefaultValue;
        [NonSerialized] private protected int _runtimeValue;
        [NonSerialized] private GenericStatScaling<T, U> _scaling;

        [ReadOnly, ShowInInspector, LabelText("Default"), LabelWidth(50)]
        public int DefaultValue
        {
            get => _runtimeDefaultValue;
            set => _runtimeDefaultValue = value;
        }
        
        [ReadOnly, ShowInInspector, LabelText("Runtime"), LabelWidth(50)] 
        public virtual int RuntimeValue
        {
            get => _runtimeValue;
            set 
            {
                if (value == _runtimeValue)
                    return;

                _runtimeValue = value;
                ValueChanged?.Invoke(_runtimeValue);
            }
        }
        
        private GenericStatScaling<T, U> Scaling => _scaling;
        
        public GenericStat(U range)
        {
            _type = range.RangeType;
            _runtimeValue = Random.Range(range.Range.x, range.Range.y+1);
            _runtimeDefaultValue = _runtimeValue;
        }
        
        public GenericStat(GenericStatScaling<T, U> scaling)
        {
            _scaling = scaling;
            _type = scaling.range.RangeType;
            _runtimeValue = Random.Range(scaling.range.Range.x, scaling.range.Range.y+1);
            _runtimeDefaultValue = _runtimeValue;
        }
        
        
        public GenericStat(T type, int value)
        {
            _type = type;
            _runtimeValue = value;
            _runtimeDefaultValue = value;
        }
        
        public GenericStat(T type, Vector2Int range)
        {
            _type = type;
            _runtimeValue = Random.Range(range.x, range.y+1);
            _runtimeDefaultValue = _runtimeValue;
        }

        public void UpdateValue(int level)
        {
            Vector2Int minMax = _scaling.CalculateMinMaxScaled(level);
            RuntimeValue = Random.Range(minMax.x, minMax.y+1);
            DefaultValue = RuntimeValue;
        }
    }
}
