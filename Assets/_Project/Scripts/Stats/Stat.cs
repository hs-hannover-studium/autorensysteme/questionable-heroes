using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Heroes.Stats
{
    [Serializable]
    public class Stat: GenericStat<StatType, GenericRange<StatType>>
    {
        [ShowInInspector, HideLabel]
        private string Type => Enum.GetName(typeof(StatType), _type);

        public bool Equals(StatType type)
        {
            return _type == type;
        }

        public Stat(GenericRange<StatType> range) : base(range)
        {
        }

        public Stat(GenericStatScaling<StatType, GenericRange<StatType>> scaling) : base(scaling)
        {
        }

        public Stat(StatType type, int value) : base(type, value)
        {
        }

        public Stat(StatType type, Vector2Int range) : base(type, range)
        {
        }
    }
}
