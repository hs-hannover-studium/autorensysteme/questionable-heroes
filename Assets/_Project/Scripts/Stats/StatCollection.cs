using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.Stats
{
    [Serializable]
    public abstract class StatCollection
    {
        [PropertyOrder(1), ReadOnly] public List<Stat> stats = new List<Stat>();

        private int _level = 1;

        [ShowInInspector, ReadOnly, PropertyOrder(0)]
        public int Level
        {
            get => _level;
            private set
            {
                if (value <= 0)
                    return;
                
                _level = value;
            }
        }

        public event Action StatsChanged;

        public Stat this[StatType type]
        {
            get
            {
                foreach (var stat in stats)
                {
                    if (stat.Equals(type))
                        return stat;
                }

                Stat newStat = new Stat(type, new Vector2Int());
                stats.Add(newStat);
                return newStat;
            }
        }

        [Button, FoldoutGroup("Debug")]
        public void LevelUp()
        {
            SetLevel(Level + 1);
        }

        [Button, FoldoutGroup("Debug")]
        public virtual void SetLevel(int level)
        {
            if (!CheckAbilityToLevel(level-1))
                return;
            
            UseResourcesToLevel(level-1);
            
            Level = level;
            UpdateStats();
        }

        private protected abstract bool CheckAbilityToLevel(int level);

        private protected abstract void UseResourcesToLevel(int level);

        private protected virtual void UpdateStats()
        {
            StatsChanged?.Invoke();
        }
    }
}
