using System;
using System.Collections;
using System.Collections.Generic;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [Serializable]
    public class GenericRange<T>
    {
        [SerializeField, HideLabel]
        private T _rangeType;
        [SerializeField, MinMaxSlider(0, 100, true), HideLabel]
        private Vector2Int _range;

        public T RangeType => _rangeType;
        
        public Vector2Int Range => _range;

        public GenericRange(T rangeType, Vector2Int range)
        {
            _rangeType = rangeType;
            _range = range;
        }
    }
}
