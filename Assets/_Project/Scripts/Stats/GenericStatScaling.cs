using System;
using System.Collections;
using System.Collections.Generic;
using Heroes.ScriptableObjects.Values;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [SerializeField]
    public class GenericStatScaling<T, U> where U: GenericRange<T>
    {
        [InlineProperty, HideLabel]
        public U range;

        [FoldoutGroup("Scaling")]
        [SerializeField] private float per = 1;
        [FoldoutGroup("Scaling")]
        [SerializeField] private float value = 1;
        
        [FoldoutGroup("Scaling"), Button]
        public Vector2Int CalculateMinMaxScaled(int level)
        {
            if (level == 0)
                return range.Range;
            
            return new Vector2Int(
                range.Range.x + Mathf.RoundToInt((level-1) / per * value), 
                range.Range.y + Mathf.RoundToInt((level-1) / per * value));
        }

        public string ToString(int level)
        {
            Vector2 minMax = CalculateMinMaxScaled(level);

            return minMax.x + " - " + minMax.y;
        }
    }
}
