using System;
using System.Collections;
using System.Collections.Generic;
using Heroes.ScriptableObjects.Values;
using UnityEngine;

namespace Heroes
{
    [Serializable]
    public class ResourceScaling: GenericStatScaling<ResourceValue, GenericRange<ResourceValue>>
    {
    }
}
