using System;
using System.Collections;
using System.Collections.Generic;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes
{
    [Serializable]
    public class StatScaling: GenericStatScaling<StatType, GenericRange<StatType>>
    {
    }
}
