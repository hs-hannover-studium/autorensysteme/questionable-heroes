using System;
using System.Collections.Generic;
using Heroes.ScriptableObjects;
using Heroes.ScriptableObjects.Values;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.Stats
{
    [Serializable]
    public class HeroStatCollection : StatCollection
    {
        [PropertyOrder(2), ReadOnly]
        public List<Resource> capacity = new List<Resource>();
        [PropertyOrder(2), ReadOnly]
        public List<Resource> drops = new List<Resource>();

        public HeroStatCollection(HeroData heroData)
        {
            foreach (var stat in heroData.stats)
            {
                stats.Add(new Stat(stat));
            }
            foreach (var resource in heroData.capacity)
            {
                capacity.Add(new Resource(resource));
            }
            foreach (var resource in heroData.drops)
            {
                drops.Add(new Resource(resource));
            }
        }
        
        public Resource this[ResourceValue type]
        {
            get
            {
                foreach (var resource in capacity)
                {
                    if (resource.ResourceValue.Equals(type))
                        return resource;
                }
                return null;
            }
        }

        private protected override bool CheckAbilityToLevel(int level)
        {
            return true;
        }

        private protected override void UseResourcesToLevel(int level)
        {
        }

        private protected override void UpdateStats()
        {
            base.UpdateStats();
            foreach (var stat in stats)
            {
                stat.UpdateValue(Level);
            }
            foreach (var resource in capacity)
            {
                resource.UpdateValue(Level);
            }
            foreach (var resource in drops)
            {
                resource.UpdateValue(Level);
            }
        }
    }
}
