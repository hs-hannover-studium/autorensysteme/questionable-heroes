using System;
using Heroes.ScriptableObjects.Values;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Heroes.Stats
{
    [Serializable]
    public class ResourceRange: GenericRange<ResourceValue>
    {
        public ResourceRange(ResourceValue rangeType, Vector2Int range) : base(rangeType, range)
        {
        }
    }
}
