using System;
using System.Collections.Generic;
using Heroes.Misc;
using Heroes.ScriptableObjects.Buildings;
using Heroes.ScriptableObjects.Values;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.Stats
{
    [Serializable]
    public class TowerStatCollection : StatCollection
    {
        private TowerData _towerData;

        [ShowInInspector, ReadOnly, PropertyOrder(2)]
        private List<ResourceValuePair> _investedResources = new List<ResourceValuePair>();

        public TowerStatCollection(TowerData towerData)
        {
            _towerData = towerData;

            foreach (var statValue in towerData.stats)
            {
                stats.Add(new Stat(statValue));
            }

            foreach (var resourceCost in towerData.cost)
            {
                _investedResources.Add(new ResourceValuePair(resourceCost.resourceValue, resourceCost.value));
            }
        }

        public List<ResourceValuePair> SellResources
        {
            get
            {
                List<ResourceValuePair> sellResources = new List<ResourceValuePair>();
                foreach (var resourceCost in _investedResources)
                {
                    sellResources.Add(new ResourceValuePair(resourceCost.resourceValue, Mathf.RoundToInt(resourceCost.value*0.6f)));
                }

                return sellResources;
            }
        }

        private protected override bool CheckAbilityToLevel(int level)
        {
            if (level > _towerData.MaxLevel)
                return false;
            
            bool enoughResourcesToLevel = true;
            foreach (var resource in _towerData.levelCost)
            {
                Vector2Int minMax = resource.CalculateMinMaxScaled(level);
                if (resource.range.RangeType.RuntimeValue < minMax.x)
                    enoughResourcesToLevel = false;
            }

            return enoughResourcesToLevel;
        }

        private protected override void UseResourcesToLevel(int level)
        {
            foreach (var resource in _towerData.levelCost)
            {
                Vector2Int minMax = resource.CalculateMinMaxScaled(level);
                resource.range.RangeType.Subtract(minMax.x);
                ResourceValuePair resourceToIncrease =
                    _investedResources.Find(e => e.resourceValue == resource.range.RangeType);
                resourceToIncrease.value += minMax.x;
            }
        }

        private protected override void UpdateStats()
        {
            base.UpdateStats();

            foreach (var stat in stats)
            {
                stat.UpdateValue(Level);
            }
        }
    }
}