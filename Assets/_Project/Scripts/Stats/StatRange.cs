using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Heroes.Stats
{
    [Serializable]
    public class StatRange: GenericRange<StatType>
    {
        public StatRange(StatType rangeType, Vector2Int range) : base(rangeType, range)
        {
        }
    }
}
