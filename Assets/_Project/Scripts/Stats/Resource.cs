using System;
using System.Collections;
using System.Collections.Generic;
using Heroes.ScriptableObjects.Values;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Heroes
{
    [Serializable]
    public class Resource: GenericStat<ResourceValue, GenericRange<ResourceValue>>
    {
        [ShowInInspector, HideLabel]
        public ResourceValue ResourceValue => _type;

        public Resource(GenericRange<ResourceValue> range) : base(range)
        {
        }

        public Resource(GenericStatScaling<ResourceValue, GenericRange<ResourceValue>> scaling) : base(scaling)
        {
        }
    }
}
