using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Heroes.Stats
{
    public enum StatType
    {
        HP = 1,
        Damage = 2,
        ATKSpeed = 4,
        MovementSpeed = 8,
        ATKRange = 16,
        ViewRange = 32
    }
}
