using Heroes.PlayerInputs;
using Heroes.ScriptableObjects.Events;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Heroes.Cheats
{
    public class CheatControls : MonoBehaviour
    {
        [SerializeField] private GameObject cheatWindow;

        private bool _isOpen;

        private void OnEnable()
        {
            PlayerInputController.Instance.inputActions.Gameplay.Cheats.performed += Toggle;
        }

        private void OnDisable()
        {
            PlayerInputController.Instance.inputActions.Gameplay.Cheats.performed -= Toggle;
        }

        private void Toggle(InputAction.CallbackContext obj)
        {
            if (_isOpen)
            {
                cheatWindow.SetActive(false);
                _isOpen = false;
            }
            else
            {
                cheatWindow.SetActive(true);
                _isOpen = true;
            }
        }
    }
}