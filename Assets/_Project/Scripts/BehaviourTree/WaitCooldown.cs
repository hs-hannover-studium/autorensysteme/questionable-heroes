using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using Heroes.BehaviourTree.SharedVariables;
using Heroes.Stats;
using UnityEngine;

namespace Heroes.BehaviourTree.Tower
{
    public abstract class WaitCooldown : Action
    {
        private protected abstract Stat AtkSpeedStat { get; set; }
        
        private float _timer;

        public override TaskStatus OnUpdate()
        {
            if (_timer > 0f)
            {
                _timer -= Time.deltaTime;
                return TaskStatus.Running;
            }
            
            return TaskStatus.Success;
        }

        public void ResetTimer()
        {
            _timer = 100f/AtkSpeedStat.RuntimeValue;
        }
    }
}
