using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Heroes.Stats;
using UnityEngine;

namespace Heroes
{
    public abstract class TargetsInRange : Conditional
    {
        public SharedTransform selfPosition;
        public SharedLayerMask targetLayerMask;
        public SharedLayerMask obstacleLayerMask;
        public SharedTransformList visibleTargets;
        public SharedTransform currentTarget;

        public Color lineColor = Color.green;

        private Collider[] _hitColliders;

        private bool _fixedUpdate = false;

        private protected abstract Stat StatRange { get; set; }
        
        public override void OnDrawGizmos()
        {
            Gizmos.color = lineColor;
            if (StatRange != null)
            {
                Gizmos.matrix = transform.localToWorldMatrix;
                Gizmos.DrawWireSphere(Vector3.zero, (float) StatRange.RuntimeValue/10);
            }
            
            if (currentTarget.Value != null)
                Gizmos.DrawLine(selfPosition.Value.position, currentTarget.Value.position);
        }

        public override TaskStatus OnUpdate()
        {
            if (!_fixedUpdate)
                return TaskStatus.Running;

            _fixedUpdate = false;

            if (visibleTargets.Value.Count != 0) 
                return TaskStatus.Success;

            if (!currentTarget.IsNone)
            {
                if (currentTarget.Value != null)
                {
                    currentTarget.SetValue(null);
                }
            }
                
            
            return TaskStatus.Failure;
        }
        
        public override void OnFixedUpdate()
        {
            FindVisibleTargets();
        }

        private void FindVisibleTargets()
        {
            _hitColliders = Physics.OverlapSphere(transform.position, (float) StatRange.RuntimeValue/10, targetLayerMask.Value);
            
            visibleTargets.Value.Clear();
            foreach (var collider in _hitColliders)
            {
                if (collider == null)
                    continue;
                    
                float distance = Vector3.Distance(selfPosition.Value.position, collider.transform.position);
                bool isVisible = !Physics.Raycast(selfPosition.Value.position,
                    collider.transform.position - selfPosition.Value.position,
                    out RaycastHit hit, distance, obstacleLayerMask.Value);
                
                if (!isVisible)
                    continue;

                visibleTargets.Value.Add(collider.transform);
            }
            _fixedUpdate = true;
        }
    }
}
