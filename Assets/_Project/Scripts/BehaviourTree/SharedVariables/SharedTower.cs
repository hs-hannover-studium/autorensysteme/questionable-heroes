using System;
using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace Heroes.BehaviourTree.SharedVariables
{
    [Serializable]
    public class SharedTower : SharedVariable<Build.Tower.Tower>
    {
        public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
        public static implicit operator SharedTower(Build.Tower.Tower value) { return new SharedTower { mValue = value }; }
    }
}
