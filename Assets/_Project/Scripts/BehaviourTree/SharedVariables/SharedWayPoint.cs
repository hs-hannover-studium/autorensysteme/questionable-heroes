using System;
using BehaviorDesigner.Runtime;

namespace Heroes
{
    [Serializable]
    public class SharedWayPoint : SharedVariable<WayPoint>
    {
        public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
        public static implicit operator SharedWayPoint(WayPoint value) { return new SharedWayPoint { mValue = value }; }
    }
}
