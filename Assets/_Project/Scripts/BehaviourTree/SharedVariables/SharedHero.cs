using System;
using BehaviorDesigner.Runtime;
using Heroes.Heroes;

namespace Heroes
{
    [Serializable]
    public class SharedHero : SharedVariable<Hero>
    {
        public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
        public static implicit operator SharedHero(Hero value) { return new SharedHero { mValue = value }; }
    }
}
