using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using DG.Tweening;
using Heroes.Stats;
using Heroes.Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes
{
    public class MoveToTarget : Action
    {
        public SharedTransform target;
        public SharedTransform front;
        public SharedLayerMask layerMask;

        private NavMeshAgent _navMeshAgent;
        private float _defaultRadius;
        private NavMeshPath _path = new NavMeshPath();
        
        private bool _collision;
        private TaskStatus _status = TaskStatus.Running;

        private Vector3 _colliderSize = new Vector3(0.25f, 1f, 0.25f);

        public override void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;
            if (front.Value != null)
            {
                Gizmos.matrix = front.Value.localToWorldMatrix;
                Gizmos.DrawWireCube(Vector3.zero, _colliderSize*2f);
            }
                
        }

        public override void OnAwake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _defaultRadius = _navMeshAgent.radius;
        }

        public override void OnStart()
        {
            if (target.Value == null)
                return;

            SetPathDestination(target.Value.position);
        }


        public override TaskStatus OnUpdate()
        {
            if (target.Value == null)
                return TaskStatus.Failure;

            while (_navMeshAgent.pathPending)
            {
                return TaskStatus.Running;
            }
            
            if (_path.status != NavMeshPathStatus.PathComplete)
            {
                return TaskStatus.Failure;
            }

            _path.DrawPath(Color.blue);
            
            if (_navMeshAgent.isStopped)
            {
                _navMeshAgent.isStopped = false;
                _navMeshAgent.radius = _defaultRadius;
                _navMeshAgent.avoidancePriority = Random.Range(1, 100);
            }

            StartCoroutine(WaitForFixedUpdate());

            return _status;
        }

        public override void OnFixedUpdate()
        {
            // checks if standing Heroes are in front of oneself
            _collision = false;
            Collider[] hitColliders = Physics.OverlapBox(front.Value.position, _colliderSize, front.Value.rotation, layerMask.Value);
            foreach (var collider in hitColliders)
            {
                if (collider.transform == transform) continue;

                if (!collider.transform.TryGetComponent(out NavMeshAgent agent)) continue;
                
                if (agent.avoidancePriority == 0) // avoidance priority 0 -> Hero is standing
                {
                    _collision = true;
                }
            }
        }

        private void SetPathDestination(Vector3 destination)
        {
            if (!NavMesh.SamplePosition(destination, out NavMeshHit hit, 2f, NavMesh.AllAreas)) 
                return;
            
            if (_navMeshAgent.CalculatePath(hit.position, _path))
            {
                _navMeshAgent.SetPath(_path);
            }
        }

        private IEnumerator WaitForFixedUpdate()
        {
            // makes sure that collision detection is actually executed
            yield return new WaitForFixedUpdate();

            if (_collision)
            {
                /*
                 decrease NavMeshAgent radius OnCollision with a standing Hero
                 to be able to go past that Hero/ possible multiple Heroes
                 */
                if (_navMeshAgent.radius > 0)
                    _navMeshAgent.radius *= 0.5f;
            }
            else
            {
                _navMeshAgent.radius = _defaultRadius;
            }

            float remainingDistance = _path.GetPathDistance();
            
            bool destinationReached = !_navMeshAgent.hasPath && remainingDistance <= _navMeshAgent.stoppingDistance;
            
            _status = !destinationReached ? TaskStatus.Running : TaskStatus.Success;
            
        }
    }
}
