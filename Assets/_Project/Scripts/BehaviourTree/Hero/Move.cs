using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes
{
    public class Move : Action
    {
        public SharedTransform front;
        public SharedLayerMask layerMask;

        private NavMeshAgent _navMeshAgent;
        private float _defaultRadius;
        
        private bool _collision;

        private Vector3 _colliderSize = new Vector3(0.25f, 1f, 0.25f);

        public override void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;
            if (front.Value != null)
            {
                Gizmos.matrix = front.Value.localToWorldMatrix;
                Gizmos.DrawWireCube(Vector3.zero, _colliderSize*2f);
            }
        }

        public override void OnAwake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _defaultRadius = _navMeshAgent.radius;
        }

        public override TaskStatus OnUpdate()
        {
            if (_navMeshAgent.isStopped)
            {
                _navMeshAgent.isStopped = false;
                _navMeshAgent.radius = _defaultRadius;
                _navMeshAgent.avoidancePriority = Random.Range(1, 100);
            }

            if (_collision)
            {
                /*
                 decrease NavMeshAgent radius OnCollision with a standing Hero
                 to be able to go past that Hero/ possible multiple Heroes
                 */
                if (_navMeshAgent.radius > 0)
                    _navMeshAgent.radius *= 0.5f;
            }
            else
            {
                _navMeshAgent.radius = _defaultRadius;
            }

            return TaskStatus.Running;
        }

        public override void OnFixedUpdate()
        {
            // checks if standing Heroes are in front of oneself
            _collision = false;
            Collider[] hitColliders = Physics.OverlapBox(front.Value.position, _colliderSize, Quaternion.identity, layerMask.Value);
            foreach (var collider in hitColliders)
            {
                if (collider.transform == transform) continue;

                if (!collider.transform.TryGetComponent(out NavMeshAgent agent)) continue;
                
                if (agent.avoidancePriority == 0) // avoidance priority 0 -> Hero is standing
                {
                    _collision = true;
                }
            }
        }
    }
}
