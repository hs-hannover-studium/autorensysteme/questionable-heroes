using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Heroes.Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes
{
    public class SetDestination : Action
    {
        public SharedTransform target;
        
        private NavMeshAgent _navMeshAgent;
        private NavMeshPath _path = new NavMeshPath();

        private Vector2 _offset;
        
        public override void OnAwake()
        {
            _offset = Random.insideUnitCircle*0.5f;
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override TaskStatus OnUpdate()
        {
            if (target.Value == null)
                return TaskStatus.Failure;
            
            SetPathDestination(target.Value.position);

            while (_navMeshAgent.pathPending)
            {
                return TaskStatus.Running;
            }

            if (_path.status != NavMeshPathStatus.PathComplete)
            {
                return TaskStatus.Failure;
            }
            
            _path.DrawPath(Color.blue);
            return TaskStatus.Success;
        }

        private void SetPathDestination(Vector3 destination)
        {
            if (!NavMesh.SamplePosition(destination+new Vector3(_offset.x, 1f, _offset.y), out NavMeshHit hit, 1f, NavMesh.AllAreas)) 
                return;
            
            if (_navMeshAgent.CalculatePath(hit.position, _path))
            {
                _navMeshAgent.SetPath(_path);
            }
        }
    }
}
