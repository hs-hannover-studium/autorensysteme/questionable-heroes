using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Heroes
{
    public class ChooseNewAbsoluteTarget : Action
    {
        public SharedTransform absoluteTarget;
        public SharedWayPoint wayPoint;

        private PathDecider _pathDecider;
        
        public override void OnAwake()
        {
            _pathDecider = GetComponent<PathDecider>();
        }

        public override TaskStatus OnUpdate()
        {
            WayPoint target = _pathDecider.DecidePath();

            if (target == null)
                return TaskStatus.Failure;

            absoluteTarget.SetValue(target.transform);
            wayPoint.SetValue(target);
            return TaskStatus.Success;
        }
    }
}
