using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes
{
    public class SetAvoidancePriority : Action
    {
        [SerializeField]
        private int avoidancePriority = 101;
        
        private NavMeshAgent _navMeshAgent;
        
        public override void OnAwake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override TaskStatus OnUpdate()
        {
            if (_navMeshAgent == null)
                return TaskStatus.Failure;
            
            _navMeshAgent.avoidancePriority = avoidancePriority;
            return TaskStatus.Success;
        }
    }
}
