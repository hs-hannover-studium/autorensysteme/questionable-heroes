using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Heroes
{
    public class GetTreasure : Action
    {
        public SharedHero hero;
        
        public override TaskStatus OnUpdate()
        {
            foreach (var treasure in hero.Value.runtimeStats.capacity)
            {
                treasure.ResourceValue.RuntimeValue -= treasure.RuntimeValue;
            }
            return TaskStatus.Success;
        }
    }
}
