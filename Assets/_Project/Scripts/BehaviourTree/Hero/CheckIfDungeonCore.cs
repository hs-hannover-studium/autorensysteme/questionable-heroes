using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Heroes
{
    public class CheckIfDungeonCore : Conditional
    {
        public SharedWayPoint wayPoint;
    
        public override TaskStatus OnUpdate()
        {
            if (wayPoint.Value == null)
                return TaskStatus.Failure;

            if (!wayPoint.Value.isNextToCore)
                return TaskStatus.Failure;

            return TaskStatus.Success;
        }
    }
}
