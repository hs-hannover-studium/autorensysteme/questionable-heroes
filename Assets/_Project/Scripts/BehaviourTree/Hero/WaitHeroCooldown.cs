using BehaviorDesigner.Runtime.Tasks;
using Heroes.BehaviourTree.Tower;
using Heroes.Stats;

namespace Heroes
{
    public class WaitHeroCooldown: WaitCooldown
    {
        public SharedHero hero;
        private protected override Stat AtkSpeedStat { get; set; }

        public override void OnAwake()
        {
            AtkSpeedStat = hero.Value.runtimeStats[StatType.ATKSpeed];
        }

        public override TaskStatus OnUpdate()
        {
            return base.OnUpdate();
        }
    }
}