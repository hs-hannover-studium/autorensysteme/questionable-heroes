using BehaviorDesigner.Runtime.Tasks;
using Heroes.Stats;
using UnityEngine;

namespace Heroes
{
    public class TargetsInViewRange : TargetsInRange
    {
        public SharedHero hero;
        private protected override Stat StatRange { get; set; }
        
        public override void OnStart()
        {
            StatRange = hero.Value.runtimeStats[StatType.ViewRange];
        }

        public override TaskStatus OnUpdate()
        {
            return base.OnUpdate();
        }

        public override void OnFixedUpdate()
        {
            base.OnFixedUpdate();
        }
    }
}
