using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Heroes.Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace Heroes
{
    public class DestinationReached : Conditional
    {
        private NavMeshAgent _navMeshAgent;

        public override void OnAwake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override TaskStatus OnUpdate()
        {
            bool destinationReached = (!_navMeshAgent.hasPath || _navMeshAgent.velocity.sqrMagnitude == 0f) && 
                                      _navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance;

            return !destinationReached ? TaskStatus.Failure : TaskStatus.Success;
        }
    }
}
