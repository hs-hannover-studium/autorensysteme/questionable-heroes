using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using DG.Tweening;
using UnityEngine.AI;

namespace Heroes
{
    public class StopMovement: Action
    {
        private NavMeshAgent _navMeshAgent;

        private float _defaultRadius;

        public override void OnAwake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _defaultRadius = _navMeshAgent.radius;
        }

        public override TaskStatus OnUpdate()
        {
            _navMeshAgent.isStopped = true;
            _navMeshAgent.nextPosition = transform.position;
            _navMeshAgent.avoidancePriority = 0;
            _navMeshAgent.radius = 0.15f;
            return TaskStatus.Success;
        }
    }
}