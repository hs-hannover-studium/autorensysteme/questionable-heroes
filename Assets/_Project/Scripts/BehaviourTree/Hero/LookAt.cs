using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Heroes.ScriptableObjects.RuntimeSets;
using UnityEngine;

namespace Heroes
{
    public class LookAt: Action
    {
        public SharedTransform target;

        [SerializeField]
        private TileSet _dungeonCores;

        public override TaskStatus OnUpdate()
        {
            if (target.Value == null)
            {
                if (_dungeonCores == null)
                    return TaskStatus.Failure;
                
                if (_dungeonCores.Count == 0)
                    return TaskStatus.Failure;
                
                target.SetValue(_dungeonCores[0].transform);
            }
                
            Vector3 direction = (target.Value.position - transform.position).normalized;
            transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));;

            return TaskStatus.Success;
        }
    }
}