using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Heroes.Stats;
using UnityEngine;

namespace Heroes
{
    public class TargetInAttackRange : Conditional
    {
        public SharedHero hero;
        public SharedTransform selfPosition;
        public SharedTransform target;
        
        public Color lineColor = Color.red;

        private Stat _statRange;
        private readonly float _offset = Random.Range(-0.15f, 0.15f);

        public override void OnDrawGizmos()
        {
            Gizmos.color = lineColor;
            if (_statRange != null)
            {
                Gizmos.matrix = transform.localToWorldMatrix;
                Gizmos.DrawWireSphere(Vector3.zero, (float) _statRange.RuntimeValue/10 + _offset);
            }
            
            if (target.Value != null)
                Gizmos.DrawLine(selfPosition.Value.position, target.Value.position);
        }

        public override void OnAwake()
        {
            _statRange = hero.Value.runtimeStats[StatType.ATKRange];
        }

        public override TaskStatus OnUpdate()
        {
            if (target.Value == null)
                return TaskStatus.Failure;
        
            if (!target.Value.TryGetComponent(out Damageable.Damageable damageable))
                return TaskStatus.Failure;
            
            if (!damageable.IsAlive)
                return TaskStatus.Failure;
            
            float distance = Vector3.Distance(selfPosition.Value.position, target.Value.position);

            return distance <= (float) _statRange.RuntimeValue/10 + _offset ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}
