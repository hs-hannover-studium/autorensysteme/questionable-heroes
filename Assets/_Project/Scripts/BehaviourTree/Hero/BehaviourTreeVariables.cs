using BehaviorDesigner.Runtime;
using Heroes.Heroes;
using UnityEngine;

namespace Heroes
{
    public class BehaviourTreeVariables : MonoBehaviour
    {
        private BehaviorTree _behaviourTree;
        [SerializeField] private Transform selfPosition;
        [SerializeField] private Transform front;
        [SerializeField] private GameObject animator;
        [SerializeField] private Hero hero;

        private void Awake()
        {
            _behaviourTree = GetComponent<BehaviorTree>();
        }

        private void Start()
        {
            _behaviourTree.SetVariableValue("SelfPosition", selfPosition);
            _behaviourTree.SetVariableValue("Front", front);
            _behaviourTree.SetVariableValue("Animator", animator);
            _behaviourTree.SetVariableValue("Hero", hero);
            _behaviourTree.enabled = true;
        }
    }
}
