using BehaviorDesigner.Runtime.Tasks;
using Heroes.BehaviourTree.SharedVariables;
using Heroes.Stats;

namespace Heroes.BehaviourTree.Tower
{
    public class TargetsInTowerRange : TargetsInRange
    {
        public SharedTower tower;
        private protected override Stat StatRange { get; set; }
        
        public override void OnStart()
        {
            StatRange = tower.Value.runtimeStats[StatType.ATKRange];
        }

        public override TaskStatus OnUpdate()
        {
            return base.OnUpdate();
        }

        public override void OnFixedUpdate()
        {
            base.OnFixedUpdate();
        }
    }
}
