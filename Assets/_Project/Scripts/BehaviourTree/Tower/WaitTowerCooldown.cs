using BehaviorDesigner.Runtime.Tasks;
using Heroes.BehaviourTree.SharedVariables;
using Heroes.Stats;

namespace Heroes.BehaviourTree.Tower
{
    public class WaitTowerCooldown: WaitCooldown
    {
        public SharedTower tower;
        private protected override Stat AtkSpeedStat { get; set; }

        public override void OnAwake()
        {
            AtkSpeedStat = tower.Value.runtimeStats[StatType.ATKSpeed];
        }

        public override TaskStatus OnUpdate()
        {
            return base.OnUpdate();
        }
    }
}