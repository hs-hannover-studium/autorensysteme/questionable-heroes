using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Heroes.BehaviourTree.SharedVariables;
using Heroes.Build.Tower;
using Heroes.ScriptableObjects.Buildings;
using Heroes.Stats;
using UnityEngine;

namespace Heroes.BehaviourTree.Tower
{
    public class ShootTarget : Action
    {
        public SharedTower tower;
        public SharedTransform target;
        public SharedTransform shootPosition;

        public override TaskStatus OnUpdate()
        {
            if (target.Value == null)
                return TaskStatus.Failure;

            TowerData towerData = (TowerData) tower.Value.Data;

            GameObject projectileGO = Object.Instantiate(towerData.Projectile, shootPosition.Value.position, Quaternion.identity);
            if (!projectileGO.TryGetComponent(out Projectile projectile))
            {
                Object.Destroy(projectileGO);
                return TaskStatus.Failure;
            }
            
            projectile.Shoot(target.Value.position, tower.Value.runtimeStats[StatType.Damage].RuntimeValue);
            return TaskStatus.Success;
        }
    }
}
