using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Heroes.BehaviourTree.Tower
{
    public class FindNearestTarget : Action
    {
        public SharedTransformList visibleTargets;
        public SharedTransform target;

        public override TaskStatus OnUpdate()
        {
            if (visibleTargets.Value.Count == 0)
            {
                ResetTarget();
                return TaskStatus.Failure;
            }

            Transform nearestTarget = null;
            float shortestDistance = float.PositiveInfinity;
            for (var i = 0; i < visibleTargets.Value.Count; i++)
            {
                if (visibleTargets.Value[i] == null)
                    continue;
                
                if (!visibleTargets.Value[i].TryGetComponent(out Damageable.Damageable damageable))
                    continue;
                
                if (!damageable.IsAlive)
                    continue;

                float currentDistance = Vector3.Distance(transform.position, visibleTargets.Value[i].position);
                if (currentDistance < shortestDistance)
                {
                    nearestTarget = visibleTargets.Value[i];
                    shortestDistance = Vector3.Distance(transform.position, nearestTarget.position);
                }
            }

            if (nearestTarget == null)
            {
                ResetTarget();
                return TaskStatus.Failure;
            }
                

            if (nearestTarget == target.Value)
                return TaskStatus.Success;

            
            ResetTarget();
            target.SetValue(nearestTarget);
            return TaskStatus.Success;
        }

        private void ResetTarget()
        {
            if (target.Value != null)
            {
                target.SetValue(null);
            }
        }
    }
}
