using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Heroes.BehaviourTree.Tower
{
    public class RotateTowardsTarget : Action
    {
        public SharedTransform target;
        public SharedTransform rotatePart;
        
        public float rotationSpeed;

        public override TaskStatus OnUpdate()
        {
            Vector3 targetPosition = new Vector3(target.Value.position.x, rotatePart.Value.position.y,
                target.Value.position.z);
            Vector3 targetDirection = (targetPosition - rotatePart.Value.position).normalized;
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
            rotatePart.Value.rotation = Quaternion.RotateTowards(rotatePart.Value.rotation, targetRotation, Time.deltaTime*rotationSpeed);
            return Quaternion.Angle(rotatePart.Value.rotation, targetRotation) <= 0.01f ? TaskStatus.Success : TaskStatus.Running;
        }
    }
}
