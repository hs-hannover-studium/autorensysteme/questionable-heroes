using BehaviorDesigner.Runtime.Tasks;
using Heroes.BehaviourTree.Tower;

namespace Heroes.Assets.BehaviourTree
{
    public class ResetCooldown: Action
    {
        public WaitCooldown cooldown;

        public override TaskStatus OnUpdate()
        {
            if (cooldown == null)
                return TaskStatus.Failure;
            
            cooldown.ResetTimer();
            return TaskStatus.Success;

        }
    }
}