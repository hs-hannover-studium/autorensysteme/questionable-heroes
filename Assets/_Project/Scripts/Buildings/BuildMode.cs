using System;
using DG.Tweening;
using Heroes.Extensions;
using Heroes.PlayerInputs;
using Heroes.ScriptableObjects.Buildings;
using Heroes.ScriptableObjects.Events;
using Heroes.ScriptableObjects.RuntimeSets;
using Heroes.World;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

namespace Heroes.Buildings
{
    public class BuildMode : MonoBehaviour
    {
        #region Variables

        public static BuildMode Instance { get; private set; }

        public GameEvent placementStarted;
        public GameEvent placementEnded;

        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private LayerMask guideLayer;
        [SerializeField] private GameObject placementGuidePrefab;
        [SerializeField] private ResourcesSet playerResources;

        [FoldoutGroup("Events")] public UnityEvent OnPlaced;
        [FoldoutGroup("Events")] public UnityEvent OnRotated;
        [FoldoutGroup("Events")] public UnityEvent OnCancelled;
        [FoldoutGroup("Events")] public UnityEvent OnCannotPlace;
        [FoldoutGroup("Events")] public UnityEvent OnNotEnoughResources;

        [FoldoutGroup("Placement")] [SerializeField]
        private Ease placeEase = Ease.Linear;

        [FoldoutGroup("Placement")] [SerializeField] [Range(0, 2)]
        private float placeDuration = 0.1f;

        [FoldoutGroup("Placement")] [SerializeField] [Range(0, 2)]
        private float shakeDuration = 0.25f;

        [FoldoutGroup("Placement")] [SerializeField]
        private Vector3 shakeStrength = new Vector3(1f, 0, 0);

        [FoldoutGroup("Placement")] [SerializeField] [Range(0, 50)]
        private int shakeVibrato = 10;

        [FoldoutGroup("Placement")] [SerializeField] [Range(0, 90)]
        private float shakeRandomness = 90F;

        [FoldoutGroup("Dragging")] [SerializeField]
        private Vector3 offset = new Vector3(0, 2, 0);

        [FoldoutGroup("Dragging")] [SerializeField]
        private bool consistentYOffset = true;

        [FoldoutGroup("Dragging")] [SerializeField]
        private Ease draggingEase = Ease.Linear;

        [FoldoutGroup("Dragging")] [SerializeField]
        private bool draggingSnap = true;

        [FoldoutGroup("Dragging")]
        [SerializeField]
        [Tooltip("Amount which the mouse has to move before searching for new tile")]
        [ShowIf("draggingSnap")]
        [Range(0, 3)]
        private float epsilon = 0.1f;

        [FoldoutGroup("Dragging")] [SerializeField] [Range(0, 2)]
        private float draggingDuration = 0.1f;

        [FoldoutGroup("Rotation")] [SerializeField]
        private Ease rotationEase = Ease.Linear;

        [FoldoutGroup("Rotation")] [SerializeField] [Range(0, 2)]
        private float rotationDuration = 0.25f;

        [FoldoutGroup("Rotation")] [SerializeField]
        private Vector3 rotationAngle = new Vector3(0, 90, 0);

        private Tile _focusedTile;
        private Vector3 _lastPosition = Vector3.zero;
        private GameObject _buildingGameObject;
        private Building _building;
        private BuildingData _buildingData;

        private bool _placementIsActive;
        private Camera _cam;
        private GameObject _placementGuide;

        #endregion

        #region Lifecycle

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);

            _cam = Camera.main;
        }

        private void Update()
        {
            if (!_placementIsActive)
                return;

            Drag();
        }

        private void OnDisable()
        {
            Cancel();
        }

        #endregion

        public void StartPlacement(GameObject prefab)
        {
            Cancel();

            PlayerInputController.Instance.inputActions.BuildMode.Enable();
            PlayerInputController.Instance.inputActions.Gameplay.Disable();
            
            PlayerInputController.Instance.inputActions.BuildMode.Place.performed += Place;
            PlayerInputController.Instance.inputActions.BuildMode.Cancel.performed += Cancel;
            PlayerInputController.Instance.inputActions.BuildMode.Rotate.performed += Rotate;

            _buildingGameObject = Instantiate(prefab, transform.position, Quaternion.identity);
            _building = _buildingGameObject.GetComponent<Building>();
            _buildingData = _building.Data;

            foreach (MeshRenderer modelPart in _building.ModelParts)
                modelPart.shadowCastingMode = ShadowCastingMode.Off;
            
            _placementGuide = Instantiate(placementGuidePrefab, _buildingGameObject.transform.position, Quaternion.identity, _buildingGameObject.transform);

            _placementIsActive = true;
            placementStarted.Raise();
        }

        public void ExitPlacement()
        {
            PlayerInputController.Instance.ResetActionMap();
            
            PlayerInputController.Instance.inputActions.BuildMode.Place.performed -= Place;
            PlayerInputController.Instance.inputActions.BuildMode.Cancel.performed -= Cancel;
            PlayerInputController.Instance.inputActions.BuildMode.Rotate.performed -= Rotate;

            _placementIsActive = false;
            placementEnded.Raise();
        }

        private void Drag()
        {
            RaycastHit hit;
            Ray ray = _cam.ScreenPointToRay(Mouse.current.position.ReadValue());
            Vector3 currentPosition;
            RaycastHit guideHit;

            if (!Physics.Raycast(ray, out hit, Mathf.Infinity, groundLayer))
                return;

            if (Physics.Raycast(_buildingGameObject.transform.position, Vector3.down, out guideHit, Mathf.Infinity, guideLayer))
                _placementGuide.transform.position = guideHit.point + new Vector3(0, 0.01f, 0);

            currentPosition = Vector3Int.RoundToInt(hit.point);

            if (draggingSnap && currentPosition.Approximation(_lastPosition, epsilon))
                return;

            _lastPosition = currentPosition;
            _focusedTile = hit.transform.GetComponent<Tile>();
            Vector3 trans = _focusedTile.transform.position;
            float yOffset = consistentYOffset ? offset.y : trans.y + offset.y;

            // Snap building to underlying tile
            Vector3 snapPosition = new Vector3(trans.x + offset.x, yOffset, trans.z + offset.z);

            if (draggingSnap)
                _buildingGameObject.transform.DOMove(snapPosition, draggingDuration).SetEase(draggingEase);
            else
                _buildingGameObject.transform.DOMove(hit.point + offset, draggingDuration).SetEase(draggingEase);
        }

        private void Place(InputAction.CallbackContext context)
        {
            BuildingTile currentTile;
            if (!CanPlace(out currentTile))
                return;

            currentTile.Occupied = _buildingGameObject;
            _buildingGameObject.transform.DOMove(currentTile.PlacementSpot.transform.position, placeDuration).SetEase(placeEase);
            _buildingGameObject = null;
            
            foreach (MeshRenderer modelPart in _building.ModelParts)
                modelPart.shadowCastingMode = ShadowCastingMode.On;

            _building = null;
            
            Destroy(_placementGuide);
            
            OnPlaced.Invoke();

            ExitPlacement();
        }

        private bool CanPlace(out BuildingTile tile)
        {
            _buildingGameObject.transform.DOComplete();

            try
            {
                tile = (BuildingTile) _focusedTile;
            }
            catch (Exception e)
            {
                tile = default;
                _buildingGameObject.transform.DOShakePosition(shakeDuration, shakeStrength, shakeVibrato, shakeRandomness);
                OnCannotPlace.Invoke();
                return false;
            }

            if (!_buildingGameObject)
                return false;

            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("Hit Ui");
                return false;
            }

            if (_focusedTile.Occupied)
            {
                _buildingGameObject.transform.DOShakePosition(shakeDuration, shakeStrength, shakeVibrato, shakeRandomness);
                OnCannotPlace.Invoke();
                return false;
            }

            if (!playerResources.HasEnoughResources(_buildingData.cost))
            {
                _buildingGameObject.transform.DOShakePosition(shakeDuration, shakeStrength, shakeVibrato, shakeRandomness);
                Debug.Log("Not enough resources");
                OnNotEnoughResources.Invoke();
                return false;
            }
            
            playerResources.PayResources(_buildingData.cost);
            
            return true;
        }

        private void Rotate(InputAction.CallbackContext obj)
        {
            _buildingGameObject.transform.DOComplete();
            Vector3 rotation = _buildingGameObject.transform.eulerAngles + rotationAngle;
            _buildingGameObject.transform.DORotate(rotation, rotationDuration).SetEase(rotationEase);

            OnRotated.Invoke();
        }

        private void Cancel(InputAction.CallbackContext obj)
        {
            Cancel();
        }

        private void Cancel()
        {
            ExitPlacement();
            Destroy(_buildingGameObject);

            OnCancelled.Invoke();
        }
    }
}