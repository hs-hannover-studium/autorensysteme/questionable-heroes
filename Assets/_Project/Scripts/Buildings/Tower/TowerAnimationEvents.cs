using System.Collections;
using System.Collections.Generic;
using Heroes.Build.Tower;
using Heroes.ScriptableObjects.Buildings;
using Heroes.Stats;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Heroes
{
    public class TowerAnimationEvents : MonoBehaviour
    {
        [SerializeField]
        private GameObject _parentObject;

        [SerializeField]
        private GameObject _shootPosition;

        [FoldoutGroup("Events"), SerializeField]
        private UnityEvent onProjectileShot;

        public Transform Target { get; set; }

        private Tower _tower;
        private Projectile _currentProjectile = null;

        private void Awake()
        {
            _tower = _parentObject.GetComponent<Tower>();
        }

        public void Spawn()
        {
            if (_currentProjectile != null)
                return;

            TowerData towerData = (TowerData) _tower.Data;
            
            GameObject projectileObject = Instantiate(towerData.Projectile, _shootPosition.transform.position, _shootPosition.transform.rotation, _shootPosition.transform);
            if (!projectileObject.TryGetComponent(out Projectile projectile))
            {
                Destroy(projectileObject);
                return;
            }

            _currentProjectile = projectile;
        }

        public void Shoot()
        {
            if (_currentProjectile == null)
                return;

            if (Target == null)
            {
                Destroy(_currentProjectile.gameObject);
                return;
            }

            _currentProjectile.Shoot(Target.position, _tower.runtimeStats[StatType.Damage].RuntimeValue);
            onProjectileShot.Invoke();
            _currentProjectile = null;
        }
    }
}
