using System;
using Heroes.Buildings;
using Heroes.ScriptableObjects.Buildings;
using Heroes.Stats;
using Sirenix.OdinInspector;

namespace Heroes.Build.Tower
{
    [Serializable]
    public class Tower : Building
    {
        [HideLabel, FoldoutGroup("Stats")]
        public TowerStatCollection runtimeStats;

        private void Awake()
        {
            Instantiate();
        }

        [Button("New Instance")]
        private void Instantiate()
        {
            runtimeStats = new TowerStatCollection((TowerData) Data);
        }
    }
}
