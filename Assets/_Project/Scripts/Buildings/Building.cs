using System.Collections.Generic;
using Heroes.ScriptableObjects.Buildings;
using UnityEngine;

namespace Heroes.Buildings
{
    public class Building : MonoBehaviour
    {
        [SerializeField] private BuildingData data;
        [SerializeField] private List<MeshRenderer> modelParts = new List<MeshRenderer>();

        public List<MeshRenderer> ModelParts => modelParts;

        public BuildingData Data => data;
    }
}