using Heroes.SceneManagement;
using UnityEngine;

namespace Heroes.GameMethods
{
    public class GameMethods : MonoBehaviour
    {
        public void Quit()
        {
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
        
        public void LoadScene(int index)
        {
            SceneLoader.Instance.LoadScene(index);
        }

        public void LoadScene(string sceneName)
        {
            SceneLoader.Instance.LoadScene(sceneName);
        }
    }
}